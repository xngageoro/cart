<?php

namespace Xngage\Bundle\CartBundle\DataProvider;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;

/**
 * Provides info about specific products in cart
 */
class ProductCartDataProvider
{
    protected CurrentCartManager $currentCartManager;
    protected DoctrineHelper $doctrineHelper;
    protected AclHelper $aclHelper;

    public function __construct(
        CurrentCartManager $currentCartManager,
        DoctrineHelper $doctrineHelper,
        AclHelper $aclHelper
    ) {
        $this->currentCartManager = $currentCartManager;
        $this->doctrineHelper = $doctrineHelper;
        $this->aclHelper = $aclHelper;
    }

    /**
     * @param Product $product
     *
     * @return array|null
     */
    public function getProductUnitsQuantity(Product $product): ?array
    {
        $productUnits = $this->getProductsUnitsQuantity([$product]);

        return $productUnits[$product->getId()] ?? null;
    }

    /**
     * @param Product[] $products
     * @return array
     */
    public function getProductsUnitsQuantity(array $products): array
    {
        $cart = $this->currentCartManager->getCurrent();

        if (!$cart) {
            return [];
        }

        if ($cart->getCustomerUser()) {
            $productUnits = $this->prepareCartProductUnits($cart, $products);
        } else {
            $productUnits = $this->prepareCartProductUnitsForGuest($cart);
        }

        return $productUnits;
    }

    /**
     * @param Cart $cart
     * @return array
     */
    protected function prepareCartProductUnitsForGuest(Cart $cart): array
    {
        $lineItems = $cart->getLineItems()->toArray();

        return $this->prepareCartProductUnitsData($lineItems);
    }

    /**
     * @param Product[] $products
     * @param Cart $cart
     * @return array
     */
    protected function prepareCartProductUnits(Cart $cart, array $products): array
    {
        $lineItems = $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class)
            ->getProductItemsByCart($this->aclHelper, $cart, $products);

        return $this->prepareCartProductUnitsData($lineItems);
    }

    /**
     * @param LineItem[] $lineItems
     *
     * @return array
     */
    protected function prepareCartProductUnitsData(array $lineItems): array
    {
        $cartProducts = [];

        foreach ($lineItems as $lineItem) {
            $product = $lineItem->getProduct();

            $parentProduct = $lineItem->getParentProduct();
            if ($parentProduct) {
                $cartProductUnits = $this->getUnitData($lineItem);
                $cartProducts[$parentProduct->getId()]['line_items'][] = $cartProductUnits;
            }

            $cartProductUnits = $this->getUnitData($lineItem);
            $cartProducts[$product->getId()]['line_items'][] = $cartProductUnits;
        }

        return $cartProducts;
    }

    /**
     * @param CartLineItem $lineItem
     * @return array
     */
    protected function getUnitData(CartLineItem $lineItem): array
    {
        return [
            'id' => $lineItem->getId(),
            'productId' => $lineItem->getProduct()->getId(),
            'unit' => $lineItem->getProductUnitCode(),
            'quantity' => $lineItem->getQuantity()
        ];
    }
}
