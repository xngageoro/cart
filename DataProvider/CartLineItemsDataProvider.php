<?php

namespace Xngage\Bundle\CartBundle\DataProvider;

use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\ORM\Proxy\Proxy;
use Oro\Bundle\ShoppingListBundle\DataProvider\ShoppingListLineItemsDataProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class CartLineItemsDataProvider extends ShoppingListLineItemsDataProvider
{
    /**
     * @param Cart $cart
     *
     * @return CartLineItem[]
     */
    public function getCartLineItems(Cart $cart): array
    {
        $cartId = $cart->getId();
        if (array_key_exists($cartId, $this->lineItems)) {
            return $this->lineItems[$cartId];
        }

        if ($this->isInitializedWithProducts($cart)) {
            $lineItems = $cart->getLineItems()->toArray();
        } else {
            $repository = $this->registry->getRepository(CartLineItem::class);
            $lineItems = $repository->getItemsWithProductByCart($cart);
        }

        $this->lineItems[$cartId] = $lineItems;

        return $lineItems;
    }

    /**
     * Checks if cart line items collection is initialized along with their products.
     *
     * @param Cart $cart
     *
     * @return bool
     */
    private function isInitializedWithProducts(Cart $cart): bool
    {
        $isInitialized = true;
        $lineItems = $cart->getLineItems();
        if ($lineItems instanceof AbstractLazyCollection && !$lineItems->isInitialized()) {
            $isInitialized = false;
        } elseif ($lineItems->count()) {
            $product = $lineItems->first()->getProduct();
            if ($product instanceof Proxy && !$product->__isInitialized()) {
                $isInitialized = false;
            }
        }
        return $isInitialized;
    }
}
