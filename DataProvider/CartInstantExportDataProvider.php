<?php

namespace Xngage\Bundle\CartBundle\DataProvider;

use Doctrine\Common\Collections\ArrayCollection;
use Oro\Bundle\FrontendLocalizationBundle\Manager\UserLocalizationManagerInterface;
use Oro\Bundle\LocaleBundle\Formatter\DateTimeFormatterInterface;
use Oro\Bundle\PricingBundle\Provider\FrontendProductPricesDataProvider;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Symfony\Contracts\Translation\TranslatorInterface;

class CartInstantExportDataProvider
{
    public const EXPORT_BASENAME = 'cart';

    protected FrontendProductPricesDataProvider $frontendProductPricesDataProvider;
    protected TranslatorInterface $translator;
    protected UserLocalizationManagerInterface $userLocalizationManager;
    protected DateTimeFormatterInterface $dateTimeFormatter;

    public function __construct(
        FrontendProductPricesDataProvider $frontendProductPricesDataProvider,
        TranslatorInterface $translator,
        UserLocalizationManagerInterface $userLocalizationManager,
        DateTimeFormatterInterface $dateTimeFormatter
    ) {
        $this->frontendProductPricesDataProvider = $frontendProductPricesDataProvider;
        $this->translator = $translator;
        $this->userLocalizationManager = $userLocalizationManager;
        $this->dateTimeFormatter = $dateTimeFormatter;
    }

    /**
     * @return array
     */
    public function generateHeaderRow()
    {
        return [
            $this->translator->trans('xngage.cart.export.headers.sku'),
            $this->translator->trans('xngage.cart.export.headers.quantity'),
            $this->translator->trans('xngage.cart.export.headers.unit'),
            $this->translator->trans('xngage.cart.export.headers.product_name'),
            $this->translator->trans('xngage.cart.export.headers.price'),
            $this->translator->trans('xngage.cart.export.headers.subtotal')
        ];
    }

    /**
     * @param ArrayCollection|LineItem[] $lineItems
     *
     * @return array
     */
    public function generateRowsData(iterable $lineItems): array
    {
        $localization = $this->userLocalizationManager->getCurrentLocalization();
        $prices = $this->frontendProductPricesDataProvider->getProductsMatchedPrice($lineItems->toArray());

        $rows = [];
        foreach ($lineItems as $lineItem) {
            $price = $prices[$lineItem->getProduct()->getId()][$lineItem->getUnit()->getCode()];

            $rows[] = [
                $lineItem->getProduct()->getSku(),
                $lineItem->getQuantity(),
                $lineItem->getUnit()->getCode(),
                $lineItem->getProduct()->getName($localization)->getString(),
                $price ? $price->getValue() : 'N/A',
                $price ? ($price->getValue() * $lineItem->getQuantity()) : 'N/A'
            ];
        }
        return $rows;
    }

    /**
     * @param string $extension
     * @return string
     */
    public function getFilename(string $extension): string
    {
        return sprintf(
            '%s_%s.%s',
            CartInstantExportDataProvider::EXPORT_BASENAME,
            $this->dateTimeFormatter->format('now'),
            $extension
        );
    }
}
