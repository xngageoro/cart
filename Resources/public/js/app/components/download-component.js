
define(function (require) {
    'use strict';
    
    const BaseComponent = require('oroui/js/app/components/base/component');
    const _ = require('underscore');
    const routing = require('routing');
    const $ = require('jquery');
    const DownloadComponent = BaseComponent.extend({

        /** @property {jQuery} */
        $el: null,

        /** @property {String} */
        route: null,

        /** @property {Object} */
        routeParameters: {},

        /**
         * @inheritDoc
         */
        initialize: function (options) {
            this.$el = options._sourceElement;
            this.route = options.route;
            this.routeParameters = options.route_parameters || {};

            this.$el.on('click.' + this.cid, _.bind(this.downloadFile, this));
        },

        downloadFile: function () {
            const path = routing.generate(this.route, this.routeParameters);
            this.getFrame().prop('src', path);
        },

        /**
         * @return {jQuery}
         */
        getFrame: function () {
            let $frame = $('iframe[data-cart-download-frame]');
            if (!$frame.length) {
                $frame = $('<iframe src="" height="0" width="0" data-cart-download-frame></iframe>');
                $('body').append($frame);
            }

            return $frame;
        },

        /**
         * @inheritDoc
         */
        dispose: function () {
            this.$el.off('click.' + this.cid);
            this.getFrame().remove();
        }
    });
    
    return DownloadComponent;
});
