import ViewComponent from 'oroui/js/app/components/view-component';
import NotesModel from 'oroshoppinglist/js/app/models/shoppinglist-notes-editable-model';

const CartNotesEditableViewComponent = ViewComponent.extend({
    /**
     * @inheritDoc
     */
    constructor: function CartNotesEditableViewComponent(options) {
        CartNotesEditableViewComponent.__super__.constructor.call(this, options);
    },

    /**
     * @param {Object} options
     */
    initialize(options) {
        options.model = new NotesModel({
            id: options.cartId,
            routingOptions: {...options.routingOptions},
            notes: options.notes
        });

        CartNotesEditableViewComponent.__super__.initialize.call(this, options);
    }
});

export default CartNotesEditableViewComponent;
