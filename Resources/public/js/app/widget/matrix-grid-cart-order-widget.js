define(function(require) {
    'use strict';

    const routing = require('routing');
    const _ = require('underscore');
    const MatrixGridOrderWidget = require('oro/matrix-grid-order-widget');
    const mediator = require('oroui/js/mediator');

    const MatrixGridCartOrderWidget = MatrixGridOrderWidget.extend({
        optionNames: MatrixGridOrderWidget.prototype.optionNames.concat([
            'cartId'
        ]),

        cartId: null,

        /**
         * @inheritDoc
         */
        constructor: function MatrixGridCartOrderWidget(options) {
            MatrixGridCartOrderWidget.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            MatrixGridCartOrderWidget.__super__.initialize.call(this, options);

            const urlOptions = {
                productId: this.model.get('id'),
                cartId: this.cartId
            };

            options.url = routing.generate('xngage_cart_frontend_matrix_grid_order', urlOptions);
        },

        _onContentLoad: function(content) {
            if (_.isObject(content)) {
                mediator.trigger('cart:line-items:changed', content);
                this.remove();
            } else {
                return MatrixGridCartOrderWidget.__super__._onContentLoad.call(this, content);
            }
        }
    });

    return MatrixGridCartOrderWidget;
});
