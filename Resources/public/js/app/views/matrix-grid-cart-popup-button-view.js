define(function(require) {
    'use strict';

    const ProductAddToCartView = require('xngagecart/js/app/views/product-add-to-cart-view');
    const MatrixGridCartOrderWidget = require('xngagecart/js/app/widget/matrix-grid-cart-order-widget');

    const MatrixGridCartPopupButtonView = ProductAddToCartView.extend({
        /**
         * @inheritDoc
         */
        constructor: function MatrixGridCartPopupButtonView(options) {
            MatrixGridCartPopupButtonView.__super__.constructor.call(this, options);
        },

        _openMatrixGridPopup: function(cartId) {
            this.subview('popup', new MatrixGridCartOrderWidget({
                model: this.model,
                cartId: cartId
            }));
            this.subview('popup').render();
        },

        _saveLineItem: function(url, urlOptions, formData) {
            this._openMatrixGridPopup(urlOptions.cartId);
        },

        _addLineItem: function(url, urlOptions, formData) {
            this._openMatrixGridPopup(urlOptions.cartId);
        }
    });

    return MatrixGridCartPopupButtonView;
});
