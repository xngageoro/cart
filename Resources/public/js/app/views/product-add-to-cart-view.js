define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const ElementsHelper = require('orofrontend/js/app/elements-helper');
    const ApiAccessor = require('oroui/js/tools/api-accessor');
    const routing = require('routing');
    const mediator = require('oroui/js/mediator');
    const $ = require('jquery');
    const _ = require('underscore');
    /** @var QuantityHelper QuantityHelper **/
    const QuantityHelper = require('oroproduct/js/app/quantity-helper');

    const ProductAddToCartView = BaseView.extend(_.extend({}, ElementsHelper, {
        options: {
            emptyMatrixAllowed: false,
            buttonTemplate: '',
            removeButtonTemplate: '',
            buttonsSelector: '.add-to-cart-button',
            quantityField: '[data-name="field__quantity"]',
            messages: {
                success: 'oro.form.inlineEditing.successMessage'
            },
            save_api_accessor: {
                http_method: 'PUT',
                route: 'oro_api_cart_frontend_put_cartline_item',
                form_name: 'oro_product_frontend_line_item'
            }
        },

        listen: {
            'cart:line-items:changed mediator': '_onLineItemsUpdate'
        },

        dropdownWidget: null,

        cart: null,

        modelAttr: {
            product_cart_units: []
        },

        rendered: false,

        /**
         * @inheritDoc
         */
        constructor: function ProductAddToCartView(options) {
            ProductAddToCartView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            ProductAddToCartView.__super__.initialize.call(this, options);
            this.deferredInitializeCheck(options, ['productModel', 'dropdownWidget']);
        },

        deferredInitialize: function(options) {
            this.options = $.extend(true, {}, this.options, _.pick(options, _.keys(this.options)));

            this.dropdownWidget = options.dropdownWidget;
            this.$form = this.dropdownWidget.element.closest('form');

            this.initModel(options);

            if (this.options.buttonTemplate) {
                this.options.buttonTemplate = _.template(this.options.buttonTemplate);
            }
            if (this.options.removeButtonTemplate) {
                this.options.removeButtonTemplate = _.template(this.options.removeButtonTemplate);
            }
            this.saveApiAccessor = new ApiAccessor(this.options.save_api_accessor);

            if (this.model) {
                this.model.on('change:unit', this._onUnitChanged, this);
                this.model.on('editLineItem', this._onEditLineItem, this);
            }

            this.$form.find(this.options.quantityField).on('keydown', _.bind(this._onQuantityEnter, this));

            this.cart = options.cart;

            this.render();
        },

        initModel: function(options) {
            const modelAttr = _.each(options.modelAttr, function(value, attribute) {
                options.modelAttr[attribute] = value === 'undefined' ? undefined : value;
            }) || {};
            this.modelAttr = $.extend(true, {}, this.modelAttr, modelAttr);
            if (options.productModel) {
                this.model = options.productModel;
            }

            if (!this.model) {
                return;
            }

            _.each(this.modelAttr, function(value, attribute) {
                if (!this.model.has(attribute) || modelAttr[attribute] !== undefined) {
                    this.model.set(attribute, value);
                }
            }, this);

            if (this.model.get('product_cart_units') === undefined) {
                this.model.set('product_cart_units', []);
            }
        },

        render: function() {
            this._setEditLineItem(null, true);

            const buttons = this._collectAllButtons();

            this._getContainer().html(buttons);
        },

        _onItemInCartStateChange: function(...args) {
            this._setEditLineItem();

            const buttons = this._collectAllButtons();

            this._clearButtons();
            this._getContainer().prepend(buttons);
            this.dropdownWidget._renderButtons();
            mediator.trigger('cart:refresh');
        },

        _clearButtons: function() {
            this._getContainer()
                .trigger('disposeLayout')
                .find(this.options.buttonsSelector)
                .remove();
        },

        _getContainer: function() {
            return this.dropdownWidget.element.find('.btn-group:first');
        },

        _collectAllButtons: function() {
            let buttons = [];

            this._addCartListButtons(buttons);

            return buttons;
        },

        _addCartListButtons: function(buttons) {
            let $button = $(this.options.buttonTemplate(this.cart));
            if (!this.model) {
                buttons.push($button);
                return;
            }

            const hasLineItems = this.findByUnit(this.model.get('unit'));
            if (hasLineItems) {
                $button = this.updateLabel($button, hasLineItems);
            }
            buttons.push($button);

            if (hasLineItems) {
                const $removeButton = $(this.options.removeButtonTemplate(this.cart));
                $removeButton.find('a').attr('data-intention', 'remove');
                buttons.push($removeButton);
            }
        },

        _afterRenderButtons: function() {
            this.initButtons();
            this.rendered = true;
        },

        initButtons: function() {
            this.findAllButtons()
                .attr('role', 'button')
                .off('click' + this.eventNamespace())
                .on('click' + this.eventNamespace(), _.bind(this.onClick, this));
        },

        findDropdownButtons: function(filter) {
            const $el = this.dropdownWidget.element || this.dropdownWidget.dropdown;
            let $buttons = $el.find(this.options.buttonsSelector);
            if (filter) {
                $buttons = $buttons.filter(filter);
            }
            return $buttons;
        },

        findMainButton: function() {
            if (this.dropdownWidget.main && this.dropdownWidget.main.is(this.options.buttonsSelector)) {
                return this.dropdownWidget.main;
            }
            return $([]);
        },

        findAllButtons: function(filter) {
            let $buttons = this.findMainButton().add(this.findDropdownButtons());
            if (filter) {
                $buttons = $buttons.filter(filter);
            }
            return $buttons;
        },

        getProductCartUnitsFromModel: function() {
            if (!this.model) {
                return null;
            }
            return this.model.get('product_cart_units');
        },

        findByUnit: function(unit) {
            const productCartUnitsInModel = this.getProductCartUnitsFromModel();
            if (!productCartUnitsInModel) {
                return null;
            }
            const hasUnits = _.find(productCartUnitsInModel.line_items, {unit: unit});
            return hasUnits ? productCartUnitsInModel : null;
        },

        findLineItemByUnit: function(unit) {
            const productCartUnitsInModel = this.getProductCartUnitsFromModel();
            if (!productCartUnitsInModel) {
                return null;
            }
            return _.find(productCartUnitsInModel.line_items, {unit: unit});
        },

        _onQuantityEnter: function(e) {
            const ENTER_KEY_CODE = 13;

            if (e.keyCode === ENTER_KEY_CODE) {
                this.model.set({
                    quantity: QuantityHelper.getQuantityNumberOrDefaultValue($(e.target).val(), NaN)
                });

                let mainButton = this.findMainButton();

                if (!mainButton.length) {
                    mainButton = this.findAllButtons();
                }

                mainButton.click();
            }
        },

        validate: function() {
            return this.dropdownWidget.validateForm();
        },

        onClick: function(e) {
            const $button = $(e.currentTarget);
            if ($button.data('disabled')) {
                return false;
            }
            const url = $button.data('url');
            const intention = $button.data('intention');
            const formData = this.$form.serialize();

            const urlOptions = {
                cartId: $button.data('cart').id
            };
            if (this.model) {
                urlOptions.productId = this.model.get('id');
                if (this.model.has('parentProduct')) {
                    urlOptions.parentProductId = this.model.get('parentProduct');
                }
            }

            if (!this.validate()) {
                return;
            }

            if (intention === 'update') {
                this._saveLineItem(url, urlOptions, formData);
            } else if (intention === 'remove') {
                this._removeLineItem(url, urlOptions, formData);
            } else {
                this._addLineItem(url, urlOptions, formData);
            }
        },

        updateLabel: function($button, hasLineItems) {
            let label;
            let intention;

            if (hasLineItems) {
                label = _.__('xngage.cart.actions.update_cart');
                intention = 'update';
            } else {
                label = _.__('xngage.cart.actions.add_to_cart');
                intention = 'add';
            }

            const $link = $button.find('a');
            const $icon = $button.find('.fa').clone();

            $link
                .text(label)
                .prop('title', label)
                .attr('data-intention', intention);

            if ($icon.length) {
                $link.prepend($icon);
            }

            return $button;
        },

        _setEditLineItem: function(lineItemId, setFirstLineItem) {
            this.editLineItem = null;

            if (!this.model || this.cart === null) {
                return;
            }

            const productCartUnitsInModel = this.getProductCartUnitsFromModel();
            if (!productCartUnitsInModel || productCartUnitsInModel.length === 0) {
                return;
            }

            if (lineItemId) {
                this.editLineItem = _.findWhere(productCartUnitsInModel.line_items, {id: lineItemId});
            } else if (setFirstLineItem) {
                this.editLineItem = productCartUnitsInModel.line_items[0] || null;
            } else if (!this.model.get('quantity_changed_manually')) {
                this.editLineItem = _.findWhere(
                    productCartUnitsInModel.line_items, {unit: this.model.get('unit')}
                ) || null;
            }

            // Local variable used because this.editLineItem is set to null in model change:unit listener
            const editLineItem = this.editLineItem;
            if (editLineItem) {
                // quantity precision depend on unit, set unit first
                this.model.set('unit', editLineItem.unit);
                this.model.set('quantity', editLineItem.quantity);
                this.model.set('quantity_changed_manually', true);// prevent quantity change in other components
            }
        },

        _addProductToCart: function(url, urlOptions, formData) {
            if (this.model && !this.model.get('line_item_form_enable')) {
                return;
            }
            const self = this;

            mediator.execute('showLoading');
            $.ajax({
                type: 'POST',
                url: routing.generate(url, urlOptions),
                data: formData,
                success: function(response) {
                    self._onLineItemsUpdate(response);
                },
                complete: function() {
                    mediator.execute('hideLoading');
                }
            });
        },

        _removeProductFromCart: function(url, urlOptions, formData) {
            if (this.model && !this.model.get('line_item_form_enable')) {
                return;
            }
            const self = this;

            mediator.execute('showLoading');
            $.ajax({
                type: 'DELETE',
                url: routing.generate(url, urlOptions),
                data: formData,
                success: function(response) {
                    self._onLineItemsUpdate(response);
                },
                complete: function() {
                    mediator.execute('hideLoading');
                }
            });
        },

        _onUnitChanged: function() {
            this._setEditLineItem();
            if (this.rendered) {
                this._onItemInCartStateChange();
            }
        },

        _onEditLineItem: function(lineItemId) {
            this._setEditLineItem(lineItemId);
            this.model.trigger('focus:quantity');
        },

        _addLineItem: function(url, urlOptions, formData) {
            this._addProductToCart(url, urlOptions, formData);
        },

        _removeLineItem: function(url, urlOptions, formData) {
            this._removeProductFromCart(url, urlOptions, formData);
        },

        _saveLineItem: function(url, urlOptions, formData) {
            if (this.model && !this.model.get('line_item_form_enable')) {
                return;
            }
            mediator.execute('showLoading');

            const lineItem = this.findLineItemByUnit(this.model.get('unit'));

            const savePromise = this.saveApiAccessor.send({
                id: lineItem.id
            }, {
                quantity: QuantityHelper.formatQuantity(this.model.get('quantity')),
                unit: this.model.get('unit')
            });

            savePromise
                .done(_.bind(function(response) {
                    lineItem.quantity = response.quantity;
                    lineItem.unit = response.unit;
                    this._onItemInCartStateChange();
                    mediator.execute('showFlashMessage', 'success', _.__(this.options.messages.success));
                }, this))
                .always(function() {
                    mediator.execute('hideLoading');
                });
        },

        _onLineItemsUpdate: function(response) {
            if (!this.model || !response) {
                return;
            }

            if (response.message) {
                const isSuccessful = response.hasOwnProperty('successful') && response.successful;
                mediator.execute(
                    'showFlashMessage', (isSuccessful ? 'success' : 'error'),
                    response.message
                );
            }

            if (response.product && !_.isArray(this.model)) {
                this.model.set('product_cart_units', response.product.product_cart_units, {silent: true});
                this.model.trigger('change:product_cart_units');
            }

            this._onItemInCartStateChange();
        },

        dispose: function(options) {
            delete this.dropdownWidget;
            delete this.modelAttr;
            delete this.cart;
            delete this.editLineItem;
            delete this.$form;

            mediator.off(null, null, this);
            if (this.model) {
                this.model.off(null, null, this);
            }

            ProductAddToCartView.__super__.dispose.call(this);
        }
    }));

    return ProductAddToCartView;
});
