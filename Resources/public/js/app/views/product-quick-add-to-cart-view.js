define(function(require) {
    'use strict';

    const ProductAddToCartView = require('xngagecart/js/app/views/product-add-to-cart-view');
    const mediator = require('oroui/js/mediator');

    const ProductQuickAddToCartView = ProductAddToCartView.extend({
        /**
         * @inheritDoc
         */
        constructor: function ProductQuickAddToCartView(options) {
            ProductQuickAddToCartView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            ProductQuickAddToCartView.__super__.initialize.call(this, options);
            this.options.quickAddComponentPrefix = options.quickAddComponentPrefix;
        },

        _addProductToCart: function(url, urlOptions, formData) {
            mediator.trigger(
                this.options.quickAddComponentPrefix + ':submit',
                'xngage_cart_quick_add_processor',
                urlOptions.cartId
            );
        },

        /**
         * @param {$.Event} e
         */
        onClick: function(e) {
            e.preventDefault();
            e.stopPropagation();

            if (this.formHasErrors()) {
                return;
            }

            ProductQuickAddToCartView.__super__.onClick.call(this, e);
        },

        formHasErrors: function() {
            return !this.$form.validate().valid() ||
                this.$form.find('.product-autocomplete-error .validation-failed:visible').length;
        }
    });

    return ProductQuickAddToCartView;
});
