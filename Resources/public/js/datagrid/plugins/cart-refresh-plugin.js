import mediator from 'oroui/js/mediator';
import BasePlugin from 'oroui/js/app/plugins/base/plugin';

const CartRefreshPlugin = BasePlugin.extend({
    constructor: function CartRefreshPlugin(grid, options) {
        CartRefreshPlugin.__super__.constructor.call(this, grid, options);
    },

    enable: function() {
        if (this.enabled) {
            return;
        }

        this.listenTo(this.main.collection, 'request', () => mediator.trigger('cart:request'));
        this.listenTo(this.main.collection, 'reset', () => mediator.trigger('cart:refresh'));
        CartRefreshPlugin.__super__.enable.call(this);
    }
});

export default CartRefreshPlugin;
