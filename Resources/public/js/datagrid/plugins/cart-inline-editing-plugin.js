import _ from 'underscore';
import mediator from 'oroui/js/mediator';
import tools from 'oroui/js/tools';
import BaseComponent from 'oroui/js/app/components/base/component';
import BaseInlineEditingPlugin from 'oroshoppinglist/js/datagrid/plugins/shopping-list-inline-editing-plugin';

const CartInlineEditingPlugin = BaseInlineEditingPlugin.extend({

    constructor: function CartInlineEditingPlugin(...args) {
        CartInlineEditingPlugin.__super__.constructor.apply(this, args);
    },

    saveItems(component) {
        let componentsToSend = [];
        if (component instanceof BaseComponent && component.isChanged()) {
            componentsToSend = [component];
        } else {
            componentsToSend = this.activeEditorComponents.filter(component => component.isChanged());
        }

        componentsToSend = componentsToSend.filter(component => component.isDataValid());

        const sendData = {
            data: componentsToSend.map(component => component.getServerUpdateData()),
            fetchData: _.extend(this.getGridFetchData(), {
                appearanceType: this.main.collection.state.appearanceType
            })
        };

        componentsToSend.forEach(component => component.beforeSaveHook());
        const savePromise = this.saveApiAccessor.send({
            id: this.options.metadata.gridParams.cart_id,
            _wid: tools.createRandomUUID()
        }, sendData);

        const sendModels = componentsToSend.map(component => component.getModel());

        savePromise.done(_.bind(this.onSaveSuccess, this, sendModels.slice()))
            .fail(_.bind(this.onSaveError, this, sendModels.slice()))
            .always(_.bind(this.onSaveComplete, this));

        _.invoke(componentsToSend, 'exitEditMode', true);
        return savePromise;
    },

    onSaveComplete() {
        mediator.trigger('cart:refresh');

        this.toggleUpdateAll();
    },
});

export default CartInlineEditingPlugin;
