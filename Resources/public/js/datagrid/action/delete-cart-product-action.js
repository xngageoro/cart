define(function(require) {
    'use strict';

    const __ = require( 'orotranslation/js/translator');
    const mediator = require('oroui/js/mediator');
    const messenger = require('oroui/js/messenger');
    const routing = require('routing');
    const DeleteProductAction = require('oro/datagrid/action/delete-product-action');

    /**
     * Delete action with confirm dialog, triggers REST DELETE request
     *
     * @export  oro/datagrid/action/delete-cart-product-action
     * @class   oro.datagrid.action.DeleteCartProductAction
     * @extends oro.datagrid.action.DeleteProductAction
     */
    const DeleteCartProductAction = DeleteProductAction.extend({
        /**
         * @inheritDoc
         */
        constructor: function DeleteCartProductAction(options) {
            DeleteCartProductAction.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        doDelete() {
            const success = __(this.messages.success, this.model.toJSON());

            for (const subModel of this.model.subModels()) {
                subModel.classList().add('loading');
            }

            this.model.classList().add('loading');
            this.model.destroy({
                url: this.getLink(),
                wait: true,
                reset: false,
                uniqueOnly: true,
                toggleLoading: false,
                success() {
                    messenger.notificationFlashMessage('success', success);
                    mediator.trigger('cart:refresh');
                }
            });
        },

        /**
         * @inheritDoc
         */
        getLink: function() {
            if (this.model.attributes.isConfigurable) {
                return routing.generate('oro_api_cart_frontend_delete_cartline_item_configurable', {
                    cartId: this.datagrid.metadata.gridParams.cart_id,
                    productId: this.model.attributes.productId,
                    unitCode: this.model.attributes.unit
                });
            } else {
                return routing.generate('oro_api_cart_frontend_delete_cartline_item', {
                    id: this.model.attributes.id,
                    onlyCurrent: this.model.attributes.variantId ? 1 : 0
                });
            }
        }
    });

    return DeleteCartProductAction;
});
