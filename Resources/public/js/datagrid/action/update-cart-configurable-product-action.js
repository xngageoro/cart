import $ from 'jquery';
import __ from 'orotranslation/js/translator';
import mediator from 'oroui/js/mediator';
import messenger from 'oroui/js/messenger';
import routing from 'routing';
import WidgetComponent from 'oroui/js/app/components/widget-component';
import UpdateConfigurableProductAction from 'oro/datagrid/action/update-configurable-product-action';

/**
 * Update configurable products action with matrix grid order dialog
 *
 * @export  oro/datagrid/action/update-cart-configurable-product-action
 * @class   oro.datagrid.action.UpdateCartConfigurableProductAction
 * @extends oro.datagrid.action.UpdateConfigurableProductAction
 */
const UpdateCartConfigurableProductAction = UpdateConfigurableProductAction.extend({
    /**
     * @inheritDoc
     */
    constructor: function UpdateCartConfigurableProductAction(options) {
        UpdateCartConfigurableProductAction.__super__.constructor.call(this, options);
    },

    /**
     * @inheritDoc
     */
    run: function() {
        const title = __('xngage.frontend.cart.matrix_grid_update.title', {
            product: this.model.attributes.name,
            cart: this.datagrid.metadata.cartLabel
        });

        this.widgetOptions.options.dialogOptions.title = title;
        this.widgetOptions.options.fullscreenViewOptions.popupLabel = title;
        this.widgetOptions.options.initLayoutOptions = {
            productModel: this.model
        };
        if (!this.widgetComponent) {
            this.widgetComponent = new WidgetComponent(this.widgetOptions);
        }

        this.widgetComponent.openWidget().done(() => {
            const $form = $(this.widgetComponent.view.el).find('form');
            const columnsCount = $(this.widgetComponent.view.el).data('columns-count');

            if (columnsCount !== void 0) {
                this.widgetComponent.listenTo(this.widgetComponent.view, 'widgetReady', dialog => {
                    const width = this.getFlexibleWidth(columnsCount);

                    dialog.loadingElement.addClass('invisible');
                    dialog.widget.dialog('option', 'width', width);
                    dialog.options.dialogOptions.width = width;
                    dialog.loadingElement.removeClass('invisible');
                });
            }

            this.widgetComponent.listenTo(this.widgetComponent.view, 'adoptedFormSubmitClick', () => {
                $.ajax({
                    method: 'POST',
                    url: this.getLink(),
                    data: $form.serialize(),
                    success: response => {
                        if (response.message) {
                            messenger.notificationFlashMessage('success', response.message);
                            mediator.trigger(`datagrid:doRefresh:${this.datagrid.name}`);
                        }
                    }
                });
            });
        });
    },

    /**
     * @inheritDoc
     */
    getLink: function() {
        return routing.generate('xngage_cart_frontend_matrix_grid_update', {
            cartId: this.datagrid.metadata.gridParams.cart_id,
            productId: this.model.attributes.productId,
            unitCode: this.model.attributes.unit
        });
    }
});

export default UpdateCartConfigurableProductAction;

