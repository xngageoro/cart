define(function(require) {
    'use strict';

    const mediator = require('oroui/js/mediator');
    const MoveProductsMassAction = require('oro/datagrid/action/move-products-mass-action');
    const loadModules = require('oroui/js/app/services/load-modules');

    /**
     * Move products to the shopping list
     *
     * @export  oro/datagrid/action/move-cart-products-mass-action
     * @class   oro.datagrid.action.MoveCartProductsMassAction
     * @extends oro.datagrid.action.MoveProductsMassAction
     */
    const MoveCartProductsMassAction = MoveProductsMassAction.extend({

        /**
         * @inheritDoc
         */
        constructor: function MoveCartProductsMassAction(options) {
            MoveCartProductsMassAction.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            MoveCartProductsMassAction.__super__.initialize.call(this, options);
            this.route_parameters.id = this.route_parameters[this.datagrid.name]['cart_id'];
        },

        /**
         * @inheritDoc
         */
        _onAjaxSuccess: function(data) {
            this.datagrid.collection.fetch({
                reset: true,
                toggleLoading: false
            });
            this.datagrid.resetSelectionState();

            this._showAjaxSuccessMessage(data);
            mediator.trigger('cart:refresh');
            mediator.trigger('shopping-list:refresh');
        },

        /**
         * @inheritDoc
         */
        _handleWidget: function() {
            if (this.dispatched) {
                return;
            }
            this.frontend_options = this.frontend_options || {};
            this.frontend_options.url = this.getLinkWithParameters();
            this.frontend_options.title = this.frontend_options.title || this.label;

            loadModules(
                'orofrontend/js/app/components/frontend-' + this.frontend_handle + '-widget',
                function(WidgetType) {
                    const widget = new WidgetType(this.frontend_options);
                    this.listenToOnce(widget, {
                        'widgetReady': function () {
                            widget
                                .$el
                                .find('td.grid-body-cell-assigned > :input[name="selected"]')
                                .first()
                                .prop('checked', true);
                        }.bind(this)
                    });

                    widget.render();

                    this.listenToOnce(widget, {
                        'frontend-dialog:accept': this._handleAjax.bind(this)
                    });

                }.bind(this)
            );
        }
    });

    return MoveCartProductsMassAction;
});
