import __ from 'orotranslation/js/translator';
import mediator from 'oroui/js/mediator';
import Modal from 'oroui/js/modal';
import template from 'tpl-loader!xngagecart/templates/actions/add-cart-notes-action.html';
import AddNotesAction from 'oro/datagrid/action/add-notes-action';

const ENTER_KEY_CODE = 13;

/**
 * Add notes action, triggers REST PATCH request
 *
 * @export  oro/datagrid/action/add-cart-notes-action
 * @class   oro.datagrid.action.AddCartNotesAction
 * @extends oro.datagrid.action.AddNotesAction
 */
const AddCartNotesAction = AddNotesAction.extend({
    /**
     * @inheritDoc
     */
    constructor: function AddCartNotesAction(options) {
        AddCartNotesAction.__super__.constructor.call(this, options);
    },

    /**
     * @inheritDoc
     */
    _handleWidget() {
        if (this.dispatched) {
            return;
        }

        const notes = this.model.get('notes');
        const action = notes ? 'edit' : 'add';

        const modal = new Modal({
            className: 'modal oro-modal-normal cart-notes-modal',
            title: __(`xngage.frontend.cart.lineitem.dialog.${action}.title`, {
                productName: this.model.get('name')
            }),
            okText: __(`xngage.frontend.cart.lineitem.dialog.${action}.label`),
            cancelText: __('xngage.frontend.cart.lineitem.dialog.cancel.label'),
            okCloses: false
        }).on('shown', () => {
            this.validator = modal.$('form').validate({
                rules: this.validationRules
            });
        });

        modal.setContent(template({
            arialLabelBy: modal.cid
        }));

        modal.on('ok', () => {
            if (this.validator.form()) {
                this.updateNotes(modal.$('[name="notes"]').val());
                this._handleAjax();
                modal.close();
            }
        });

        modal.open();

        modal.$('[name="notes"]')
            .focus()
            .val(notes)
            .on('keydown', e => {
                if (e.keyCode === ENTER_KEY_CODE && e.ctrlKey) {
                    modal.trigger('ok');
                }
            });
    },

    _showAjaxSuccessMessage() {
        mediator.execute(
            'showFlashMessage',
            'success',
            __('xngage.frontend.cart.lineitem.dialog.notes.success')
        );
    }
});

export default AddCartNotesAction;
