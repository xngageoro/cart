define(function(require) {
    'use strict';

    const mediator = require('oroui/js/mediator');
    const MassAction = require('oro/datagrid/action/mass-action');

    /**
     * Move/add products to the cart
     *
     * @export  oro/datagrid/action/move-list-to-cart-products-mass-action
     * @class   oro.datagrid.action.MoveListToCartProductsMassAction
     * @extends oro.datagrid.action.MassAction
     */
    const MoveListToCartProductsMassAction = MassAction.extend({

        /**
         * @inheritDoc
         */
        constructor: function MoveListToCartProductsMassAction(options) {
            MoveListToCartProductsMassAction.__super__.constructor.call(this, options);
        },
        /**
         * @inheritDoc
         */
        _onAjaxSuccess: function(data) {
            if (this.reloadData) {
                this.datagrid.hideLoading();
                this.datagrid.collection.fetch({reset: true});
            }
            this.datagrid.resetSelectionState();

            this._showAjaxSuccessMessage(data);

            mediator.trigger('cart:refresh');
        },

    });

    return MoveListToCartProductsMassAction;
});
