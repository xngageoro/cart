services:
    _defaults:
        public: false

    xngage_cart.datagrid.total_currency_helper:
        class: Xngage\Bundle\CartBundle\Datagrid\Helper\CartGridTotalCurrencyHelper
        public: true
        arguments:
            - '@oro_currency.config.currency'
            - '@oro_pricing.provider.website_currency_provider'

    Xngage\Bundle\CartBundle\EventListener\CustomerViewListener:
        arguments:
            - '@translator'
            - '@oro_entity.doctrine_helper'
            - '@request_stack'
        tags:
            - { name: kernel.event_listener, event: oro_ui.scroll_data.before.customer-user-view, method: onCustomerUserView }
            - { name: kernel.event_listener, event: oro_ui.scroll_data.before.customer-view, method: onCustomerView }

    Xngage\Bundle\CartBundle\Discount\Converter\CartDiscountContextConverter:
        arguments:
            - '@oro_promotion.discount.line_items_to_discount_line_items_converter'
            - '@oro_pricing.user_currency_manager'
            - '@Xngage\Bundle\CartBundle\Manager\CartTotalManager'
        tags:
            - { name: 'oro_promotion.discount_context_converter' }

    Xngage\Bundle\CartBundle\Context\CartContextDataConverter:
        arguments:
            - '@oro_promotion.context.criteria_data_provider'
            - '@oro_promotion.discount.line_items_to_discount_line_items_converter'
            - '@oro_pricing.user_currency_manager'
            - '@oro_scope.scope_manager'
            - '@Xngage\Bundle\CartBundle\Manager\CartTotalManager'
        tags:
            - { name: 'oro_promotion.promotion_context_converter' }

    Xngage\Bundle\CartBundle\Manager\CartManager:
        public: true
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@oro_config.manager'
            - '@oro_entity.delete_handler_registry'
            - '@oro_product.provider.product_variant_availability_provider'
            - '@oro_product.service.quantity_rounding'
            - '@Xngage\Bundle\CartBundle\Manager\CartTotalManager'
            - '@Xngage\Bundle\CartBundle\Manager\CurrentCartManager'

    Xngage\Bundle\CartBundle\Manager\CurrentCartManager:
        public: true
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@Xngage\Bundle\CartBundle\Manager\GuestCartManager'
            - '@security.authorization_checker'
            - '@oro_website.manager'
            - '@translator'
            - '@oro_security.token_accessor'

    Xngage\Bundle\CartBundle\Manager\GuestCartManager:
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@security.token_storage'
            - '@oro_website.manager'
            - '@translator'
            - '@Xngage\Bundle\CartBundle\Authorization\CartAuthorizationChecker'

    Xngage\Bundle\CartBundle\Manager\CartTotalManager:
        public: true
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@oro_pricing.subtotal_processor.provider.subtotal_line_item_not_priced'
            - '@oro_pricing.user_currency_manager'

    Xngage\Bundle\CartBundle\Handler\CartLineItemDeleteHandler:
        parent: oro_entity.delete_handler
        public: false
        arguments:
            - '@Xngage\Bundle\CartBundle\Manager\CartTotalManager'
        tags:
            - { name: oro_entity.delete_handler, entity: Xngage\Bundle\CartBundle\Entity\CartLineItem }

    Xngage\Bundle\CartBundle\Layout\DataProvider\MatrixGridOrderFormProvider:
        arguments:
            - '@form.factory'
            - '@router'
        calls:
            - [setMatrixOrderManager, ['@Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager']]
            - [setTwigRenderer, ['@twig.form.renderer']]
        tags:
            - { name: layout.data_provider, alias: xngage_cart_matrix_order_form }

    Xngage\Bundle\CartBundle\Manager\EmptyMatrixGridCartManager:
        public: true
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@Xngage\Bundle\CartBundle\CartLineItem\Factory\CartLineItemFactory'
            - '@oro_config.manager'

    Xngage\Bundle\CartBundle\CartLineItem\Factory\CartLineItemFactory: ~

    Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager:
        arguments:
            - '@property_accessor'
            - '@oro_product.provider.product_variant_availability_provider'
            - '@Xngage\Bundle\CartBundle\Manager\EmptyMatrixGridCartManager'
            - '@Xngage\Bundle\CartBundle\CartLineItem\Factory\CartLineItemFactory'

    Xngage\Bundle\CartBundle\DataProvider\CartLineItemsDataProvider:
        arguments:
            - '@doctrine'

    Xngage\Bundle\CartBundle\Layout\DataProvider\FrontendCartProductsProvider:
        arguments:
            - '@oro_pricing.provider.frontend_product_prices'
            - '@Xngage\Bundle\CartBundle\DataProvider\CartLineItemsDataProvider'
            - '@oro_pricing.formatter.product_price_formatter'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_products }

    Xngage\Bundle\CartBundle\Layout\DataProvider\CartFormProvider:
        arguments:
            - "@form.factory"
            - '@router'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_cart_form }

    Xngage\Bundle\CartBundle\Layout\DataProvider\CartTotalsDataProvider:
        arguments:
            - '@oro_pricing.subtotal_processor.total_processor_provider'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_totals }

    Xngage\Bundle\CartBundle\Layout\DataProvider\MatrixGridOrderCartProvider:
        arguments:
            - '@Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager'
            - '@oro_pricing.subtotal_processor.total_processor_provider'
            - '@oro_locale.formatter.number'
            - '@Xngage\Bundle\CartBundle\Manager\CurrentCartManager'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_matrix_grid_order }

    Xngage\Bundle\CartBundle\Layout\DataProvider\CustomerUserCartProvider:
        arguments:
            - '@Xngage\Bundle\CartBundle\Manager\CurrentCartManager'
            - '@Xngage\Bundle\CartBundle\Manager\CartTotalManager'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_customer_user_cart }

    Xngage\Bundle\CartBundle\Layout\DataProvider\FrontendCartProductUnitsQuantityProvider:
        arguments:
            - '@Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider'
        tags:
            - { name: layout.data_provider, alias: xngage_cart_product_units_quantity }

    Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider:
        arguments:
            - '@Xngage\Bundle\CartBundle\Manager\CurrentCartManager'
            - '@oro_entity.doctrine_helper'
            - '@oro_security.acl_helper'

    xngage_cart.line_item.manager.api:
        class: 'Oro\Bundle\SoapBundle\Entity\Manager\ApiEntityManager'
        public: true
        parent: oro_soap.manager.entity_manager.abstract
        arguments:
            - 'Xngage\Bundle\CartBundle\Entity\CartLineItem'
            - '@doctrine.orm.entity_manager'

    Xngage\Bundle\CartBundle\EventListener\FrontendProductDatagridListener:
        arguments:
            - '@Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider'
            - '@oro_entity.doctrine_helper'
        tags:
            - { name: kernel.event_listener, event: oro_datagrid.datagrid.build.pre.frontend-product-search-grid, method: onPreBuild }
            - { name: kernel.event_listener, event: oro_datagrid.search_datasource.result.after.frontend-product-search-grid, method: onResultAfter }

    Xngage\Bundle\CartBundle\CartLineItem\LineItemMapper: ~

    Xngage\Bundle\CartBundle\Processor\CartQuickAddProcessor:
        arguments:
            - '@Xngage\Bundle\CartBundle\Handler\CartLineItemHandler'
            - '@Xngage\Bundle\CartBundle\Generator\MessageGenerator'
            - '@oro_security.acl_helper'
            - '@oro_entity.doctrine_helper'
        tags:
            - { name: oro_product.quick_add_processor }

    Xngage\Bundle\CartBundle\Generator\MessageGenerator:
        arguments:
            - '@translator'
            - '@router'
            - '@Oro\Bundle\ShoppingListBundle\Provider\ShoppingListUrlProvider'
            - '@Xngage\Bundle\CartBundle\Provider\CartUrlProvider'

    Xngage\Bundle\CartBundle\Authorization\CartAuthorizationChecker:
        arguments:
            - '@security.authorization_checker'
            - '@oro_security.token_accessor'
        tags:
            - { name: oro_featuretogle.feature, feature: 'guest_cart' }

    Xngage\Bundle\CartBundle\Handler\CartLineItemHandler:
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@Xngage\Bundle\CartBundle\Manager\CartManager'
            - '@Xngage\Bundle\CartBundle\Manager\CurrentCartManager'
            - '@Xngage\Bundle\CartBundle\Authorization\CartAuthorizationChecker'
            - '@oro_product.product.manager'
            - '@oro_security.acl_helper'

    Xngage\Bundle\CartBundle\Converter\CartLineItemConverter:
        tags:
            - { name: oro.checkout.line_item.converter, alias: cart }

    Xngage\Bundle\CartBundle\DataProvider\CartInstantExportDataProvider:
        arguments:
            - '@oro_pricing.provider.frontend_product_prices'
            - '@translator'
            - '@oro_frontend_localization.manager.user_localization'
            - '@oro_locale.formatter.date_time'

    Xngage\Bundle\CartBundle\Handler\FileSheetHandler:
        public: true
        arguments:
            - '/tmp'
        calls:
            - ['setLogger', ['@logger']]

    Xngage\Bundle\CartBundle\Manager\GuestCartMigrationManager:
        arguments:
            - '@Xngage\Bundle\CartBundle\Manager\CartManager'
            - '@oro_entity.doctrine_helper'

    Xngage\Bundle\CartBundle\EventListener\InteractiveLoginListener:
        arguments:
            - '@oro_customer.customer_visitor_manager'
            - '@Xngage\Bundle\CartBundle\Manager\GuestCartMigrationManager'
            - '@logger'
            - '@oro_config.manager'
            - '@oro_dataaudit.listener.send_changed_entities_to_message_queue'
        tags:
            - { name: kernel.event_listener, event: security.interactive_login, method: onInteractiveLogin }

    Xngage\Bundle\CartBundle\Async\InvalidateTotalsByInventoryStatusPerWebsiteProcessor:
        decorates: oro_shopping_list.async.invalidate_totals_by_inventory_status_per_website_processor
        arguments:
            - '@oro_config.manager'
            - '@oro_website.website.provider'
            - '@doctrine'
            - '@oro_shopping_list.async.message_factory'
            - '@logger'

    Xngage\Bundle\CartBundle\Async\InvalidateTotalsByInventoryStatusPerProductProcessor:
        decorates: oro_shopping_list.async.invalidate_totals_by_inventory_status_per_product_processor
        arguments:
            - '@doctrine'
            - '@oro_shopping_list.async.message_factory'
            - '@logger'

    Xngage\Bundle\CartBundle\EventListener\Entity\RemoveParentProductsFromCartLineItemListener:
        public: false
        tags:
            - { name: doctrine.orm.entity_listener, entity: 'Xngage\Bundle\CartBundle\Entity\CartLineItem', event: prePersist }

    Xngage\Bundle\CartBundle\Provider\CartUrlProvider:
        public: true
        arguments:
            - '@security.authorization_checker'
            - '@router'

    Xngage\Bundle\CartBundle\Handler\CartLineItemBatchUpdateHandler:
        public: true
        arguments:
            - '@oro_entity.doctrine_helper'
            - '@Xngage\Bundle\CartBundle\Manager\CartManager'
            - '@validator'

    Xngage\Bundle\CartBundle\Form\Handler\CartMatrixGridOrderFormHandler:
        arguments:
            - '@oro_form.event.event_dispatcher'
            - '@oro_entity.doctrine_helper'
            - '@Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager'
            - '@Xngage\Bundle\CartBundle\Manager\CartManager'

    Xngage\Bundle\CartBundle\Datagrid\EventListener\FrontendLineItemsGrid\CartLineItemsActionsOnResultAfterListener:
        arguments:
            - '@security.authorization_checker'
        tags:
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-cart-edit-grid, method: onResultAfter, priority: 128 }

    Xngage\Bundle\CartBundle\Datagrid\Extension\FrontendCartLineItemsGridExtension:
        arguments:
            - '@doctrine'
            - '@oro_config.manager'
            - '@oro_security.token_accessor'
        tags:
            - { name: oro_datagrid.extension, priority: 10 }

    Xngage\Bundle\CartBundle\Validator\Constraints\CartLineItemValidator:
        arguments:
            - "@doctrine"
        tags:
            - { name: validator.constraint_validator, alias: xngage_cart_line_item_validator }

    Xngage\Bundle\CartBundle\Twig\CartUrlExtension:
        arguments:
            - "@oro_platform.twig.service_locator"
        tags:
            - { name: twig.extension }

# override core definitions to add tags
    oro_shopping_list.extension.action.type.delete_product:
        class: 'Oro\Bundle\ShoppingListBundle\Datagrid\Extension\Action\Actions\DeleteProductAction'
        shared: false
        tags:
            - { name: oro_datagrid.extension.action.type, type: delete-product }
            - { name: oro_datagrid.extension.action.type, type: delete-cart-product }

    oro_shopping_list.extension.action.type.add_notes:
        class: 'Oro\Bundle\ShoppingListBundle\Datagrid\Extension\Action\Actions\AddNotesAction'
        shared: false
        tags:
            - { name: oro_datagrid.extension.action.type, type: add-notes }
            - { name: oro_datagrid.extension.action.type, type: add-cart-notes }

    oro_shopping_list.extension.action.type.update_configurable_product:
        class: 'Oro\Bundle\ShoppingListBundle\Datagrid\Extension\Action\Actions\UpdateConfigurableProductAction'
        shared: false
        tags:
            - { name: oro_datagrid.extension.action.type, type: update-configurable-product }
            - { name: oro_datagrid.extension.action.type, type: update-cart-configurable-product }

    oro_shopping_list.datagrid.event_listener.frontend_line_items_grid.data:
        class: Oro\Bundle\ProductBundle\DataGrid\EventListener\FrontendLineItemsGrid\LineItemsDataOnResultAfterListener
        arguments:
            - '@event_dispatcher'
            - '@oro_entity.orm.entity_class_resolver'
        tags:
            # Should be executed earlier than others because it adds data required by other listeners.
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-shopping-list-grid, method: onResultAfter, priority: 1024 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-checkout-line-items-grid, method: onResultAfter, priority: 1024 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-cart-grid, method: onResultAfter, priority: 1024 }

    oro_shopping_list.datagrid.event_listener.frontend_line_items_grid.simple:
        class: Oro\Bundle\ProductBundle\DataGrid\EventListener\FrontendLineItemsGrid\LineItemsSimpleOnResultAfterListener
        tags:
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-shopping-list-grid, method: onResultAfter, priority: 768 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-checkout-line-items-grid, method: onResultAfter, priority: 768 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-cart-grid, method: onResultAfter, priority: 768 }

    oro_shopping_list.datagrid.event_listener.frontend_line_items_grid.grouped:
        class: Oro\Bundle\ProductBundle\DataGrid\EventListener\FrontendLineItemsGrid\LineItemsGroupedOnResultAfterListener
        arguments:
            - '@oro_attachment.manager'
            - '@oro_locale.formatter.number'
        tags:
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-shopping-list-grid, method: onResultAfter, priority: 512 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-checkout-line-items-grid, method: onResultAfter, priority: 512 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-cart-grid, method: onResultAfter, priority: 512 }

    oro_shopping_list.datagrid.event_listener.frontend_line_items_grid.matrix_form:
        class: Oro\Bundle\ProductBundle\DataGrid\EventListener\FrontendLineItemsGrid\LineItemsMatrixFormOnResultAfterListener
        arguments:
            - '@oro_product.provider.product_matrix_availability_provider'
        tags:
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-shopping-list-edit-grid, method: onResultAfter, priority: 256 }
            - { name: kernel.event_listener, event: oro_datagrid.orm_datasource.result.after.frontend-customer-user-cart-edit-grid, method: onResultAfter, priority: 256 }
