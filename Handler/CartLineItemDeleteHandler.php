<?php

namespace Xngage\Bundle\CartBundle\Handler;

use Oro\Bundle\EntityBundle\Handler\AbstractEntityDeleteHandler;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;

/**
 * The delete handler for cart LineItem entity.
 */
class CartLineItemDeleteHandler extends AbstractEntityDeleteHandler
{
    protected CartTotalManager $totalManager;

    public function __construct(CartTotalManager $totalManager)
    {
        $this->totalManager = $totalManager;
    }

    /**
     * {@inheritdoc}
     */
    public function flush(array $options): void
    {
        /** @var CartLineItem $lineItem */
        $lineItem = $options[self::ENTITY];

        $cart = $lineItem->getCart();
        if (null !== $cart) {
            $this->totalManager->recalculateTotals($cart, false);
        }

        parent::flush($options);
    }

    /**
     * {@inheritdoc}
     */
    public function flushAll(array $listOfOptions): void
    {
        $processedCarts = [];
        foreach ($listOfOptions as $options) {
            /** @var CartLineItem $lineItem */
            $lineItem = $options[self::ENTITY];
            
            $cart = $lineItem->getCart();
            if (null !== $cart) {
                $cartHash = spl_object_hash($cart);
                if (!isset($processedCarts[$cartHash])) {
                    $this->totalManager->recalculateTotals($cart, false);
                    $processedCarts[$cartHash] = true;
                }
            }
        }

        parent::flushAll($listOfOptions);
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteWithoutFlush($entity, array $options): void
    {
        /** @var CartLineItem $entity */
        $cart = $entity->getCart();
        if (null !== $cart) {
            $cart->removeLineItem($entity);
        }

        parent::deleteWithoutFlush($entity, $options);
    }
}
