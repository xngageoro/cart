<?php

namespace Xngage\Bundle\CartBundle\Handler;

use Doctrine\ORM\Internal\Hydration\IterableResult;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Manager\ProductManager;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\ProductBundle\Entity\Repository\ProductRepository;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Xngage\Bundle\CartBundle\Authorization\CartAuthorizationChecker;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Handles batch adding products to the cart.
 */
class CartLineItemHandler
{
    public const FLUSH_BATCH_SIZE = 100;

    protected DoctrineHelper $doctrineHelper;
    protected CartManager $cartManager;
    protected CurrentCartManager $currentCartManager;
    protected CartAuthorizationChecker $cartAuthorizationChecker;
    protected ProductManager $productManager;
    protected AclHelper $aclHelper;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        CartManager $cartManager,
        CurrentCartManager $currentCartManager,
        CartAuthorizationChecker $cartAuthorizationChecker,
        ProductManager $productManager,
        AclHelper $aclHelper
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->cartManager = $cartManager;
        $this->currentCartManager = $currentCartManager;
        $this->cartAuthorizationChecker = $cartAuthorizationChecker;
        $this->productManager = $productManager;
        $this->aclHelper = $aclHelper;
    }

    /**
     * @return bool
     */
    public function isAllowed(): bool
    {
        return $this->cartAuthorizationChecker->isCartEditAllowed();
    }

    /**
     * @param array $productIds
     * @param array $productUnitsWithQuantities
     *
     * @return int Added entities count
     */
    public function addFromQuickOrderForm(
        array $productIds = [],
        array $productUnitsWithQuantities = []
    ): int {
        if (!$this->cartAuthorizationChecker->isCartEditAllowed()) {
            throw new AccessDeniedException();
        }

        $cartLineItems = $this->prepareCartLineItems(
            $this->getProductsIterable($productIds),
            $productUnitsWithQuantities
        );

        return $this->cartManager->bulkAddLineItems($cartLineItems, self::FLUSH_BATCH_SIZE);
    }

    /**
     * @param array $productIds
     *
     * @return IterableResult
     */
    protected function getProductsIterable(array $productIds = []): IterableResult
    {
        /** @var ProductRepository $productsRepository */
        $productsRepository = $this->doctrineHelper->getEntityRepositoryForClass(Product::class);
        $queryBuilder = $this->productManager->restrictQueryBuilder(
            $productsRepository->getProductsQueryBuilder($productIds),
            []
        );

        return $this->aclHelper->apply($queryBuilder)->iterate();
    }

    /**
     * @param IterableResult $productsIterable
     * @param array $productUnitsWithQuantities
     *
     * @return CartLineItem[]
     */
    protected function prepareCartLineItems(
        IterableResult $productsIterable,
        array $productUnitsWithQuantities = []
    ): array {
        $cart = $this->currentCartManager->getCurrent();
        $productUnitsWithQuantities = array_combine(
            array_map('mb_strtoupper', array_keys($productUnitsWithQuantities)),
            array_values($productUnitsWithQuantities)
        );

        $cartLineItems = [];
        foreach ($productsIterable as $entityArray) {
            /** @var Product $product */
            $product = reset($entityArray);
            $upperSku = mb_strtoupper($product->getSku());

            if (isset($productUnitsWithQuantities[$upperSku])) {
                $productCartLineItems = $this->createCartLineItemsWithQuantityAndUnit(
                    $cart,
                    $product,
                    $productUnitsWithQuantities[$upperSku]
                );

                if ($productCartLineItems) {
                    $cartLineItems = array_merge($cartLineItems, $productCartLineItems);
                }

                continue;
            }

            $cartLineItems[] = $this->createCartLineItem(
                $cart,
                $product,
                $product->getPrimaryUnitPrecision()->getUnit()
            );
        }

        return $cartLineItems;
    }

    /**
     * @param Cart $cart
     * @param Product $product
     * @param array $unitsWithQuantities
     *
     * @return CartLineItem[]
     */
    protected function createCartLineItemsWithQuantityAndUnit(
        Cart $cart,
        Product $product,
        array $unitsWithQuantities
    ): array {
        $cartLineItems = [];

        $productUnitRepository = $this->doctrineHelper->getEntityRepositoryForClass(ProductUnit::class);

        foreach ($unitsWithQuantities as $unit => $quantity) {
            /** @var ProductUnit $unitEntity */
            $unitEntity = $productUnitRepository->findOneBy(['code' => $unit]);

            if ($unitEntity !== null) {
                $cartLineItems[] = $this->createCartLineItem(
                    $cart,
                    $product,
                    $unitEntity,
                    $quantity
                );
            }
        }

        return $cartLineItems;
    }

    /**
     * @param Cart $cart
     * @param Product $product
     * @param ProductUnit $unit
     * @param int $quantity
     *
     * @return CartLineItem
     */
    protected function createCartLineItem(
        Cart $cart,
        Product $product,
        ProductUnit $unit,
        int $quantity = null
    ): CartLineItem {
        return (new CartLineItem())
            ->setCustomerUser($cart->getCustomerUser())
            ->setOrganization($cart->getOrganization())
            ->setProduct($product)
            ->setUnit($unit)
            ->setQuantity($quantity);
    }
}
