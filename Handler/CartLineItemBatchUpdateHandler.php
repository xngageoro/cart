<?php

namespace Xngage\Bundle\CartBundle\Handler;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\ProductBundle\Entity\Repository\ProductUnitRepository;
use Oro\Bundle\ShoppingListBundle\Model\LineItemModel;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * The handler for the line item batch update.
 */
class CartLineItemBatchUpdateHandler
{
    private DoctrineHelper $doctrineHelper;
    private CartManager $cartManager;
    private ValidatorInterface $validator;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        CartManager $cartManager,
        ValidatorInterface $validator
    ) {
        $this->cartManager = $cartManager;
        $this->doctrineHelper = $doctrineHelper;
        $this->validator = $validator;
    }

    /**
     * @param LineItemModel[] $collection
     * @param Cart $cart
     * @return array
     */
    public function process(array $collection, Cart $cart): array
    {
        $lineItems = $this->getLineItems(
            array_map(
                static function (LineItemModel $model) {
                    return $model->getId();
                },
                $collection
            )
        );

        $products = [];
        $unitCodes = [];

        foreach ($collection as $lineItemModel) {
            if (isset($lineItems[$lineItemModel->getId()])) {
                $lineItem = $lineItems[$lineItemModel->getId()];

                $products[] = $lineItem->getProduct();
                $unitCodes[] = $lineItemModel->getUnitCode();
            }
        }

        /** @var ProductUnitRepository $repository */
        $repository = $this->doctrineHelper->getEntityRepository(ProductUnit::class);
        $productUnits = $repository->getProductsUnitsByCodes($products, $unitCodes);

        foreach ($collection as $lineItemModel) {
            if (isset($lineItems[$lineItemModel->getId()], $productUnits[$lineItemModel->getUnitCode()])) {
                $lineItem = $lineItems[$lineItemModel->getId()];
                $lineItem->setQuantity($lineItemModel->getQuantity());
                $lineItem->setUnit($productUnits[$lineItemModel->getUnitCode()]);

                $this->cartManager->addLineItem($lineItem, $cart, false, true);
            }
        }

        $errors = $this->getCartErrors($cart);
        if (!$errors) {
            $manager = $this->doctrineHelper->getEntityManagerForClass(Cart::class);
            $manager->flush();
        }

        return $errors;
    }

    /**
     * @param array $ids
     * @return CartLineItem[]
     */
    private function getLineItems(array $ids): array
    {
        $repository = $this->doctrineHelper->getEntityRepository(CartLineItem::class);

        $data = [];
        foreach ($repository->findBy(['id' => $ids]) as $item) {
            $data[$item->getId()] = $item;
        }

        return $data;
    }

    /**
     * @param Cart $cart
     * @return array
     */
    private function getCartErrors(Cart $cart): array
    {
        $constraintViolationList = $this->validator->validate($cart);

        if ($constraintViolationList->count()) {
            return array_map(
                static function (ConstraintViolation $constraintViolation) {
                    return $constraintViolation->getMessage();
                },
                iterator_to_array($constraintViolationList)
            );
        }

        return [];
    }
}
