<?php

namespace Xngage\Bundle\CartBundle\Handler;

use Box\Spout\Common\Exception\InvalidArgumentException;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Xngage\Bundle\CartBundle\Exception\FileSheetException;
use Psr\Log\LoggerAwareTrait;

class FileSheetHandler
{
    use LoggerAwareTrait;

    public string $tempDir;

    public function __construct(string $tempDir)
    {
        $this->tempDir = $tempDir;
    }

    /**
     * @param string $type
     * @param array  $rowData
     * @param array  $headerRowData
     *
     * @return false|string
     * @throws FileSheetException
     */
    public function writeFile(string $type, array $rowData, array $headerRowData = [])
    {
        try {
            $this->logger->info('Creating ' . $type);
            $writer = WriterEntityFactory::createWriter($type);
        } catch (UnsupportedTypeException $e) {
            $this->logger->error('Error creating ' . $type, [ 'exception' => $e ]);
            throw new FileSheetException($e);
        }

        // If it is a xlsx and not a csv we need a temporary file
        if ($type == Type::XLSX) {
            $writer->setTempFolder($this->tempDir);
        }

        ob_start();
        try {
            $writer->openToFile('php://output');
        } catch (IOException $e) {
            ob_end_clean();
            $this->logger->error('Error generating ' . $type, [ 'exception' => $e ]);
            throw new FileSheetException($e);
        }

        try {
            $writer->setDefaultRowStyle((new StyleBuilder())->setFontBold()->build());
            $writer->addRow(WriterEntityFactory::createRowFromArray($headerRowData));
            $writer->setDefaultRowStyle((new StyleBuilder())->build());
            foreach ($rowData as $row) {
                $writer->addRow(WriterEntityFactory::createRowFromArray($row));
            }
            $writer->close();
        } catch (InvalidArgumentException | IOException | WriterNotOpenedException $e) {
            ob_end_clean();
            $this->logger->error('Error writing ' . $type, [ 'exception' => $e ]);
            throw new FileSheetException($e);
        }

        return ob_get_clean();
    }
}
