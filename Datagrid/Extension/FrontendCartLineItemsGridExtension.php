<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration;
use Oro\Bundle\DataGridBundle\Datagrid\Common\MetadataObject;
use Oro\Bundle\DataGridBundle\Datagrid\Common\ResultsObject;
use Oro\Bundle\DataGridBundle\Datagrid\ParameterBag;
use Oro\Bundle\DataGridBundle\Datasource\Orm\OrmQueryConfiguration;
use Oro\Bundle\DataGridBundle\Extension\AbstractExtension;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

/**
 * Adds additional configuration and metadata to the grid.
 */
class FrontendCartLineItemsGridExtension extends AbstractExtension
{
    /** @var string[] */
    protected const SUPPORTED_GRIDS = [
        'frontend-customer-user-cart-grid',
        'frontend-customer-user-cart-edit-grid',
    ];

    protected ManagerRegistry $registry;
    protected ConfigManager $configManager;
    protected TokenAccessorInterface $tokenAccessor;
    protected array $cache = [];

    public function __construct(
        ManagerRegistry $registry,
        ConfigManager $configManager,
        TokenAccessorInterface $tokenAccessor
    ) {
        $this->registry = $registry;
        $this->configManager = $configManager;
        $this->tokenAccessor = $tokenAccessor;
    }

    /**
     * {@inheritdoc}
     */
    public function isApplicable(DatagridConfiguration $config): bool
    {
        return \in_array($config->getName(), static::SUPPORTED_GRIDS, true) && parent::isApplicable($config);
    }

    /**
     * {@inheritdoc}
     */
    public function setParameters(ParameterBag $parameters): void
    {
        if ($parameters->has(ParameterBag::MINIFIED_PARAMETERS)) {
            $minifiedParameters = $parameters->get(ParameterBag::MINIFIED_PARAMETERS);
            $additional = $parameters->get(ParameterBag::ADDITIONAL_PARAMETERS, []);

            if (array_key_exists('g', $minifiedParameters)) {
                $additional['group'] = $minifiedParameters['g']['group'] ?? false;
            }

            $parameters->set(ParameterBag::ADDITIONAL_PARAMETERS, $additional);
        }

        parent::setParameters($parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function processConfigs(DatagridConfiguration $config): void
    {
        $queryPart = 'lineItem.id';
        if ($this->isLineItemsGrouped()) {
            $queryPart = '(SELECT GROUP_CONCAT(innerItem.id ORDER BY innerItem.id ASC) ' .
                'FROM Xngage\Bundle\CartBundle\Entity\CartLineItem innerItem ' .
                'WHERE (innerItem.parentProduct = lineItem.parentProduct OR innerItem.product = lineItem.product) ' .
                'AND innerItem.cart = lineItem.cart ' .
                'AND innerItem.unit = lineItem.unit) as allLineItemsIds';
        }
        $config->offsetAddToArrayByPath(OrmQueryConfiguration::SELECT_PATH, [$queryPart]);

        $cartId = $this->getCartId();
        if (!$cartId) {
            return;
        }

        $cart = $this->getCart($cartId);
        $lineItemsCount = $cart ? count($cart->getLineItems()) : 0;
        $item = $this->configManager->get('xngage_cart.cart_max_line_items_per_page');
        if ($lineItemsCount <= $item) {
            $item = [
                'label' => 'xngage.cart.datagrid.toolbar.pageSize.all.label',
                'size' => $item
            ];
        }

        $config->offsetSetByPath(
            '[options][toolbarOptions][pageSize][items]',
            array_merge(
                $config->offsetGetByPath('[options][toolbarOptions][pageSize][items]'),
                [$item]
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function visitMetadata(DatagridConfiguration $config, MetadataObject $data): void
    {
        $cartId = $this->getCartId();
        if (!$cartId) {
            return;
        }

        $data->offsetSetByPath('[hasEmptyMatrix]', $this->hasEmptyMatrix($cartId));
        $data->offsetSetByPath('[canBeGrouped]', $this->canBeGrouped($cartId));
        $data->offsetSetByPath('[cartLabel]', $this->getCart($cartId)?->getLabel());
        $data->offsetAddToArrayByPath('[initialState][parameters]', ['group' => false]);
        $data->offsetAddToArrayByPath('[state][parameters]', ['group' => $this->isLineItemsGrouped()]);
    }

    /**
     * {@inheritdoc}
     */
    public function visitResult(DatagridConfiguration $config, ResultsObject $result): void
    {
        $cartId = $this->getCartId();
        if (!$cartId) {
            return;
        }

        $result->offsetAddToArrayByPath(
            '[metadata]',
            [
                'hasEmptyMatrix' => $this->hasEmptyMatrix($cartId),
                'canBeGrouped' => $this->canBeGrouped($cartId),
            ]
        );
    }

    /**
     * @return int
     */
    protected function getCartId(): int
    {
        return (int) $this->parameters->get('cart_id');
    }

    /**
     * @return bool
     */
    protected function isLineItemsGrouped(): bool
    {
        $parameters = $this->parameters->get('_parameters', []);

        return isset($parameters['group']) ? filter_var($parameters['group'], FILTER_VALIDATE_BOOLEAN) : false;
    }

    /**
     * @param int $cartId
     * @return bool
     */
    protected function hasEmptyMatrix(int $cartId): bool
    {
        if (!isset($this->cache['hasEmptyMatrix'][$cartId])) {
            $this->cache['hasEmptyMatrix'][$cartId] = $this->registry
                ->getRepository(CartLineItem::class)
                ->hasEmptyMatrix($cartId);
        }

        return $this->cache['hasEmptyMatrix'][$cartId];
    }

    /**
     * @param int $cartId
     * @return bool
     */
    protected function canBeGrouped(int $cartId): bool
    {
        if (!isset($this->cache['canBeGrouped'][$cartId])) {
            $this->cache['canBeGrouped'][$cartId] = $this->registry
                ->getRepository(CartLineItem::class)
                ->canBeGrouped($cartId);
        }

        return $this->cache['canBeGrouped'][$cartId];
    }

    /**
     * @param int $cartId
     * @return Cart|null
     */
    protected function getCart(int $cartId): ?Cart
    {
        if (!isset($this->cache['cart'][$cartId])) {
            $this->cache['cart'][$cartId] = $this->registry
                ->getRepository(Cart::class)
                ->find($cartId);
        }

        return $this->cache['cart'][$cartId];
    }
}
