<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension\MassAction;

use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Oro\Bundle\BatchBundle\ORM\Query\ResultIterator\IdentifierWithoutOrderByIterationStrategy;
use Oro\Bundle\DataGridBundle\Datasource\Orm\IterableResult;
use Oro\Bundle\DataGridBundle\Datasource\ResultRecordInterface;
use Oro\Bundle\DataGridBundle\Exception\LogicException;
use Oro\Bundle\DataGridBundle\Extension\MassAction\Actions\MassActionInterface;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerArgs;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerInterface;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionResponse;
use Oro\Bundle\ProductBundle\Model\ProductLineItemInterface;
use Xngage\Bundle\CartBundle\CartLineItem\LineItemMapper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * DataGrid mass action base handler that move/add products between shopping list <-> cart.
 */
abstract class AbstractMoveCartProductsMassActionHandler implements MassActionHandlerInterface
{
    protected const FLUSH_BATCH_SIZE = 100;
    protected const TRANSLATION_NO_ACCESS_RESPONSE = 'xngage.cart.mass_actions.move_line_items.no_edit_permission_message'; // @codingStandardsIgnoreLine
    protected const TRANSLATION_MOVE_SUCCESS = 'xngage.cart.mass_actions.move_line_items.success_message';

    public const STRATEGY_ADD = 'add';
    public const STRATEGY_MOVE = 'move';

    /** @var ManagerRegistry */
    protected $registry;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /** @var RequestStack */
    protected $requestStack;

    /** @var string */
    protected $sourceLineItemClass = null;

    /** @var LineItemMapper */
    protected $lineItemMapper;

    /** @var string */
    protected $strategy = self::STRATEGY_MOVE;

    /**
     * @param ManagerRegistry $registry
     * @param TranslatorInterface $translator
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RequestStack $requestStack
     * @param LineItemMapper $lineItemMapper
     */
    public function __construct(
        ManagerRegistry $registry,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
        RequestStack $requestStack,
        LineItemMapper $lineItemMapper
    ) {
        $this->registry = $registry;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
        $this->requestStack = $requestStack;
        $this->lineItemMapper = $lineItemMapper;
    }

    /**
     * Sets strategy property
     *
     * @param string $strategy
     * @return self
     */
    public function setStrategy(string $strategy): self
    {
        if (!in_array($strategy, ($availableStrategies = [self::STRATEGY_MOVE, self::STRATEGY_ADD]))) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid strategy %s. Available: %s',
                $strategy,
                implode(', ', $availableStrategies)
            ));
        }

        $this->strategy = $strategy;
        return $this;
    }

    /**
     * Sets sourceLineItemClass property
     *
     * @param string $sourceLineItemClass
     * @return self
     */
    public function setSourceLineItemClass(string $sourceLineItemClass): self
    {
        $this->sourceLineItemClass = $sourceLineItemClass;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(MassActionHandlerArgs $args)
    {
        $request = $this->requestStack->getMainRequest();
        if ($request?->getMethod() === $this->getHandlerHttpMethod()) {
            $result = $this->doHandle($args);
        } else {
            $result = new MassActionResponse(
                false,
                sprintf('Request method "%s" is not supported', $request?->getMethod())
            );
        }

        return $result;
    }

    /**
     * Returns handler HTTP method
     *
     * @return string
     */
    abstract protected function getHandlerHttpMethod(): string;

    /**
     * @param MassActionHandlerArgs $args
     * @return MassActionResponse
     */
    protected function doHandle(MassActionHandlerArgs $args): MassActionResponse
    {
        $targetEntity = $this->getTargetEntity($args);
        if (!$this->isEditAllowed($targetEntity)) {
            return $this->getNoAccessResponse();
        }

        $results = new IterableResult($args->getResults()->getSource());
        $results->setIterationStrategy(new IdentifierWithoutOrderByIterationStrategy());
        $results->setBufferSize(static::FLUSH_BATCH_SIZE);

        $entityIdentifiedField = $this->getEntityIdentifierField($args);
        /** @var EntityManager $manager */
        $manager = $this->registry->getManagerForClass($this->sourceLineItemClass);
        $updated = 0;
        $affectedEntities = [];

        /** @var ResultRecordInterface[] $results */
        foreach ($results as $result) {
            $originalLineItem = $result->getRootEntity();
            if (!$originalLineItem) {
                // No entity in result record, it should be extracted from DB.
                $originalLineItem = $manager->getReference(
                    $this->sourceLineItemClass,
                    $result->getValue($entityIdentifiedField)
                );
            }

            if (!$originalLineItem) {
                continue;
            }

            $originalEntity = $this->getOriginalEntity($originalLineItem);
            if (!$this->isEditAllowed($originalEntity)) {
                continue;
            }

            $this->removeOriginalEntityLineItem($originalEntity, $originalLineItem);
            $affectedEntities[$originalEntity->getId()] = $originalEntity;
            $this->addLineItem($targetEntity, $originalLineItem);

            $updated++;
            if ($updated % static::FLUSH_BATCH_SIZE === 0) {
                $manager->flush();
            }
        }

        if ($updated % static::FLUSH_BATCH_SIZE > 0) {
            $manager->flush();
        }

        $this->recalculateTotals($manager, $affectedEntities);

        return $this->getResponse($args->getMassAction(), $updated);
    }

    /**
     * @param mixed $originalEntity
     * @param ProductLineItemInterface $originalLineItem
     */
    protected function removeOriginalEntityLineItem(
        $originalEntity,
        ProductLineItemInterface $originalLineItem
    ): void {
        if ($this->strategy === self::STRATEGY_MOVE) {
            $this->doRemoveOriginalEntityLineItem($originalEntity, $originalLineItem);
        }
    }

    /**
     * @param ObjectManager $manager
     * @param array $affectedEntities
     */
    abstract protected function recalculateTotals(ObjectManager $manager, array $affectedEntities): void;

    /**
     * Returns original source entity for given line item
     *
     * @param mixed $lineItem
     * @return mixed
     */
    abstract protected function getOriginalEntity($lineItem);

    /**
     * Returns target entity to copy line items to
     *
     * @param MassActionHandlerArgs $args
     * @return object|null
     */
    abstract protected function getTargetEntity(MassActionHandlerArgs $args);

    /**
     * Removes original entity line item
     *
     * @param object $originalEntity
     * @param object|ProductLineItemInterface $originalLineItem
     * @return mixed
     */
    abstract protected function doRemoveOriginalEntityLineItem(
        $originalEntity,
        ProductLineItemInterface $originalLineItem
    ): void;

    /**
     * Adds line item to target entity
     *
     * @param object $targetEntity
     * @param object|ProductLineItemInterface $originalLineItem
     */
    abstract protected function addLineItem($targetEntity, ProductLineItemInterface $originalLineItem): void;

    /**
     * @param null|object $targetEntity
     * @return bool
     */
    protected function isEditAllowed($targetEntity): bool
    {
        return $targetEntity && $this->authorizationChecker->isGranted('EDIT', $targetEntity);
    }

    /**
     * @param MassActionInterface $massAction
     * @param int $entitiesCount
     * @return MassActionResponse
     */
    protected function getResponse(MassActionInterface $massAction, $entitiesCount = 0): MassActionResponse
    {
        $responseMessage = $massAction->getOptions()
            ->offsetGetByPath('[messages][success]', static::TRANSLATION_MOVE_SUCCESS);

        return new MassActionResponse(
            $entitiesCount > 0,
            $this->translator->trans($responseMessage, ['%count%' => $entitiesCount]),
            ['count' => $entitiesCount]
        );
    }

    /**
     * @return MassActionResponse
     */
    protected function getNoAccessResponse(): MassActionResponse
    {
        return new MassActionResponse(
            false,
            $this->translator->trans(static::TRANSLATION_NO_ACCESS_RESPONSE)
        );
    }

    /**
     * @param MassActionHandlerArgs $args
     * @throws LogicException
     * @return string
     */
    protected function getEntityIdentifierField(MassActionHandlerArgs $args): string
    {
        $massAction = $args->getMassAction();
        $identifier = $massAction->getOptions()->offsetGet('data_identifier');
        if (!$identifier) {
            throw new LogicException(sprintf('Mass action "%s" must define identifier name', $massAction->getName()));
        }
        if (strpos('.', $identifier) !== -1) {
            $parts      = explode('.', $identifier);
            $identifier = end($parts);
        }

        return $identifier;
    }
}
