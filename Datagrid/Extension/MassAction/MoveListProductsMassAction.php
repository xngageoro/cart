<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension\MassAction;

use Oro\Bundle\DataGridBundle\Extension\Action\ActionConfiguration;
use Oro\Bundle\DataGridBundle\Extension\MassAction\Actions\Ajax\AjaxMassAction;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines default options for moving/adding line items from Shopping List to the cart mass action.
 */
class MoveListProductsMassAction extends AjaxMassAction
{
    public const DEFAULT_HANDLER = 'xngage_cart.mass_action.move_list_to_cart_products_handler';
    public const DEFAULT_FRONTEND_ROUTE = 'xngage_cart_frontend_move_mass_action';
    public const DEFAULT_FRONTEND_ROUTE_PARAMS = [];
    public const FRONTEND_TYPE = 'move-list-to-cart-products-mass';

    /**
     * {@inheritdoc}
     */
    public function setOptions(ActionConfiguration $options)
    {
        if (empty($options['handler'])) {
            $options['handler'] = self::DEFAULT_HANDLER;
        }

        if (!isset($options['entity_name'])) {
            $options['entity_name']  = LineItem::class;
        }

        if (!isset($options['route'])) {
            $options['route'] = self::DEFAULT_FRONTEND_ROUTE;
        }

        if (!isset($options['route_parameters'])) {
            $options['route_parameters'] = self::DEFAULT_FRONTEND_ROUTE_PARAMS;
        }

        $options['confirmation'] = true;
        $options['frontend_type'] = self::FRONTEND_TYPE;

        return parent::setOptions($options);
    }

    /**
     * {@inheritdoc}
     */
    protected function getAllowedRequestTypes()
    {
        return [Request::METHOD_GET];
    }

    /**
     * {@inheritdoc}
     */
    protected function getRequestType()
    {
        return Request::METHOD_GET;
    }
}
