<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension\MassAction;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerArgs;
use Oro\Bundle\ProductBundle\Model\ProductLineItemInterface;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Oro\Bundle\ShoppingListBundle\Manager\ShoppingListTotalManager;
use Xngage\Bundle\CartBundle\CartLineItem\LineItemMapper;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * DataGrid mass action handler that move/add products from shopping list to the cart
 */
class MoveListProductsToCartActionHandler extends AbstractMoveCartProductsMassActionHandler
{
    protected const TRANSLATION_NO_ACCESS_RESPONSE = 'xngage.cart.mass_actions.add_line_items_to_cart.success_message'; // @codingStandardsIgnoreLine
    protected const TRANSLATION_MOVE_SUCCESS = 'oro.shoppinglist.mass_actions.move_line_items.success_message';

    /** @var string */
    protected $sourceLineItemClass = LineItem::class;

    /** @var CartManager */
    private $cartManager;

    /** @var ShoppingListTotalManager */
    private $shoppingListTotalManager;

    /** @var CurrentCartManager */
    private $currentCartManager;

    /**
     * @param ManagerRegistry $registry
     * @param TranslatorInterface $translator
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RequestStack $requestStack
     * @param LineItemMapper $lineItemMapper
     * @param CartManager $cartManager
     * @param ShoppingListTotalManager $shoppingListTotalManager
     * @param CurrentCartManager $currentCartManager
     */
    public function __construct(
        ManagerRegistry $registry,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
        RequestStack $requestStack,
        LineItemMapper $lineItemMapper,
        CartManager $cartManager,
        ShoppingListTotalManager $shoppingListTotalManager,
        CurrentCartManager $currentCartManager
    ) {
        $this->cartManager = $cartManager;
        $this->currentCartManager = $currentCartManager;
        $this->shoppingListTotalManager = $shoppingListTotalManager;
        parent::__construct($registry, $translator, $authorizationChecker, $requestStack, $lineItemMapper);
    }

    /**
     * {@inheritdoc}
     * @return Cart|null
     */
    protected function getTargetEntity(MassActionHandlerArgs $args)
    {
        return $this->currentCartManager->getCurrent();
    }

    /**
     * @param LineItem $lineItem
     * @return ShoppingList
     */
    protected function getOriginalEntity($lineItem)
    {
        return $lineItem->getShoppingList();
    }

    /**
     * @param ShoppingList $originalEntity
     * @param LineItem|ProductLineItemInterface $originalLineItem
     * @return void
     */
    protected function doRemoveOriginalEntityLineItem($originalEntity, ProductLineItemInterface $originalLineItem): void
    {
        $originalEntity->removeLineItem($originalLineItem);
    }

    /**
     * @param Cart $targetEntity
     * @param LineItem|ProductLineItemInterface $originalLineItem
     */
    protected function addLineItem($targetEntity, ProductLineItemInterface $originalLineItem): void
    {
        $cartLineItem = $this->lineItemMapper->mapLineItemToCartLineItem($originalLineItem);
        $this->cartManager->addLineItem($cartLineItem, $targetEntity, false);
    }

    /**
     * @param ObjectManager $manager
     * @param array|ShoppingList[] $shoppingLists
     */
    protected function recalculateTotals(ObjectManager $manager, array $shoppingLists): void
    {
        foreach ($shoppingLists as $shoppingList) {
            $this->shoppingListTotalManager->recalculateTotals($shoppingList, false);
        }
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function getHandlerHttpMethod(): string
    {
        return 'GET';
    }
}
