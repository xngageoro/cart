<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension\MassAction;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerArgs;
use Oro\Bundle\ProductBundle\Model\ProductLineItemInterface;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Oro\Bundle\ShoppingListBundle\Manager\ShoppingListManager;
use Xngage\Bundle\CartBundle\CartLineItem\LineItemMapper;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * DataGrid mass action handler that move/add products from Cart to Shopping List
 */
class MoveCartProductsToListActionHandler extends AbstractMoveCartProductsMassActionHandler
{
    /** @var string */
    protected $sourceLineItemClass = CartLineItem::class;

    /** @var ShoppingListManager */
    private $shoppingListManager;

    /** @var CartTotalManager */
    private $cartTotalsManager;

    /**
     * @param ManagerRegistry $registry
     * @param TranslatorInterface $translator
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RequestStack $requestStack
     * @param LineItemMapper $lineItemMapper
     * @param ShoppingListManager $shoppingListManager
     * @param CartTotalManager $cartTotalManager
     */
    public function __construct(
        ManagerRegistry $registry,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
        RequestStack $requestStack,
        LineItemMapper $lineItemMapper,
        ShoppingListManager $shoppingListManager,
        CartTotalManager $cartTotalManager
    ) {
        $this->shoppingListManager = $shoppingListManager;
        $this->cartTotalsManager = $cartTotalManager;
        parent::__construct($registry, $translator, $authorizationChecker, $requestStack, $lineItemMapper);
    }

    /**
     * {@inheritdoc}
     * @return ShoppingList|null
     */
    protected function getTargetEntity(MassActionHandlerArgs $args)
    {
        $id = $args->getData()['shopping_list_id'] ?? null;
        if (!$id) {
            return null;
        }

        return $this->registry->getRepository(ShoppingList::class)->find($id);
    }

    /**
     * @param CartLineItem $lineItem
     * @return Cart
     */
    protected function getOriginalEntity($lineItem)
    {
        return $lineItem->getCart();
    }

    /**
     * @param Cart $originalEntity
     * @param CartLineItem|ProductLineItemInterface $originalLineItem
     * @return void
     */
    protected function doRemoveOriginalEntityLineItem($originalEntity, ProductLineItemInterface $originalLineItem): void
    {
        $originalEntity->removeLineItem($originalLineItem);
    }

    /**
     * @param ShoppingList $shoppingList
     * @param CartLineItem|ProductLineItemInterface $cartLineItem
     */
    protected function addLineItem($shoppingList, ProductLineItemInterface $cartLineItem): void
    {
        $shoppingListLineItem = $this->lineItemMapper->mapCartLineItemToLineItem($cartLineItem);
        $this->shoppingListManager->addLineItem($shoppingListLineItem, $shoppingList, false);
    }

    /**
     * @param ObjectManager $manager
     * @param array|Cart[] $shoppingCarts
     */
    protected function recalculateTotals(ObjectManager $manager, array $shoppingCarts): void
    {
        foreach ($shoppingCarts as $shoppingCart) {
            $this->cartTotalsManager->recalculateTotals($shoppingCart, false);
        }
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function getHandlerHttpMethod(): string
    {
        return 'POST';
    }
}
