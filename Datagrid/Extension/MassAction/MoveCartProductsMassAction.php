<?php

namespace Xngage\Bundle\CartBundle\Datagrid\Extension\MassAction;

use Oro\Bundle\DataGridBundle\Extension\Action\ActionConfiguration;
use Oro\Bundle\DataGridBundle\Extension\MassAction\Actions\Ajax\AjaxMassAction;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

/**
 * Defines default options for moving/adding line items from Cart to shopping list mass action.
 */
class MoveCartProductsMassAction extends AjaxMassAction
{
    public const DEFAULT_HANDLER = 'xngage_cart.mass_action.move_cart_to_list_products_handler';
    public const FRONTEND_TYPE = 'move-cart-products-mass';
    public const DEFAULT_FRONTEND_ROUTE = 'xngage_cart_frontend_move_mass_action';
    public const DEFAULT_FRONTEND_ROUTE_PARAMS = [];

    /**
     * {@inheritdoc}
     */
    public function setOptions(ActionConfiguration $options)
    {
        if (!isset($options['handler'])) {
            $options['handler'] = self::DEFAULT_HANDLER;
        }

        if (!isset($options['route'])) {
            $options['route'] = self::DEFAULT_FRONTEND_ROUTE;
        }

        if (!isset($options['route_parameters'])) {
            $options['route_parameters'] = self::DEFAULT_FRONTEND_ROUTE_PARAMS;
        }

        if (!isset($options['frontend_handle'])) {
            $options['frontend_handle'] = 'dialog';
        }

        if (!isset($options['selectedElement'])) {
            $options['selectedElement'] = 'input[name="selected"]:checked';
        }

        $options['entity_name'] = CartLineItem::class;
        $options['frontend_type'] = self::FRONTEND_TYPE;
        $options['confirmation'] = false;

        return parent::setOptions($options);
    }
}
