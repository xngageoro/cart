<?php

namespace Xngage\Bundle\CartBundle\Datagrid\MassAction;

use Doctrine\ORM\EntityManager;
use Oro\Bundle\DataGridBundle\Extension\MassAction\DeleteMassActionHandler as ParentHandler;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;

/**
 * Recalculates totals after line items are deleted.
 */
class DeleteMassActionHandler extends ParentHandler
{
    private ?CartTotalManager $cartTotalManager = null;
    private ?Cart $cart = null;

    public function setCartTotalManager(CartTotalManager $cartTotalManager): void
    {
        $this->cartTotalManager = $cartTotalManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function processDelete($entity, EntityManager $manager): self
    {
        $this->cart = $entity->getCart();

        return parent::processDelete($entity, $manager);
    }

    /**
     * {@inheritdoc}
     */
    protected function finishBatch(EntityManager $manager): void
    {
        $manager->flush();

        if ($this->cart) {
            $this->cartTotalManager->recalculateTotals($this->cart, false);
        }

        $this->cart = null;

        parent::finishBatch($manager);
    }
}
