<?php

namespace Xngage\Bundle\CartBundle\Datagrid\EventListener\FrontendLineItemsGrid;

use Oro\Bundle\DataGridBundle\Event\OrmResultAfter;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Adds action configuration for simple and grouped rows.
 */
class CartLineItemsActionsOnResultAfterListener
{
    private array $cachedIsGranted = [];
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param OrmResultAfter $event
     */
    public function onResultAfter(OrmResultAfter $event): void
    {
        $records = $event->getRecords();
        if (!$records) {
            return;
        }

        $isEditGranted = $this->isEditGranted($event);
        $defaultActionsConfig = [
            'add_notes' => false,
            'edit_notes' => false,
            'update_configurable' => false,
            'delete' => $isEditGranted,
        ];

        foreach ($records as $record) {
            if ($record->getValue('isConfigurable')) {
                $actionsConfig = $defaultActionsConfig;
                $actionsConfig['update_configurable'] = $isEditGranted && $record->getValue('isMatrixFormAvailable');

                $subData = (array)$record->getValue('subData') ?: [];
                foreach ($subData as &$lineItemData) {
                    $subDataActionsConfig = $defaultActionsConfig;
                    $subDataActionsConfig['add_notes'] = $isEditGranted
                        && (string)($lineItemData['notes'] ?? '') === '';

                    $lineItemData['action_configuration'] = $subDataActionsConfig;
                }
                unset($lineItemData);

                $record->setValue('subData', $subData);
            } else {
                $actionsConfig = $defaultActionsConfig;
                $actionsConfig['add_notes'] = $isEditGranted && (string)$record->getValue('notes') === '';
            }

            $record->setValue('action_configuration', $actionsConfig);
        }
    }

    /**
     * @param OrmResultAfter $event
     * @return bool
     */
    protected function isEditGranted(OrmResultAfter $event): bool
    {
        $cart = $this->getCart($event);

        return $cart && $this->isEditPermissionGrantedForCart($cart);
    }

    /**
     * @param OrmResultAfter $event
     * @return Cart|null
     */
    protected function getCart(OrmResultAfter $event): ?Cart
    {
        $cartId = $event->getDatagrid()->getParameters()->get('cart_id');
        if ($cartId) {
            /** @var Cart $cart */
            $cart = $event->getQuery()->getEntityManager()->find(Cart::class, $cartId);
        }

        return $cart ?? null;
    }

    /**
     * @param Cart $cart
     * @return bool
     */
    protected function isEditPermissionGrantedForCart(Cart $cart): bool
    {
        $cartId = $cart->getId();
        if (!isset($this->cachedIsGranted[$cartId])) {
            $this->cachedIsGranted[$cartId] = $this->authorizationChecker->isGranted(
                'xngage_cart_frontend_update',
                $cart
            );
        }

        return $this->cachedIsGranted[$cartId];
    }
}
