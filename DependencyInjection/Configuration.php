<?php

namespace Xngage\Bundle\CartBundle\DependencyInjection;

use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(XngageCartExtension::ALIAS);
        $rootNode = $treeBuilder->getRootNode();

        SettingsBuilder::append(
            $rootNode,
            [
                'availability_for_guests' => ['type' => 'boolean', 'value' => false],
                'cart_max_line_items_per_page' => ['value' => 1000, 'type' => 'integer'],
            ]
        );

        return $treeBuilder;
    }
}
