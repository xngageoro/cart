<?php

namespace Xngage\Bundle\CartBundle\CartLineItem\Factory;

use Oro\Bundle\ProductBundle\Entity\Product;

interface LineItemFactoryInterface
{
    /**
     * @param object $entity
     * @param Product $product
     */
    public function create(object $entity, Product $product);
}
