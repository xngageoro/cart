<?php

namespace Xngage\Bundle\CartBundle\CartLineItem\Factory;

use Oro\Bundle\ProductBundle\Entity\Product;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class CartLineItemFactory implements LineItemFactoryInterface
{
    /**
     * @param Cart $cart
     * @param Product $product
     *
     * @return CartLineItem
     */
    public function create(object $cart, Product $product): CartLineItem
    {
        $lineItem = new CartLineItem();
        $lineItem
            ->setProduct($product)
            ->setQuantity(0)
            ->setCart($cart)
            ->setCustomerUser($cart->getCustomerUser())
            ->setOrganization($cart->getOrganization())
            ->setUnit($product->getPrimaryUnitPrecision()->getUnit())
            ->setOwner($cart->getOwner());

        return $lineItem;
    }
}
