<?php

namespace Xngage\Bundle\CartBundle\CartLineItem;

use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class LineItemMapper
{
    /**
     * @param LineItem $lineItem
     *
     * @return CartLineItem
     */
    public function mapLineItemToCartLineItem(LineItem $lineItem): CartLineItem
    {
        return (new CartLineItem())
            ->setCustomerUser($lineItem->getCustomerUser())
            ->setProduct($lineItem->getProduct())
            ->setParentProduct($lineItem->getParentProduct())
            ->setUnit($lineItem->getUnit())
            ->setQuantity($lineItem->getQuantity())
            ->setNotes($lineItem->getNotes())
            ->setOwner($lineItem->getOwner())
            ->setOrganization($lineItem->getOrganization());
    }

    /**
     * @param CartLineItem $cartLineItem
     *
     * @return LineItem
     */
    public function mapCartLineItemToLineItem(CartLineItem $cartLineItem): LineItem
    {
        $lineItem = (new LineItem())
            ->setCustomerUser($cartLineItem->getCustomerUser())
            ->setProduct($cartLineItem->getProduct())
            ->setUnit($cartLineItem->getUnit())
            ->setQuantity($cartLineItem->getQuantity())
            ->setNotes($cartLineItem->getNotes())
            ->setOwner($cartLineItem->getOwner())
            ->setOrganization($cartLineItem->getOrganization());

        if ($cartLineItem->getParentProduct()) {
            $lineItem->setParentProduct($cartLineItem->getParentProduct());
        }

        return $lineItem;
    }
}
