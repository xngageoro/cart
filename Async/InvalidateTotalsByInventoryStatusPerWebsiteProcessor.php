<?php

namespace Xngage\Bundle\CartBundle\Async;

use Doctrine\DBAL\Exception\RetryableException;
use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\ShoppingListBundle\Async\InvalidateTotalsByInventoryStatusPerWebsiteProcessor as BaseProcessor;
use Oro\Bundle\ShoppingListBundle\Async\MessageFactory;
use Oro\Bundle\ShoppingListBundle\Entity\Repository\ShoppingListTotalRepository;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingListTotal;
use Oro\Bundle\WebsiteBundle\Provider\WebsiteProviderInterface;
use Oro\Component\MessageQueue\Client\TopicSubscriberInterface;
use Oro\Component\MessageQueue\Consumption\MessageProcessorInterface;
use Oro\Component\MessageQueue\Transport\MessageInterface;
use Oro\Component\MessageQueue\Transport\SessionInterface;
use Oro\Component\MessageQueue\Util\JSON;
use Xngage\Bundle\CartBundle\Entity\CartTotal;
use Xngage\Bundle\CartBundle\Entity\Repository\CartTotalRepository;
use Psr\Log\LoggerInterface;

/**
 * Schedule cart and shopping list line items totals invalidation containing products with invisible inventory statuses.
 * Process only carts and shopping list for websites with changed settings.
 */
class InvalidateTotalsByInventoryStatusPerWebsiteProcessor implements
    MessageProcessorInterface,
    TopicSubscriberInterface
{
    protected ConfigManager $configManager;
    protected ManagerRegistry $registry;
    protected MessageFactory $messageFactory;
    protected WebsiteProviderInterface $websiteProvider;
    protected LoggerInterface $logger;

    public function __construct(
        ConfigManager $configManager,
        WebsiteProviderInterface $websiteProvider,
        ManagerRegistry $registry,
        MessageFactory $messageFactory,
        LoggerInterface $logger
    ) {
        $this->configManager = $configManager;
        $this->websiteProvider = $websiteProvider;
        $this->registry = $registry;
        $this->messageFactory = $messageFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedTopics()
    {
        return BaseProcessor::getSubscribedTopics();
    }

    /**
     * {@inheritDoc}
     */
    public function process(MessageInterface $message, SessionInterface $session)
    {
        $data = JSON::decode($message->getBody());
        $context = $this->messageFactory->getContext($data);
        $websitesToProcess = $this->getWebsitesToProcess($context);
        if (!$websitesToProcess) {
            // Nothing to do
            return self::ACK;
        }

        try {
            /** @var CartTotalRepository $repository */
            $cartTotalRepository = $this->registry->getRepository(CartTotal::class);
            /** @var ShoppingListTotalRepository $repo */
            $shoppingListRepository = $this->registry->getRepository(ShoppingListTotal::class);

            foreach ($websitesToProcess as $website) {
                $cartTotalRepository->invalidateByWebsite($website);
                $shoppingListRepository->invalidateByWebsite($website);
            }
        } catch (\Exception $e) {
            if ($e instanceof RetryableException) {
                $this->logger->error(
                    'Retryable database exception occurred during cart totals invalidation',
                    ['exception' => $e]
                );
                return self::REQUEUE;
            }

            return self::REJECT;
        }

        return self::ACK;
    }

    /**
     * @param object|null $context
     * @return array
     */
    protected function getWebsitesToProcess(mixed $context): array
    {
        if ($context) {
            return [$context];
        }

        $websites = $this->websiteProvider->getWebsites();
        $allowedStatusesPerWebsite = $this->configManager->getValues(
            'oro_product.general_frontend_product_visibility',
            $websites,
            false,
            true
        );

        $toProcess = [];
        foreach ($allowedStatusesPerWebsite as $websiteId => $config) {
            // Process only websites with fallback to parent scope
            if (!empty($config[ConfigManager::USE_PARENT_SCOPE_VALUE_KEY])) {
                $toProcess[] = $websites[$websiteId];
            }
        }

        return $toProcess;
    }
}
