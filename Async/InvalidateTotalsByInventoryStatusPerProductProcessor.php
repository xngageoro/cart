<?php

namespace Xngage\Bundle\CartBundle\Async;

use Doctrine\DBAL\Exception\RetryableException;
use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\ShoppingListBundle\Async\InvalidateTotalsByInventoryStatusPerProductProcessor as BaseProcessor;
use Oro\Bundle\ShoppingListBundle\Async\MessageFactory;
use Oro\Bundle\ShoppingListBundle\Entity\Repository\ShoppingListTotalRepository;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingListTotal;
use Oro\Bundle\WebsiteBundle\Entity\Website;
use Oro\Component\MessageQueue\Client\TopicSubscriberInterface;
use Oro\Component\MessageQueue\Consumption\MessageProcessorInterface;
use Oro\Component\MessageQueue\Transport\MessageInterface;
use Oro\Component\MessageQueue\Transport\SessionInterface;
use Oro\Component\MessageQueue\Util\JSON;
use Xngage\Bundle\CartBundle\Entity\CartTotal;
use Xngage\Bundle\CartBundle\Entity\Repository\CartTotalRepository;
use Psr\Log\LoggerInterface;

/**
 * Invalidate cart and shopping list totals for entities applicable for given context.
 */
class InvalidateTotalsByInventoryStatusPerProductProcessor implements
    MessageProcessorInterface,
    TopicSubscriberInterface
{
    protected ManagerRegistry $registry;
    protected MessageFactory $messageFactory;
    protected LoggerInterface $logger;

    public function __construct(
        ManagerRegistry $registry,
        MessageFactory $messageFactory,
        LoggerInterface $logger
    ) {
        $this->registry = $registry;
        $this->messageFactory = $messageFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedTopics()
    {
        return BaseProcessor::getSubscribedTopics();
    }

    /**
     * {@inheritDoc}
     */
    public function process(MessageInterface $message, SessionInterface $session)
    {
        $data = JSON::decode($message->getBody());
        $context = $this->messageFactory->getContext($data);
        if (!$context instanceof Website) {
            return self::ACK;
        }

        $products = $this->messageFactory->getProductIds($data);

        try {
            /** @var CartTotalRepository $repository */
            $cartTotalRepository = $this->registry->getRepository(CartTotal::class);
            $cartTotalRepository->invalidateByProducts($context, $products);
            /** @var ShoppingListTotalRepository $shoppingListRepository */
            $shoppingListRepository = $this->registry->getRepository(ShoppingListTotal::class);
            $shoppingListRepository->invalidateByProducts($context, $products);
        } catch (\Exception $e) {
            if ($e instanceof RetryableException) {
                $this->logger->error(
                    'Retryable database exception occurred during cart totals invalidation',
                    ['exception' => $e]
                );
                return self::REQUEUE;
            }

            return self::REJECT;
        }

        return self::ACK;
    }
}
