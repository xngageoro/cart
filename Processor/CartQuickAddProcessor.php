<?php

namespace Xngage\Bundle\CartBundle\Processor;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\ComponentProcessor\ComponentProcessorInterface;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Storage\ProductDataStorage;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Xngage\Bundle\CartBundle\Generator\MessageGenerator;
use Xngage\Bundle\CartBundle\Handler\CartLineItemHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CartQuickAddProcessor implements ComponentProcessorInterface
{
    public const NAME = 'xngage_cart_quick_add_processor';

    protected CartLineItemHandler $cartLineItemHandler;
    protected MessageGenerator $messageGenerator;
    protected AclHelper $aclHelper;
    protected DoctrineHelper $doctrineHelper;

    public function __construct(
        CartLineItemHandler $cartLineItemHandler,
        MessageGenerator $messageGenerator,
        AclHelper $aclHelper,
        DoctrineHelper $doctrineHelper
    ) {
        $this->cartLineItemHandler = $cartLineItemHandler;
        $this->messageGenerator = $messageGenerator;
        $this->aclHelper = $aclHelper;
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data, Request $request)
    {
        if (empty($data[ProductDataStorage::ENTITY_ITEMS_DATA_KEY]) ||
            !is_array($data[ProductDataStorage::ENTITY_ITEMS_DATA_KEY])
        ) {
            return;
        }

        $data = $data[ProductDataStorage::ENTITY_ITEMS_DATA_KEY];

        /** @var FlashBagInterface $flashBag */
        $flashBag = $request->getSession()->getFlashBag();

        if ($entitiesCount = $this->fillCart($data)) {
            $flashBag->add(
                'success',
                $this->messageGenerator->getQuickOrderFormSuccessMessage(
                    'xngage.cart.actions.quick_add_success_message',
                    'xngage.cart.actions.view',
                    $entitiesCount
                )
            );
        } else {
            $flashBag->add(
                'error',
                $this->messageGenerator->getQuickOrderFormFailMessage('')
            );
        }
    }

    /**
     * @param array $data
     *
     * @return bool|int
     */
    protected function fillCart(array $data)
    {
        try {
            return $this->cartLineItemHandler->addFromQuickOrderForm(
                array_values($this->getProductIdsBySku($data)),
                $this->getProductUnitsWithQuantities($data)
            );
        } catch (AccessDeniedException $e) {
            return false;
        }
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function getProductIdsBySku(array $data): array
    {
        $productSkus = \array_column($data, ProductDataStorage::PRODUCT_SKU_KEY);

        $qb = $this->doctrineHelper->getEntityRepositoryForClass(Product::class)
            ->getProductsIdsBySkuQueryBuilder($productSkus);

        $productsData = $this->aclHelper->apply($qb)->getArrayResult();

        $productIdsBySku = [];
        foreach ($productsData as $key => $productData) {
            $productIdsBySku[$productData['sku']] = $productData['id'];
            unset($productsData[$key]);
        }

        return $productIdsBySku;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function getProductUnitsWithQuantities(array $data): array
    {
        $productUnitsWithQuantities = [];
        foreach ($data as $product) {
            $productQuantity = $product['productQuantity'];

            if (!isset($product['productUnit'])) {
                continue;
            }

            $productUnit = $product['productUnit'];

            $upperSku = mb_strtoupper($product['productSku']);
            if (array_key_exists($upperSku, $productUnitsWithQuantities)) {
                if (isset($productUnitsWithQuantities[$upperSku][$productUnit])) {
                    $productQuantity += $productUnitsWithQuantities[$upperSku][$productUnit];
                }
            }

            $productUnitsWithQuantities[$upperSku][$productUnit] = $productQuantity;
        }

        return $productUnitsWithQuantities;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function isValidationRequired(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isAllowed(): bool
    {
        return $this->cartLineItemHandler->isAllowed();
    }
}
