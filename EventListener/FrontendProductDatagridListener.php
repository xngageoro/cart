<?php

namespace Xngage\Bundle\CartBundle\EventListener;

use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Oro\Bundle\DataGridBundle\Event\PreBuild;
use Oro\Bundle\DataGridBundle\Extension\Formatter\Property\PropertyInterface;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SearchBundle\Datagrid\Event\SearchResultAfter;
use Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider;

/**
 * Add an information to frontend products grid how much qty of some unit was added to cart
 */
class FrontendProductDatagridListener
{
    private const COLUMN_LINE_ITEMS = 'product_cart_units';

    private ProductCartDataProvider $productCartDataProvider;
    private DoctrineHelper $doctrineHelper;

    public function __construct(
        ProductCartDataProvider $productCartDataProvider,
        DoctrineHelper $doctrineHelper
    ) {
        $this->productCartDataProvider = $productCartDataProvider;
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * @param PreBuild $event
     */
    public function onPreBuild(PreBuild $event): void
    {
        $config = $event->getConfig();

        $config->offsetAddToArrayByPath(
            '[properties]',
            [
                self::COLUMN_LINE_ITEMS => [
                    'type'          => 'field',
                    'frontend_type' => PropertyInterface::TYPE_ROW_ARRAY,
                ],
            ]
        );
    }

    /**
     * @param SearchResultAfter $event
     */
    public function onResultAfter(SearchResultAfter $event): void
    {
        /** @var ResultRecord[] $records */
        $records = $event->getRecords();
        $products = [];
        $entityManager = $this->doctrineHelper->getEntityManagerForClass(Product::class);

        foreach ($records as $record) {
            if ($product = $entityManager->getReference(Product::class, $record->getValue('id'))) {
                $products[] = $product;
            }
        }

        $unitQuantities = $this->productCartDataProvider->getProductsUnitsQuantity($products);

        if (!$unitQuantities) {
            return;
        }

        foreach ($records as $record) {
            $productId = $record->getValue('id');
            if (array_key_exists($productId, $unitQuantities)) {
                $record->addData([self::COLUMN_LINE_ITEMS => $unitQuantities[$productId]]);
            }
        }
    }
}
