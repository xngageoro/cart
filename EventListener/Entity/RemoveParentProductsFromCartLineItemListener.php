<?php

namespace Xngage\Bundle\CartBundle\EventListener\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class RemoveParentProductsFromCartLineItemListener
{
    /**
     * @param CartLineItem $lineItem
     * @param LifecycleEventArgs $event
     */
    public function prePersist(CartLineItem $lineItem, LifecycleEventArgs $event)
    {
        $cart = $lineItem->getCart();

        $parentLineItems = $event->getEntityManager()->getRepository(CartLineItem::class)->findBy([
            'cart' => $cart,
            'unit' => $lineItem->getProductUnit(),
            'product' => $lineItem->getParentProduct(),
        ]);

        foreach ($parentLineItems as $item) {
            $cart->removeLineItem($item);
        }
    }
}
