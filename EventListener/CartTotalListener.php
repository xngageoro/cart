<?php

namespace Xngage\Bundle\CartBundle\EventListener;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CustomerBundle\Entity\CustomerGroup;
use Oro\Bundle\PricingBundle\Entity\Repository\PriceListCustomerFallbackRepository;
use Oro\Bundle\PricingBundle\Entity\Repository\PriceListCustomerGroupFallbackRepository;
use Oro\Bundle\PricingBundle\Entity\Repository\PriceListWebsiteFallbackRepository;
use Oro\Bundle\PricingBundle\Event\CombinedPriceList\CombinedPriceListsUpdateEvent;
use Oro\Bundle\PricingBundle\Event\CombinedPriceList\ConfigCPLUpdateEvent;
use Oro\Bundle\PricingBundle\Event\CombinedPriceList\CustomerCPLUpdateEvent;
use Oro\Bundle\PricingBundle\Event\CombinedPriceList\CustomerGroupCPLUpdateEvent;
use Oro\Bundle\PricingBundle\Event\CombinedPriceList\WebsiteCPLUpdateEvent;
use Xngage\Bundle\CartBundle\Entity\CartTotal;
use Xngage\Bundle\CartBundle\Entity\Repository\CartTotalRepository;

/**
 * Listens changes of Price Lists assigned to Customers, Customer Groups, Websites
 * or changes Price List in system configuration and trigger invalidation of totals for all related Carts.
 */
class CartTotalListener
{
    public const ACCOUNT_BATCH_SIZE = 500;

    protected ManagerRegistry $registry;
    protected ConfigManager $configManager;
    private ?int $anonymousCustomerGroupId = null;

    /**
     * @param ManagerRegistry $registry
     * @param ConfigManager $configManager
     */
    public function __construct(ManagerRegistry $registry, ConfigManager $configManager)
    {
        $this->registry = $registry;
        $this->configManager = $configManager;
    }

    /**
     * @param CombinedPriceListsUpdateEvent $event
     */
    public function onPriceListUpdate(CombinedPriceListsUpdateEvent $event): void
    {
        $this->registry->getManagerForClass(CartTotal::class)
            ->getRepository(CartTotal::class)
            ->invalidateByCombinedPriceList($event->getCombinedPriceListIds());
    }

    /**
     * @param CustomerCPLUpdateEvent $event
     */
    public function onCustomerPriceListUpdate(CustomerCPLUpdateEvent $event): void
    {
        $customersData = $event->getCustomersData();
        /** @var CartTotalRepository $repository */
        $repository = $this->registry->getManagerForClass(CartTotal::class)
            ->getRepository(CartTotal::class);
        foreach ($customersData as $data) {
            $repository->invalidateByCustomers($data['customers'], $data['websiteId']);
        }
    }

    /**
     * @param CustomerGroupCPLUpdateEvent $event
     */
    public function onCustomerGroupPriceListUpdate(CustomerGroupCPLUpdateEvent $event): void
    {
        $customersData = $event->getCustomerGroupsData();
        /** @var PriceListCustomerFallbackRepository $fallbackRepository */
        $fallbackRepository = $this->registry->getRepository('OroPricingBundle:PriceListCustomerFallback');
        /** @var CartTotalRepository $cartTotalsRepository */
        $cartTotalsRepository = $this->registry->getRepository(CartTotal::class);
        foreach ($customersData as $data) {
            $customers = $fallbackRepository->getCustomerIdentityByGroup($data['customerGroups'], $data['websiteId']);
            $i = 0;
            $ids = [];
            foreach ($customers as $customerData) {
                $ids[] = $customerData['id'];
                $i++;
                if ($i % self::ACCOUNT_BATCH_SIZE === 0) {
                    $cartTotalsRepository->invalidateByCustomers($ids, $data['websiteId']);
                    $ids = [];
                }
            }
            if (!empty($ids)) {
                $cartTotalsRepository->invalidateByCustomers($ids, $data['websiteId']);
            }

            $this->handleGuestCarts($cartTotalsRepository, $data);
        }
    }

    /**
     * @param CartTotalRepository $repository
     * @param array $data
     */
    protected function handleGuestCarts(CartTotalRepository $repository, array $data): void
    {
        $anonymousCustomerGroupId = $this->getAnonymousCustomerGroupId();
        if (!$anonymousCustomerGroupId) {
            return;
        }

        foreach ($data['customerGroups'] as $customerGroup) {
            if ($customerGroup instanceof CustomerGroup) {
                $customerGroup = $customerGroup->getId();
            }

            if ((int)$customerGroup === $anonymousCustomerGroupId) {
                $repository->invalidateGuestCarts($data['websiteId']);

                return;
            }
        }
    }

    /**
     * @return int
     */
    protected function getAnonymousCustomerGroupId(): int
    {
        if ($this->anonymousCustomerGroupId === null) {
            $this->anonymousCustomerGroupId = (int)$this->configManager->get('oro_customer.anonymous_customer_group');
        }

        return $this->anonymousCustomerGroupId;
    }

    /**
     * @param WebsiteCPLUpdateEvent $event
     */
    public function onWebsitePriceListUpdate(WebsiteCPLUpdateEvent $event): void
    {
        $websiteIds = $event->getWebsiteIds();
        /** @var PriceListCustomerGroupFallbackRepository $fallbackRepository */
        $fallbackRepository = $this->registry->getRepository('OroPricingBundle:PriceListCustomerGroupFallback');
        /** @var CartTotalRepository $cartTotalsRepository */
        $cartTotalsRepository = $this->registry->getRepository(CartTotal::class);
        foreach ($websiteIds as $websiteId) {
            $customers = $fallbackRepository->getCustomerIdentityByWebsite($websiteId);
            $i = 0;
            $ids = [];
            foreach ($customers as $customerData) {
                $ids[] = $customerData['id'];
                $i++;
                if ($i % self::ACCOUNT_BATCH_SIZE === 0) {
                    $cartTotalsRepository->invalidateByCustomers($ids, $websiteId);
                    $ids = [];
                }
            }
            if (!empty($ids)) {
                $cartTotalsRepository->invalidateByCustomers($ids, $websiteId);
            }
        }
    }

    /**
     * @param ConfigCPLUpdateEvent $event
     */
    public function onConfigPriceListUpdate(ConfigCPLUpdateEvent $event): void
    {
        /** @var PriceListWebsiteFallbackRepository $fallbackWebsiteRepository */
        $fallbackWebsiteRepository = $this->registry->getRepository('OroPricingBundle:PriceListWebsiteFallback');
        /** @var PriceListCustomerGroupFallbackRepository $fallbackRepository */
        $fallbackRepository = $this->registry->getRepository('OroPricingBundle:PriceListCustomerGroupFallback');
        /** @var CartTotalRepository $cartTotalsRepository */
        $cartTotalsRepository = $this->registry->getRepository(CartTotal::class);

        $websitesData = $fallbackWebsiteRepository->getWebsiteIdByDefaultFallback();
        foreach ($websitesData as $websiteData) {
            $customers = $fallbackRepository->getCustomerIdentityByWebsite($websiteData['id']);
            $i = 0;
            $ids = [];
            foreach ($customers as $customerData) {
                $ids[] = $customerData['id'];
                $i++;
                if ($i % self::ACCOUNT_BATCH_SIZE === 0) {
                    $cartTotalsRepository->invalidateByCustomers($ids, $websiteData['id']);
                    $ids = [];
                }
            }
            if (!empty($ids)) {
                $cartTotalsRepository->invalidateByCustomers($ids, $websiteData['id']);
            }
        }
    }
}
