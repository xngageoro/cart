<?php

namespace Xngage\Bundle\CartBundle\EventListener;

use Oro\Bundle\ProductBundle\Event\DatagridLineItemsDataEvent;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

/**
 * Adds cart line items basic data.
 */
class DatagridLineItemsDataListener
{
    /**
     * @param DatagridLineItemsDataEvent $event
     */
    public function onLineItemData(DatagridLineItemsDataEvent $event): void
    {
        $lineItems = $event->getLineItems();
        if (!$lineItems) {
            return;
        }

        $firstLineItem = reset($lineItems);
        if (!($firstLineItem instanceof CartLineItem)) {
            throw new \LogicException(
                sprintf('%s entity was expected, got %s', CartLineItem::class, \get_class($firstLineItem))
            );
        }

        foreach ($lineItems as $lineItem) {
            $event->addDataForLineItem($lineItem->getEntityIdentifier(), ['notes' => (string)$lineItem->getNotes()]);
        }
    }
}
