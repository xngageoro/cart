<?php

namespace Xngage\Bundle\CartBundle\EventListener;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\CustomerBundle\Entity\CustomerVisitorManager;
use Oro\Bundle\CustomerBundle\Security\Firewall\AnonymousCustomerUserAuthenticationListener;
use Oro\Bundle\DataAuditBundle\EventListener\SendChangedEntitiesToMessageQueueListener;
use Xngage\Bundle\CartBundle\Manager\GuestCartMigrationManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Listener to migrate guest cart during interactive login.
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class InteractiveLoginListener
{
    protected CustomerVisitorManager $visitorManager;
    protected GuestCartMigrationManager $guestCartMigrationManager;
    protected LoggerInterface $logger;
    protected ConfigManager $configManager;
    protected SendChangedEntitiesToMessageQueueListener $sendChangedEntitiesToMessageQueueListener;

    public function __construct(
        CustomerVisitorManager $visitorManager,
        GuestCartMigrationManager $guestCartMigrationManager,
        LoggerInterface $logger,
        ConfigManager $configManager,
        SendChangedEntitiesToMessageQueueListener $sendChangedEntitiesToMessageQueueListener
    ) {
        $this->visitorManager = $visitorManager;
        $this->guestCartMigrationManager = $guestCartMigrationManager;
        $this->logger = $logger;
        $this->configManager = $configManager;
        $this->sendChangedEntitiesToMessageQueueListener = $sendChangedEntitiesToMessageQueueListener;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof CustomerUser
            || !$this->configManager->get('xngage_cart.availability_for_guests')) {
            return;
        }

        try {
            $credentials = $this->getCredentials($event->getRequest());
            if ($credentials) {
                $visitor = $this->visitorManager->find($credentials['visitor_id'], $credentials['session_id']);

                if ($visitor) {
                    // Disable data audit 'cause it could fail the consumer
                    $this->sendChangedEntitiesToMessageQueueListener->setEnabled(false);

                    $this->guestCartMigrationManager->migrateGuestCart($visitor);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Migration of the guest cart failed.', ['exception' => $e]);
        } finally {
            $this->sendChangedEntitiesToMessageQueueListener->setEnabled();
        }
    }

    protected function getCredentials(Request $request): ?array
    {
        $value = $request->cookies->get(AnonymousCustomerUserAuthenticationListener::COOKIE_NAME);
        if (!$value) {
            return null;
        }
        list($visitorId, $sessionId) = json_decode(base64_decode($value));

        return [
            'visitor_id' => $visitorId,
            'session_id' => $sessionId,
        ];
    }
}
