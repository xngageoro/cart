<?php

namespace Xngage\Bundle\CartBundle\EventListener;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\PricingBundle\Event\PricingStorage\CustomerGroupRelationUpdateEvent;
use Oro\Bundle\PricingBundle\Event\PricingStorage\CustomerRelationUpdateEvent;
use Oro\Bundle\PricingBundle\Event\PricingStorage\MassStorageUpdateEvent;
use Oro\Bundle\PricingBundle\Event\PricingStorage\WebsiteRelationUpdateEvent;
use Xngage\Bundle\CartBundle\Entity\CartTotal;
use Xngage\Bundle\CartBundle\Entity\Repository\CartTotalRepository;

/**
 * Listens changes of Price Lists assigned to Customers, Customer Groups, Websites
 * or changes Price List in system configuration and trigger invalidation of totals for all related Carts.
 */
class FlatPricingCartTotalListener
{
    protected ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param MassStorageUpdateEvent $event
     */
    public function onPriceListUpdate(MassStorageUpdateEvent $event): void
    {
        $repository = $this->getRepository();
        $repository->invalidateByPriceList($event->getPriceListIds());
    }

    /**
     * @param CustomerRelationUpdateEvent $event
     */
    public function onCustomerPriceListUpdate(CustomerRelationUpdateEvent $event): void
    {
        $customersData = $event->getCustomersData();
        $repository = $this->getRepository();
        foreach ($customersData as $data) {
            $repository->invalidateByCustomers($data['customers'], $data['websiteId']);
        }
    }

    /**
     * @param CustomerGroupRelationUpdateEvent $event
     */
    public function onCustomerGroupPriceListUpdate(CustomerGroupRelationUpdateEvent $event): void
    {
        $customerGroupsData = $event->getCustomerGroupsData();
        $repository = $this->getRepository();

        foreach ($customerGroupsData as $data) {
            $repository->invalidateByCustomerGroupsForFlatPricing($data['customerGroups'], $data['websiteId']);
        }
    }

    /**
     * @param WebsiteRelationUpdateEvent $event
     */
    public function onWebsitePriceListUpdate(WebsiteRelationUpdateEvent $event): void
    {
        $websiteIds = $event->getWebsiteIds();
        $this->getRepository()->invalidateByWebsitesForFlatPricing($websiteIds);
    }

    /**
     * @return CartTotalRepository
     */
    private function getRepository(): CartTotalRepository
    {
        return $this->registry->getRepository(CartTotal::class);
    }
}
