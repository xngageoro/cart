<?php

namespace Xngage\Bundle\CartBundle\Authorization;

use Oro\Bundle\CustomerBundle\Security\Token\AnonymousCustomerUserToken;
use Oro\Bundle\FeatureToggleBundle\Checker\FeatureCheckerHolderTrait;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CartAuthorizationChecker
{
    use FeatureCheckerHolderTrait;

    protected AuthorizationCheckerInterface $authorizationChecker;
    protected TokenAccessorInterface $tokenAccessor;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TokenAccessorInterface $tokenAccessor
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenAccessor = $tokenAccessor;
    }

    /**
     * @return bool
     */
    public function isGuestTokenAndGuestFeatureEnabled(): bool
    {
        return $this->tokenAccessor->getToken() instanceof AnonymousCustomerUserToken && $this->isFeaturesEnabled();
    }

    /**
     * @return bool
     */
    public function isCartEditAllowed(): bool
    {
        if (!$this->tokenAccessor->hasUser() && !$this->isGuestTokenAndGuestFeatureEnabled()) {
            return false;
        }

        return $this->authorizationChecker->isGranted('xngage_cart_frontend_update');
    }
}
