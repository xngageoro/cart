<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\LayoutBundle\Layout\DataProvider\AbstractFormProvider;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\ShoppingListBundle\Form\Type\MatrixCollectionType;
use Xngage\Bundle\CartBundle\Manager\MatrixCollectionEntityInterface;
use Xngage\Bundle\CartBundle\Manager\MatrixGridOrderManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Form\FormView;

/**
 * Provides matrix forms and matrix form views which can be used to update configurable products.
 */
class MatrixGridOrderFormProvider extends AbstractFormProvider
{
    protected ?MatrixGridOrderManagerInterface $matrixGridOrderManager = null;
    protected ?FormRenderer $twigRenderer = null;

    public function setMatrixOrderManager(MatrixGridOrderManagerInterface $matrixGridOrderManager): void
    {
        $this->matrixGridOrderManager = $matrixGridOrderManager;
    }

    public function setTwigRenderer(FormRenderer $twigRenderer): void
    {
        $this->twigRenderer = $twigRenderer;
    }

    /**
     * @param Product                               $product
     * @param MatrixCollectionEntityInterface|null  $entity
     *
     * @return FormInterface
     */
    public function getMatrixOrderForm(Product $product, MatrixCollectionEntityInterface $entity = null): FormInterface
    {
        $collection = $this->matrixGridOrderManager->getMatrixCollection($product, $entity);

        return $this->getForm(MatrixCollectionType::class, $collection);
    }

    /**
     * @param Product $product
     * @param ProductUnit $productUnit
     * @param MatrixCollectionEntityInterface $entity
     * @return FormInterface
     */
    public function getMatrixOrderByUnitForm(
        Product $product,
        ProductUnit $productUnit,
        MatrixCollectionEntityInterface $entity
    ): FormInterface {
        $collection = $this->matrixGridOrderManager->getMatrixCollectionForUnit($product, $productUnit, $entity);

        return $this->getForm(MatrixCollectionType::class, $collection);
    }

    /**
     * @param Product|null                          $product
     * @param MatrixCollectionEntityInterface|null  $entity
     *
     * @return FormView
     */
    public function getMatrixOrderFormView(
        Product $product = null,
        MatrixCollectionEntityInterface $entity = null
    ): FormView {
        if (!$product) {
            return $this->getFormView(MatrixCollectionType::class);
        }
        $collection = $this->matrixGridOrderManager->getMatrixCollection($product, $entity);

        return $this->getFormView(
            MatrixCollectionType::class,
            $collection,
            [],
            ['cacheKey' => md5(serialize($collection))]
        );
    }

    /**
     * @param Product $product
     * @param ProductUnit $productUnit
     * @param MatrixCollectionEntityInterface $entity
     * @return FormView
     */
    public function getMatrixOrderByUnitFormView(
        Product $product,
        ProductUnit $productUnit,
        MatrixCollectionEntityInterface $entity
    ): FormView {
        $collection = $this->matrixGridOrderManager->getMatrixCollectionForUnit($product, $productUnit, $entity);

        return $this->getFormView(
            MatrixCollectionType::class,
            $collection,
            [],
            ['cacheKey' => md5(serialize($collection))]
        );
    }

    /**
     * @param Product $product
     * @param MatrixCollectionEntityInterface|null $entity
     *
     * @return string
     */
    public function getMatrixOrderFormHtml(
        Product $product = null,
        MatrixCollectionEntityInterface $entity = null
    ): string {
        $formView = $this->getMatrixOrderFormView($product, $entity);

        return $this->twigRenderer->searchAndRenderBlock($formView, 'widget');
    }
}
