<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;

/**
 * Provides currently logged in user cart for layouts.
 */
class CustomerUserCartProvider
{
    protected CurrentCartManager $currentCartManager;
    protected CartTotalManager $cartTotalManager;

    public function __construct(CurrentCartManager $currentCartManager, CartTotalManager $cartTotalManager)
    {
        $this->currentCartManager = $currentCartManager;
        $this->cartTotalManager = $cartTotalManager;
    }

    /**
     * @return Cart|null
     */
    public function getCurrent(): ?Cart
    {
        return $this->currentCartManager->getCurrent();
    }

    /**
     * @return Cart|null
     */
    public function getCurrentWithSubtotal(): ?Cart
    {
        $cart = $this->currentCartManager->getCurrent();
        if ($cart !== null) {
            $this->cartTotalManager->setSubtotal($cart);
        }

        return $cart;
    }
}
