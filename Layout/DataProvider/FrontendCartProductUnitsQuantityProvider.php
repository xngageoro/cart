<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\ProductBundle\Entity\Product;
use Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider;

class FrontendCartProductUnitsQuantityProvider
{
    protected array $data = [];
    protected ProductCartDataProvider $productCartDataProvider;

    /**
     * @param ProductCartDataProvider $productCartDataProvider
     */
    public function __construct(ProductCartDataProvider $productCartDataProvider)
    {
        $this->productCartDataProvider = $productCartDataProvider;
    }

    /**
     * @param Product|null $product
     *
     * @return mixed|null
     */
    public function getByProduct(Product $product = null)
    {
        if (!$product) {
            return null;
        }

        $this->setByProducts([$product]);

        return $this->data[$product->getId()];
    }

    /**
     * @param Product[] $products
     *
     * @return array
     */
    public function getByProducts($products): array
    {
        $this->setByProducts($products);

        return $this->data;
    }

    /**
     * @param Product[] $products
     */
    protected function setByProducts($products)
    {
        $products = array_filter($products, function (Product $product) {
            return !array_key_exists($product->getId(), $this->data);
        });
        if (!$products) {
            return;
        }

        $cartProductUnitsQuantities = $this->productCartDataProvider->getProductsUnitsQuantity($products);
        foreach ($products as $product) {
            $productId = $product->getId();
            $this->data[$productId] = $cartProductUnitsQuantities[$productId] ?? null;
        }
    }
}
