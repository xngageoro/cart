<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\LayoutBundle\Layout\DataProvider\AbstractFormProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Form\Type\CartNotesType;
use Symfony\Component\Form\FormView;

/**
 * Provides form types to edit cart.
 */
class CartFormProvider extends AbstractFormProvider
{
    /**
     * @param Cart $cart
     *
     * @return FormView
     */
    public function getCartNotesFormView(Cart $cart): FormView
    {
        return $this->getFormView(CartNotesType::class, $cart);
    }
}
