<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\LocaleBundle\Formatter\NumberFormatter;
use Oro\Bundle\PricingBundle\SubtotalProcessor\TotalProcessorProvider;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Xngage\Bundle\CartBundle\Manager\MatrixCollectionCurrentEntityManagerInterface;
use Xngage\Bundle\CartBundle\Manager\MatrixCollectionEntityInterface;
use Xngage\Bundle\CartBundle\Manager\MatrixGridOrderManagerInterface;

/**
 * Provides data for matrix order grid for layouts.
 */
abstract class MatrixGridOrderProvider
{
    protected MatrixGridOrderManagerInterface $matrixGridOrderManager;
    protected TotalProcessorProvider $totalProvider;
    protected NumberFormatter $numberFormatter;
    protected MatrixCollectionCurrentEntityManagerInterface $matrixCollectionCurrentEntityManager;

    public function __construct(
        MatrixGridOrderManagerInterface $matrixGridOrderManager,
        TotalProcessorProvider $totalProvider,
        NumberFormatter $numberFormatter,
        MatrixCollectionCurrentEntityManagerInterface $matrixCollectionCurrentEntityManager
    ) {
        $this->matrixGridOrderManager = $matrixGridOrderManager;
        $this->totalProvider = $totalProvider;
        $this->numberFormatter = $numberFormatter;
        $this->matrixCollectionCurrentEntityManager = $matrixCollectionCurrentEntityManager;
    }

    /**
     * Get total quantities for all columns and per column
     *
     * @param Product $product
     * @param MatrixCollectionEntityInterface $entity
     *
     * @return float|null
     */
    public function getTotalQuantity(Product $product, MatrixCollectionEntityInterface $entity = null): ?float
    {
        $entity = $entity ?: $this->matrixCollectionCurrentEntityManager->getCurrent();

        $collection = $this->matrixGridOrderManager->getMatrixCollection($product, $entity);

        $totalQuantity = 0;
        foreach ($collection->rows as $row) {
            foreach ($row->columns as $column) {
                $totalQuantity += $column->quantity;
            }
        }

        return $totalQuantity;
    }

    /**
     * @param Product $product
     * @param MatrixCollectionEntityInterface $entity
     * @return string
     */
    public function getTotalPriceFormatted(Product $product, MatrixCollectionEntityInterface $entity = null): string
    {
        $entity = $entity ?: $this->matrixCollectionCurrentEntityManager->getCurrent();

        $tempEntity = $this->getEntityFromCurrentMatrixCollection(
            $entity,
            $this->matrixGridOrderManager->getMatrixCollection($product, $entity)
        );

        $price = $this->totalProvider->getTotal($tempEntity)->getTotalPrice();

        return $this->numberFormatter->formatCurrency(
            $price->getValue(),
            $price->getCurrency()
        );
    }

    /**
     * @param Product[] $products
     * @param MatrixCollectionEntityInterface $entity
     *
     * @return array
     */
    public function getTotalsQuantityPrice(array $products, MatrixCollectionEntityInterface $entity = null): array
    {
        $totals = [];

        foreach ($products as $product) {
            if ($product->getType() !== Product::TYPE_CONFIGURABLE) {
                continue;
            }

            $totals[$product->getId()] = [
                'quantity' => $this->getTotalQuantity($product, $entity),
                'price' => $this->getTotalPriceFormatted($product, $entity),
            ];
        }

        return $totals;
    }

    /**
     * @param MatrixCollectionEntityInterface $entity
     * @param MatrixCollection $collection
     *
     * @return MatrixCollectionEntityInterface
     */
    abstract public function getEntityFromCurrentMatrixCollection(
        MatrixCollectionEntityInterface $entity,
        MatrixCollection $collection
    ): MatrixCollectionEntityInterface;
}
