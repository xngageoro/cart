<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\PricingBundle\Formatter\ProductPriceFormatter;
use Oro\Bundle\PricingBundle\Provider\FrontendProductPricesDataProvider;
use Xngage\Bundle\CartBundle\DataProvider\CartLineItemsDataProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;

/**
 * Provides products and prices in relation to cart
 */
class FrontendCartProductsProvider
{
    protected FrontendProductPricesDataProvider $productPriceProvider;
    protected CartLineItemsDataProvider $cartLineItemsDataProvider;
    protected ProductPriceFormatter $productPriceFormatter;

    public function __construct(
        FrontendProductPricesDataProvider $productPriceProvider,
        CartLineItemsDataProvider $cartLineItemsDataProvider,
        ProductPriceFormatter $productPriceFormatter
    ) {
        $this->productPriceProvider = $productPriceProvider;
        $this->cartLineItemsDataProvider = $cartLineItemsDataProvider;
        $this->productPriceFormatter = $productPriceFormatter;
    }

    /**
     * @param Cart|null $cart
     *
     * @return array|null
     */
    public function getAllPrices(Cart $cart = null): ?array
    {
        if (!$cart) {
            return null;
        }

        $lineItems = $this->cartLineItemsDataProvider->getCartLineItems($cart);
        $productPrices = $this->productPriceProvider->getProductsAllPrices($lineItems);

        return $this->productPriceFormatter->formatProducts($productPrices);
    }

    /**
     * @param Cart|null $cart
     *
     * @return array|null
     */
    public function getMatchedPrice(Cart $cart = null): ?array
    {
        if (!$cart) {
            return null;
        }

        $lineItems = $this->cartLineItemsDataProvider->getCartLineItems($cart);

        return $this->productPriceProvider->getProductsMatchedPrice($lineItems);
    }
}
