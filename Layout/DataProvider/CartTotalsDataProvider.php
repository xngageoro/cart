<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\PricingBundle\SubtotalProcessor\TotalProcessorProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;

/**
 * Layouts data provider for cart totals
 */
class CartTotalsDataProvider
{
    private TotalProcessorProvider $totalProcessorProvider;

    public function __construct(TotalProcessorProvider $totalProcessorProvider)
    {
        $this->totalProcessorProvider = $totalProcessorProvider;
    }

    /**
     * @param Cart $cart
     *
     * @return array
     */
    public function getTotalWithSubtotalsAsArray(Cart $cart): array
    {
        return $this->totalProcessorProvider->getTotalWithSubtotalsAsArray($cart);
    }
}
