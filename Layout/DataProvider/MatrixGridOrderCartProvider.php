<?php

namespace Xngage\Bundle\CartBundle\Layout\DataProvider;

use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\MatrixCollectionEntityInterface;

/**
 * Provides data for matrix order grid for layouts.
 */
class MatrixGridOrderCartProvider extends MatrixGridOrderProvider
{
    /**
     * {@inheritDoc}
     */
    public function getEntityFromCurrentMatrixCollection(
        MatrixCollectionEntityInterface $entity,
        MatrixCollection $collection
    ): MatrixCollectionEntityInterface {
        /** @var Cart $tempEntity */
        $tempEntity = clone $entity;
        $tempEntity->getLineItems()->clear();

        foreach ($collection->rows as $row) {
            foreach ($row->columns as $column) {
                if ($column->product === null || !$column->quantity) {
                    continue;
                }

                $lineItem = new CartLineItem();
                $lineItem->setProduct($column->product);
                $lineItem->setUnit($collection->unit);
                $lineItem->setQuantity($column->quantity);

                $tempEntity->addLineItem($lineItem);
            }
        }

        return $tempEntity;
    }
}
