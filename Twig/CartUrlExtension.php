<?php

namespace Xngage\Bundle\CartBundle\Twig;

use Xngage\Bundle\CartBundle\Provider\CartUrlProvider;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides a Twig function to get cart storefront urls:
 *   - xngage_cart_frontend_url
 */
class CartUrlExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'xngage_cart_frontend_url',
                [$this->container->get(CartUrlProvider::class), 'getFrontendUrl']
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices(): array
    {
        return [
            CartUrlProvider::class,
        ];
    }
}
