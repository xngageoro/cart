# Cart Extension

**Owner: Żaklina Data (zdata@oroinc.com)**

- [Overview](#overview)
- [Features](#features)
- [Installation](#installation)
- [System configuration](#system-configuration)
- [Tests](#tests)
- [Contribution](#contribution)

## Overview

A Cart extension complements OroShoppingListBundle with a separate shopping Cart functionality. The look and feel is consistent with ShoppingList.

## Features

- Single Cart per Customer User
- Cart button next to ShoppingLists button to quick access the Cart, with short info about items and Cart subtotal
- Add/Update/Remove to/from Cart button on products listings and view page
- Cart view page with ability to manage Cart line items
- adding products to Cart from Quick Order Form
- Move from Cart to Shopping List button on Cart view page with ability to select products which will be moved (if those products already exist in SL, they will be merged)
- Add from Shopping List to Cart button on Shopping List view page with ability to select products which will be added (if those products already exist in Cart, they will be merged)
- ability to create Order form Cart
- merging Guest Cart with Customer Users on login
- ACL protected with availability to enable/disable feature for Guests
- list of Carts on Customer and CustomerUser view page in back-office
- Export Cart to CSV in format which can be later imported via Quick Order Form (to make sharing cart between users easy and available)

## Installation

The easiest way to install Cart extension is by using [Composer](https://getcomposer.org).

First, add new composer repository:

```
{
    "repositories": {
        "cart-extension": {
          "type": "vcs",
          "url": "https://git.oroinc.com/extensions/cart.git"
        }
    }
}    
```

and install it via composer.

 
```bash
curl -sS https://getcomposer.org/installer | php
php composer.phar require Xngage/cart
```

or if you have Composer already installed, just run:

```bash
composer require Xngage/cart
```

#### Workflow patch
Add composer dependency for composer patches:
```bash
composer require cweagans/composer-patches
```

Copy b2b_flow_checkout_cart.patch from Cart extension `patches/` directory into your project `patches/` directory.

Add patch to `composer.json`:
```bash
"extra": {
    ...
    "composer-exit-on-patch-failure": true,
    "enable-patching": true,
    "patches": {
      "oro/commerce": {
        "Cart Workflow": "patches/b2b_flow_checkout_cart.patch",
      }
    }
  }
```

After installation and running `oro:platform:update` Cart functionality is enabled by default.

## System configuration
There are few options configurable from back-office.
1) Go to `System -> Configuration -> Commerce -> Sales -> Cart`.
- Enable Guest Cart
    - allow to enable/disable Cart feature for Guests
    - name: `xngage_cart.availability_for_guests`
    - default: false
- Maximum Line Items Per Page
  - allow to set max items per page in Cart datagrid
  - name: `xngage_cart.cart_max_line_items_per_page`
  - default: 1000  

Those configuration values can be also customized per Website and Organization.

## Tests

This package is covered with behat tests. See `Tests/Behat/Features/`

## Contribution

The easiest way of contributing to the extension is by installing it together with `laboro/dev`. 

First, add `path` composer repository to `dev.json`, e.g.
```
{
    "repositories": [
        {
          "type": "path",
          "url": "/var/www/oro/extensions/*"
        }
    ]
}
```
or from CLI:
```
composer config repositories.extension '{"type": "path", "url": "/var``/www/oro/extensions/*" }'
```
and install `COMPOSER=dev.json composer require Xngage/cart`.
