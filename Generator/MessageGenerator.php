<?php

namespace Xngage\Bundle\CartBundle\Generator;

use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Oro\Bundle\ShoppingListBundle\Provider\ShoppingListUrlProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Provider\CartUrlProvider;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageGenerator
{
    protected TranslatorInterface $translator;
    protected UrlGeneratorInterface $router;
    protected ShoppingListUrlProvider $shoppingListUrlProvider;
    protected CartUrlProvider $cartUrlProvider;

    public function __construct(
        TranslatorInterface $translator,
        UrlGeneratorInterface $router,
        ShoppingListUrlProvider $shoppingListUrlProvider,
        CartUrlProvider $cartUrlProvider
    ) {
        $this->translator = $translator;
        $this->router = $router;
        $this->shoppingListUrlProvider = $shoppingListUrlProvider;
        $this->cartUrlProvider = $cartUrlProvider;
    }

    /**
     * @param string $translationKey
     * @param Cart|null $cart
     * @return string
     */
    public function getCartSuccessMessage(string $translationKey, Cart $cart = null): string
    {
        $link = $this->cartUrlProvider->getFrontendUrl($cart);

        $label = $this->translator->trans('xngage.cart.default.label');
        if ($cart) {
            $label = htmlspecialchars($cart->getLabel());
        }

        return $this->translator->trans(
            $translationKey,
            ['%cart%' => sprintf('<a href="%s">%s</a>', $link, $label)]
        );
    }

    /**
     * @param ShoppingList $shoppingList
     * @param string $translationKey
     * @return string
     */
    public function getShoppingListSuccessMessage(ShoppingList $shoppingList, string $translationKey): string
    {
        $link = $this->shoppingListUrlProvider->getFrontendUrl($shoppingList);
        $label = htmlspecialchars($shoppingList->getLabel());

        return $this->translator->trans(
            $translationKey,
            ['%shoppinglist%' => sprintf('<a href="%s">%s</a>', $link, $label)]
        );
    }

    /**
     * @param string $route
     * @param array $routeParams
     * @param string $message
     * @param string $linkLabel
     * @param int $entitiesCount
     * @return string
     */
    public function getQuickOrderFormSuccessMessage(
        string $message,
        string $linkLabel,
        int $entitiesCount = 0
    ): string {
        $message = $this->translator->trans(
            $message,
            ['%count%' => $entitiesCount]
        );

        if ($entitiesCount > 0) {
            $link = $this->cartUrlProvider->getFrontendUrl();
            $message = sprintf(
                '%s (<a href="%s">%s</a>).',
                $message,
                $link,
                $this->translator->trans($linkLabel)
            );
        }

        return $message;
    }

    /**
     * @param string $message
     * @return string
     */
    public function getQuickOrderFormFailMessage(string $message): string
    {
        return $this->translator->trans($message);
    }
}
