<?php

namespace Xngage\Bundle\CartBundle\Form\Handler;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Oro\Bundle\FormBundle\Form\Handler\RequestHandlerTrait;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Handles add a product to a cart request.
 */
class CartLineItemHandler
{
    use RequestHandlerTrait;

    protected FormInterface $form;
    protected Request $request;
    protected ManagerRegistry $doctrine;
    protected CartManager $cartManager;
    protected ValidatorInterface $validator;

    public function __construct(
        FormInterface $form,
        Request $request,
        ManagerRegistry $doctrine,
        CartManager $cartManager,
        ValidatorInterface $validator
    ) {
        $this->form = $form;
        $this->request = $request;
        $this->doctrine = $doctrine;
        $this->cartManager = $cartManager;
        $this->validator = $validator;
    }

    /**
     * @param CartLineItem $lineItem
     *
     * @return bool
     */
    public function process(CartLineItem $lineItem): bool
    {
        if (!in_array($this->request->getMethod(), ['POST', 'PUT'], true)) {
            return false;
        }

        /** @var EntityManagerInterface $em */
        $em = $this->doctrine->getManagerForClass(CartLineItem::class);
        $em->beginTransaction();

        $this->submitPostPutRequest($this->form, $this->request);
        if ($this->form->isValid()) {
            $this->cartManager->addLineItem($lineItem, $lineItem->getCart(), false, true);

            if ($this->isCartValid($lineItem->getCart())) {
                $em->flush();
                $em->commit();

                return true;
            }
        }

        $em->rollback();

        return false;
    }

    /**
     * @param Cart $cart
     *
     * @return bool
     */
    protected function isCartValid(Cart $cart): bool
    {
        $constraintViolationList = $this->validator->validate($cart);

        if ($constraintViolationList->count()) {
            /** @var ConstraintViolation $constraintViolation */
            foreach ($constraintViolationList as $constraintViolation) {
                $this->addFormError($constraintViolation);
            }

            return false;
        }

        return true;
    }

    /**
     * @param ConstraintViolation $constraintViolation
     */
    protected function addFormError(ConstraintViolation $constraintViolation): void
    {
        $this->form->addError(new FormError(
            $constraintViolation->getMessage(),
            $constraintViolation->getMessageTemplate(),
            $constraintViolation->getParameters(),
            $constraintViolation->getPlural()
        ));
    }
}
