<?php

namespace Xngage\Bundle\CartBundle\Form\Handler;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\FormBundle\Event\EventDispatcher;
use Oro\Bundle\FormBundle\Event\FormHandler\AfterFormProcessEvent;
use Oro\Bundle\FormBundle\Event\FormHandler\Events;
use Oro\Bundle\FormBundle\Event\FormHandler\FormProcessEvent;
use Oro\Bundle\FormBundle\Form\Handler\FormHandlerInterface;
use Oro\Bundle\FormBundle\Form\Handler\RequestHandlerTrait;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The handler for the matrix grid order form.
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class CartMatrixGridOrderFormHandler implements FormHandlerInterface
{
    use RequestHandlerTrait;

    protected EventDispatcher $eventDispatcher;
    protected DoctrineHelper $doctrineHelper;
    protected CartMatrixGridOrderManager $cartMatrixGridOrderManager;
    protected CartManager $cartManager;

    public function __construct(
        EventDispatcher $eventDispatcher,
        DoctrineHelper $doctrineHelper,
        CartMatrixGridOrderManager $cartMatrixGridOrderManager,
        CartManager $cartManager
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->doctrineHelper = $doctrineHelper;
        $this->cartMatrixGridOrderManager = $cartMatrixGridOrderManager;
        $this->cartManager = $cartManager;
    }

    /**
     * {@inheritdoc}
     */
    public function process($data, FormInterface $form, Request $request): bool
    {
        if (!$data instanceof MatrixCollection) {
            throw new \InvalidArgumentException(
                sprintf('The "data" argument should be instance of the "%s" entity', MatrixCollection::class)
            );
        }

        $event = new FormProcessEvent($form, $data);
        $this->eventDispatcher->dispatch($event, Events::BEFORE_FORM_DATA_SET);

        if ($event->isFormProcessInterrupted()) {
            return false;
        }

        $form->setData($data);

        if ($request->getMethod() === Request::METHOD_POST) {
            $event = new FormProcessEvent($form, $data);
            $this->eventDispatcher->dispatch($event, Events::BEFORE_FORM_SUBMIT);

            if ($event->isFormProcessInterrupted()) {
                return false;
            }

            $this->submitPostPutRequest($form, $request);

            if ($form->isValid()) {
                $manager = $this->doctrineHelper->getEntityManager(CartLineItem::class);
                $manager->beginTransaction();

                try {
                    $this->saveData($data, $form, $request);
                    $manager->commit();
                } catch (\Exception $exception) {
                    $manager->rollback();
                    return false;
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param MatrixCollection $data
     * @param FormInterface $form
     * @param Request $request
     */
    protected function saveData(MatrixCollection $data, FormInterface $form, Request $request): void
    {
        $cart = $request->attributes->get('cart');
        if (!$cart instanceof Cart) {
            throw new \InvalidArgumentException('The "cart" request argument should be present');
        }

        $product = $request->attributes->get('product');
        if (!$product instanceof Product) {
            throw new \InvalidArgumentException('The "product" request argument should be present');
        }

        $this->eventDispatcher->dispatch(new AfterFormProcessEvent($form, $data), Events::BEFORE_FLUSH);

        $lineItems = $this->cartMatrixGridOrderManager->convertMatrixIntoLineItems(
            $form->getData(),
            $product,
            $request->request->get('matrix_collection', [])
        );

        foreach ($lineItems as $lineItem) {
            $this->cartManager->updateLineItem($lineItem, $cart);
        }

        $this->cartMatrixGridOrderManager->addEmptyMatrixIfAllowed($cart, $product, $lineItems);

        $this->eventDispatcher->dispatch(new AfterFormProcessEvent($form, $data), Events::AFTER_FLUSH);
    }
}
