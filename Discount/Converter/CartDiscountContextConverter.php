<?php

namespace Xngage\Bundle\CartBundle\Discount\Converter;

use Oro\Bundle\PricingBundle\Manager\UserCurrencyManager;
use Oro\Bundle\PromotionBundle\Discount\Converter\DiscountContextConverterInterface;
use Oro\Bundle\PromotionBundle\Discount\Converter\LineItemsToDiscountLineItemsConverter;
use Oro\Bundle\PromotionBundle\Discount\DiscountContext;
use Oro\Bundle\PromotionBundle\Discount\Exception\UnsupportedSourceEntityException;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;

/**
 * Data converter that prepares discount context based on cart entity to calculate discounts.
 */
class CartDiscountContextConverter implements DiscountContextConverterInterface
{
    protected LineItemsToDiscountLineItemsConverter $lineItemsConverter;
    protected UserCurrencyManager $currencyManager;
    protected CartTotalManager $cartTotalManager;

    public function __construct(
        LineItemsToDiscountLineItemsConverter $lineItemsConverter,
        UserCurrencyManager $currencyManager,
        CartTotalManager $cartTotalManager
    ) {
        $this->lineItemsConverter = $lineItemsConverter;
        $this->currencyManager = $currencyManager;
        $this->cartTotalManager = $cartTotalManager;
    }

    /**
     * @param Cart $sourceEntity
     * {@inheritdoc}
     */
    public function convert($sourceEntity): DiscountContext
    {
        if (!$this->supports($sourceEntity)) {
            throw new UnsupportedSourceEntityException(
                sprintf('Source entity "%s" is not supported.', get_class($sourceEntity))
            );
        }

        $discountContext = new DiscountContext();
        $discountContext->setSubtotal($this->getSubtotalAmount($sourceEntity));

        $discountLineItems = $this->lineItemsConverter->convert($sourceEntity->getLineItems()->toArray());
        $discountContext->setLineItems($discountLineItems);

        return $discountContext;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($sourceEntity): bool
    {
        return $sourceEntity instanceof Cart;
    }

    /**
     * @param Cart $sourceEntity
     *
     * @return float
     */
    protected function getSubtotalAmount(Cart $sourceEntity): float
    {
        $currency = $this->currencyManager->getUserCurrency();
        return $this->cartTotalManager
            ->getCartTotalForCurrency($sourceEntity, $currency)
            ->getSubtotal()
            ->getAmount();
    }
}
