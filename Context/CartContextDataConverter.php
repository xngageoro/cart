<?php

namespace Xngage\Bundle\CartBundle\Context;

use Oro\Bundle\PricingBundle\Manager\UserCurrencyManager;
use Oro\Bundle\ProductBundle\Model\ProductLineItemsHolderInterface;
use Oro\Bundle\PromotionBundle\Context\ContextDataConverterInterface;
use Oro\Bundle\PromotionBundle\Context\CriteriaDataProvider;
use Oro\Bundle\PromotionBundle\Discount\Converter\LineItemsToDiscountLineItemsConverter;
use Oro\Bundle\PromotionBundle\Discount\DiscountLineItem;
use Oro\Bundle\PromotionBundle\Discount\Exception\UnsupportedSourceEntityException;
use Oro\Bundle\ScopeBundle\Manager\ScopeManager;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Manager\CartTotalManager;

/**
 * Data converter that prepares promotion context data based on shopping list entity to filter applicable promotions.
 */
class CartContextDataConverter implements ContextDataConverterInterface
{
    protected CriteriaDataProvider $criteriaDataProvider;
    protected LineItemsToDiscountLineItemsConverter $lineItemsConverter;
    protected UserCurrencyManager $userCurrencyManager;
    protected ScopeManager $scopeManager;
    protected CartTotalManager $cartTotalManager;

    public function __construct(
        CriteriaDataProvider $criteriaDataProvider,
        LineItemsToDiscountLineItemsConverter $lineItemsConverter,
        UserCurrencyManager $userCurrencyManager,
        ScopeManager $scopeManager,
        CartTotalManager $cartTotalManager
    ) {
        $this->criteriaDataProvider = $criteriaDataProvider;
        $this->lineItemsConverter = $lineItemsConverter;
        $this->userCurrencyManager = $userCurrencyManager;
        $this->scopeManager = $scopeManager;
        $this->cartTotalManager = $cartTotalManager;
    }

    /**
     * @param Cart $entity
     * {@inheritdoc}
     */
    public function getContextData($entity): array
    {
        if (!$this->supports($entity)) {
            throw new UnsupportedSourceEntityException(
                sprintf('Entity "%s" is not supported.', get_class($entity))
            );
        }

        $customerUser = $this->criteriaDataProvider->getCustomerUser($entity);
        $customer = $this->criteriaDataProvider->getCustomer($entity);
        $customerGroup = $this->criteriaDataProvider->getCustomerGroup($entity);

        $scopeContext = [
            'customer' => $customer,
            'customerGroup' => $customerGroup,
            'website' => $this->criteriaDataProvider->getWebsite($entity)
        ];

        $currency = $this->userCurrencyManager->getUserCurrency();

        return [
            self::CUSTOMER_USER => $customerUser,
            self::CUSTOMER => $customer,
            self::CUSTOMER_GROUP => $customerGroup,
            self::LINE_ITEMS => $this->getLineItems($entity),
            self::SUBTOTAL => $this->getSubtotalAmount($entity, $currency),
            self::CURRENCY => $currency,
            self::CRITERIA => $this->scopeManager->getCriteria('promotion', $scopeContext),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supports($entity): bool
    {
        return $entity instanceof Cart;
    }

    /**
     * @param ProductLineItemsHolderInterface $entity
     *
     * @return DiscountLineItem[]|array
     */
    protected function getLineItems(ProductLineItemsHolderInterface $entity): array
    {
        return $this->lineItemsConverter->convert($entity->getLineItems()->toArray());
    }

    /**
     * @param Cart $sourceEntity
     * @param string $currency
     *
     * @return float
     */
    protected function getSubtotalAmount(Cart $sourceEntity, string $currency): float
    {
        return $this->cartTotalManager
            ->getCartTotalForCurrency($sourceEntity, $currency)
            ->getSubtotal()
            ->getAmount();
    }
}
