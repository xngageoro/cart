@fixture-OroShoppingListBundle:ShoppingListFixture.yml

Feature: Add line items from Shopping List to Cart
  In order to have ability to manage cart
  As a Buyer
  I want to add items from Shopping List to Cart

  Scenario: Add to Cart
    Given I login as AmandaRCole@example.org buyer
    And Buyer is on "Shopping List 5" shopping list
    And I click "Shopping List Actions"
    And I click "Edit"
    Then I should see following grid:
      | SKU | Qty Update All |
      | AA1 | 1 set          |
      | AA1 | 2 item         |
    When I select cart line item 1 in "Shopping List Line Items Table"
    And I click "Xngage Grid Mass Actions"
    And I click "Add to Cart"
    And I click "Yes, Add"
    And I click "Cart Widget"
    Then I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 1        | set  |

  Scenario: Add to Cart existing in Cart item merges quantity
    Given I login as AmandaRCole@example.org buyer
    And Buyer is on "Shopping List 5" shopping list
    And I click "Shopping List Actions"
    And I click "Edit"
    Then I should see following grid:
      | SKU | Qty Update All |
      | AA1 | 1 set          |
      | AA1 | 2 item         |
    When I select cart line item 1 in "Shopping List Line Items Table"
    And I click "Xngage Grid Mass Actions"
    And I click "Add to Cart"
    And I click "Yes, Add"
    And I click "Cart Widget"
    Then I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 2        | set  |
