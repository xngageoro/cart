@fixture-XngageCartBundle:CartFixture.yml
@fixture-XngageCartBundle:ShoppingListFixture.yml
@fixture-XngageCartBundle:ProductsWithPricesFixture.yml
Feature: Guest shopping list merging functionality
  As a guest I have a possibility to add products to Cart and it should be merged to customer user on login

  Scenario: Feature Background
    Given sessions active:
      | Buyer | first_session |
    And I enable configuration options:
      | xngage_cart.availability_for_guests |
      | oro_checkout.guest_checkout          |

  Scenario: Check Customer User Cart line items
    Given I proceed as the Buyer
    And I login as NancyJSallee@example.org buyer
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 1        | set  |
      | AA1   | 2        | item |

  Scenario: Create shopping list as a guest
    Given I click "Sign Out"
    And I am on homepage
    Then I should see "Cart"
    When I type "CONTROL1" in "search"
    And I click "Search Button"
    And I should see "Control Product"
    When I click "Add to Cart" for "CONTROL1" product
    Then I should see "Product has been added to Cart" flash message
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU      | Quantity  | Unit |
      | CONTROL1 | 1         | each |

  Scenario: Cart line items are merged to Customer User on login
    Given I signed in as NancyJSallee@example.org on the store frontend in old session
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU      | Quantity | Unit |
      | AA1      | 1        | set  |
      | AA1      | 2        | item |
      | CONTROL1 | 1        | each |
