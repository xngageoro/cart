@fixture-XngageCartBundle:CartFixture.yml

Feature: Carts on Customer User view page
  In order to understand which carts are associated with the customer user
  As an Administrator
  I want to have a grid with related Carts on Customer User view page

  Scenario: Check Customer User view page
    Given I login as administrator
    And I go to Customers / Customer Users
    And I filter "Email Address" as contains "AmandaRCole"
    When I click view AmandaRCole in grid
    Then I should see following "Customer User Carts Grid" grid:
      | Label  | Notes | Subtotal |
      | Cart 1 |       | $0.00    |