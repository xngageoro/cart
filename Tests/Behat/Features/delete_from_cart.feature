@fixture-XngageCartBundle:CartFixture.yml

Feature: Delete line item from Shopping List
  In order to have ability to manage cart
  As a Buyer
  I want to delete line item from Cart

  Scenario: Delete line item from Cart
    Given I login as NancyJSallee@example.org buyer
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 1        | set  |
      | AA1   | 2        | item |
    And I delete cart line item 1 in "Cart Line Items Table"
    And I click "Yes, Delete"
    Then I should see 'The "Product1" product was successfully deleted' flash message
    And I should see following cart line items in "Cart Line Items Table":
      | SKU | Quantity | Unit |
      | AA1 | 2        | item |

  Scenario: Remove product from Cart on PLP
    Given I type "AA1" in "search"
    When I click "Search Button"
    Then I should see "Product1"
    When I click on "Cart Dropdown"
    And I click "Remove From Cart"
    Then I should see "Product has been removed from Cart" flash message
    And I click "Cart Widget"
    Then I should see "There are no line items"
    And I should not see "Create Order"
