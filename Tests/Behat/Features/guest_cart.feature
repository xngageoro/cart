@fixture-XngageCartBundle:ProductsWithPricesFixture.yml
Feature: Add and Update items in Cart as Guest
  In order to place order form Cart
  As a Guest
  I want to be able to add and update products in Cart from PDP and PLP

  Scenario: Feature Background
    Given I enable configuration options:
      | xngage_cart.availability_for_guests |
      | oro_checkout.guest_checkout          |

  Scenario: Check Cart is available on frontend
    When I am on homepage
    Then I should see "Cart"
    When type "SKU003" in "search"
    And I click "Search Button"
    Then I should see "Product3"
    But I should see "Add to Cart"

  Scenario: Add product to cart from PLP (search)
    Given I type "CONTROL1" in "search"
    And I click "Search Button"
    And I should see "Control Product"
    When I click "Add to Cart" for "CONTROL1" product
    Then I should see "Product has been added to Cart" flash message

  Scenario: Check Update product in Cart
    Given I should see "Update Cart"
    When I fill "FrontendLineItemForm" with:
      | Quantity | 10   |
    And I click "Update Cart" for "CONTROL1" product
    Then I should see "Record has been successfully updated" flash message

  Scenario: There is a product in cart and user can create order
    Given I am on homepage
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU      | Quantity  | Unit | Price |
      | CONTROL1 | 10        | each | $2.00 |
    And I should see following buttons:
      | Create Order |

  Scenario: Add product from PDP
    Given I type "SKU003" in "search"
    And I click "Search Button"
    Then I should see "Product3"
    When I click "View Details" for "SKU003" product
    Then I should see "Add to Cart"
    When I click "Add to Cart"
    Then I should see "Product has been added to Cart" flash message
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU      | Quantity | Unit | Price |
      | CONTROL1 | 10       | each | $2.00 |
      | SKU003   | 1        | each | $3.00 |
