@fixture-XngageCartBundle:ProductsWithPricesFixture.yml
Feature: Add to Cart from Quick Order Form
  In order to quickly add products to Cart
  As a Buyer
  I want to be able to add products to Cart from Quick Order Form

  Scenario: Add to shopping list from quick order page
    Given I login as AmandaRCole@example.org buyer
    And I click "Quick Order Form"
    And I fill "QuickAddForm" with:
      | SKU1 | SKU003   |
      | SKU2 | CONTROL1 |
      | SKU3 | PSKU1    |
    And I wait for products to load
    And I click "Add to Cart"
    Then I should see "3 products were added (view cart)." flash message
    And I click "Cart Widget"
    Then I should see following cart line items in "Cart Line Items Table":
      | SKU      | Quantity | Unit |
      | PSKU1    | 1        | each |
      | CONTROL1 | 1        | each |
      | SKU003   | 1        | item |
