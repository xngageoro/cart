@fixture-XngageCartBundle:CartFixture.yml

Feature: Shopping Lists on Customer view page
  In order to understand which carts are associated with the customer
  As an Administrator
  I want to have a grid with related Carts on Customer view page

  Scenario: Check Customer view page
    Given I login as administrator
    And I go to Customers / Customers
    And I filter "Name" as contains "first customer"
    When I click view first customer in grid
    Then I should see following "Customer Carts Grid" grid:
      | Customer User | Label  | Notes | Subtotal |
      | Nancy Sallee  | Cart 3 |       | $0.00    |
      | Amanda Cole   | Cart 1 |       | $0.00    |
