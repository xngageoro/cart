@fixture-XngageCartBundle:CartFixture.yml
@fixture-XngageCartBundle:ShoppingListFixture.yml

Feature: Move line items from Cart to Shopping List
  In order to have ability to manage cart
  As a Buyer
  I want to move items from Cart to existing or new Shopping List

  Scenario: Move to existing Shopping List
    Given I login as NancyJSallee@example.org buyer
    And I click "Cart Widget"
    And I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 1        | set  |
      | AA1   | 2        | item |
    When I select cart line item 1 in "Cart Line Items Table"
    And should see Move to Shopping List action
    And I click "Move to Shopping List" link from mass action dropdown
    And I click "Shopping List Action Move Radio"
    And I click "Shopping List Action Submit"
    Then I should see "One entity has been moved successfully." flash message
    And  I should see following cart line items in "Cart Line Items Table":
      | SKU   | Quantity | Unit |
      | AA1   | 2        | item |

#    TODO: Implement further, shopping list related tests when shopping list context is fixed

