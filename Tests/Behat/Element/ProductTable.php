<?php

namespace Xngage\Bundle\CartBundle\Tests\Behat\Element;

use Oro\Bundle\TestFrameworkBundle\Behat\Element\Table;

class ProductTable extends Table
{
    private const PRODUCT_TABLE_ROW_ELEMENT = 'Cart Product Table Row';

    /**
     * {@inheritdoc}
     */
    public function getProductRows(): array
    {
        return $this->getElements(self::PRODUCT_TABLE_ROW_ELEMENT);
    }
}
