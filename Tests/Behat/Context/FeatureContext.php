<?php

namespace Xngage\Bundle\CartBundle\Tests\Behat\Context;

use Behat\Gherkin\Node\TableNode;
use Oro\Bundle\TestFrameworkBundle\Behat\Context\OroFeatureContext;
use Oro\Bundle\TestFrameworkBundle\Behat\Element\OroPageObjectAware;
use Oro\Bundle\TestFrameworkBundle\Behat\Element\Table;
use Oro\Bundle\TestFrameworkBundle\Behat\Element\TableRow;
use Oro\Bundle\TestFrameworkBundle\Tests\Behat\Context\PageObjectDictionary;

/**
 * The context for testing Shopping List related features.
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class FeatureContext extends OroFeatureContext implements OroPageObjectAware
{
    use PageObjectDictionary;

    /**
     * Finds the delete button for a line item by its row number for the given cart and clicks it.
     *
     * Example: When I delete line item 1 in "Cart Line Items Table"
     *
     * @param integer   $itemPosition
     * @param string    $cart
     *
     * @When I delete cart line item :itemPosition in :cart
     */
    public function iClickDeleteCartLineItemNumberIn($itemPosition, $cart)
    {
        /** @var Table $cartItemsTableElement */
        $cartItemsTableElement = $this->elementFactory->createElement($cart);
        self::assertTrue(
            $cartItemsTableElement->isValid(),
            sprintf('Element "%s" was not found', $cart)
        );

        $rows = $cartItemsTableElement->getProductRows();
        /** @var TableRow $row */
        $row = $rows[$itemPosition - 1];
        $button = $row->find('css', 'a.action[title="Delete"]');

        $button->click();
    }

    /**
     * Finds the select item checkbox for a line item by its row number for the given cart and clicks it.
     *
     * Example: When I select line item 1 in "Cart Line Items Table"
     *
     * @param integer   $itemPosition
     * @param string    $cart
     *
     * @When I select cart line item :itemPosition in :cart
     */
    public function iClickSelectCartLineItemNumberIn($itemPosition, $cart)
    {
        /** @var Table $cartItemsTableElement */
        $cartItemsTableElement = $this->elementFactory->createElement($cart);
        self::assertTrue(
            $cartItemsTableElement->isValid(),
            sprintf('Element "%s" was not found', $cart)
        );

        $rows = $cartItemsTableElement->getProductRows();
        /** @var TableRow $row */
        $row = $rows[$itemPosition - 1];
        $button = $row->find('css', '.custom-checkbox__icon');

        $button->click();
    }

    /**
     * @param string    $cart
     * @param TableNode $table
     *
     * @When I should see following cart line items in :arg1:
     */
    public function iShouldSeeFollowingCartLineItemsIn($cart, TableNode $table)
    {
        /** @var Table $cartItemsTableElement */
        $cartItemsTableElement = $this->createValidCartTableElement($cart);

        $rows = $cartItemsTableElement->getProductRows();
        $tableRows = $table->getRows();
        $columnHeaders = reset($tableRows);

        $actualRows = [];
        foreach ($rows as $rowElement) {
            $actualRows[] = $this->getLineItemRowColumnsValues($rowElement, $columnHeaders);
        }

        self::assertEquals($table->getHash(), $actualRows);
    }

    /**
     * @param string $cart
     *
     * @return Table
     */
    private function createValidCartTableElement(string $cart)
    {
        /** @var Table $cartItemsTableElement */
        $cartItemsTableElement = $this->elementFactory->createElement($cart);

        self::assertTrue(
            $cartItemsTableElement->isValid(),
            sprintf('Element "%s" was not found', $cart)
        );

        return $cartItemsTableElement;
    }

    /**
     * @param TableRow $rowElement
     * @param array $columns
     *
     * @return array
     */
    private function getLineItemRowColumnsValues(TableRow $rowElement, array $columns): array
    {
        $values = [];
        foreach ($columns as $columnTitle) {
            switch (strtolower($columnTitle)) {
                case 'sku':
                    $currentValue = $this->getLineItemSKU($rowElement);
                    break;
                case 'quantity':
                    $currentValue = $this->getLineItemQuantity($rowElement);
                    break;
                case 'unit':
                    $currentValue = $this->getLineItemUnit($rowElement);
                    break;
                case 'price':
                    $currentValue = $this->getLineItemPrice($rowElement);
                    break;
                default:
                    throw new \InvalidArgumentException(
                        sprintf(
                            '%s column is not supported, supported columns is %s',
                            $columnTitle,
                            implode(', ', ['Sku', 'Quantity', 'Unit', 'Price'])
                        )
                    );
                    break;
            }

            $values[$columnTitle] = $currentValue;
        }

        return $values;
    }

    /**
     * @param TableRow $tableRowElement
     *
     * @return string
     */
    private function getLineItemSKU(TableRow $tableRowElement)
    {
        return $tableRowElement->find('css', '.grid-body-cell-sku')->getText();
    }

    /**
     * @param TableRow $tableRowElement
     *
     * @return string
     */
    private function getLineItemQuantity(TableRow $tableRowElement)
    {
        return $tableRowElement->find('css', '.grid-body-cell-quantity .input-qty')->getText();
    }

    /**
     * @param TableRow $tableRowElement
     *
     * @return string
     */
    private function getLineItemUnit(TableRow $tableRowElement)
    {
        $select = $tableRowElement->find('css', '.grid-body-cell-quantity div.select[data-focused=".select2-container"]');
        if ($select) {
            return $select->getText();
        } else {
            return $tableRowElement->find('css', '.product__static-unit')->getText();
        }
    }

    /**
     * @param TableRow $tableRowElement
     *
     * @return string
     */
    private function getLineItemPrice(TableRow $tableRowElement)
    {
        return $this->createElement('Cart Line Item Product Price', $tableRowElement)->getText();
    }
}
