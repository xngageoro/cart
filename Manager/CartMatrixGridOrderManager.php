<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollectionColumn;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

/**
 * Provides matrix product collection which will used to update configurable products.
 */
class CartMatrixGridOrderManager extends MatrixGridOrderManager
{
    /**
     * {@inheritDoc}
     */
    public function createLineItemFromMatrixColumn(
        MatrixCollectionColumn $column,
        MatrixCollection $collection,
        Product $product
    ): CartLineItem {
        $lineItem = new CartLineItem();
        $lineItem->setProduct($column->product);
        $lineItem->setQuantity((float) $column->quantity);
        $lineItem->setUnit($collection->unit);

        if ($product->isConfigurable()) {
            $lineItem->setParentProduct($product);
        }

        return $lineItem;
    }
}
