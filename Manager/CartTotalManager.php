<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\PricingBundle\Manager\UserCurrencyManager;
use Oro\Bundle\PricingBundle\SubtotalProcessor\Provider\LineItemNotPricedSubtotalProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartTotal;

/**
 * Provides methods to manage cart totals.
 */
class CartTotalManager
{
    protected DoctrineHelper $doctrineHelper;
    protected LineItemNotPricedSubtotalProvider $lineItemNotPricedSubtotalProvider;
    protected UserCurrencyManager $currencyManager;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        LineItemNotPricedSubtotalProvider $lineItemNotPricedSubtotalProvider,
        UserCurrencyManager $currencyManager
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->lineItemNotPricedSubtotalProvider = $lineItemNotPricedSubtotalProvider;
        $this->currencyManager = $currencyManager;
    }

    /**
     * Sets Cart Subtotal for user current currency
     *
     * @param Cart $cart
     * @param bool $doFlush
     */
    public function setSubtotal(Cart $cart, bool $doFlush = true): void
    {
        $currency = $this->currencyManager->getUserCurrency();
        /** @var CartTotal|null $cartTotal */
        $cartTotal = $this->getTotalRepository()
            ->findOneBy(['cart' => $cart, 'currency' => $currency]);

        $totals = $cart->getTotals();
        if (!$cartTotal && !$totals->containsKey($currency)) {
            $cartTotal = new CartTotal($cart, $currency);
            $cart->addTotal($cartTotal);
            $this->getEntityManager()->persist($cart);
        }

        $cartTotal = $cartTotal ?? $totals->get($currency);
        if (!$cartTotal->isValid()) {
            $subtotal = $this->lineItemNotPricedSubtotalProvider->getSubtotalByCurrency($cart, $currency);
            $cartTotal->setSubtotal($subtotal)
                ->setValid(true);
        }

        $cart->setSubtotal($cartTotal->getSubtotal());

        if ($doFlush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Cart $cart
     * @param bool $doFlush
     */
    public function recalculateTotals(Cart $cart, bool $doFlush): void
    {
        $totals = $cart->getTotals();
        $enabledCurrencies = $this->currencyManager->getAvailableCurrencies();

        foreach ($totals as $total) {
            $subtotal = $this->lineItemNotPricedSubtotalProvider
                ->getSubtotalByCurrency($cart, $total->getCurrency());
            if (($key = array_search($total->getCurrency(), $enabledCurrencies, true)) !== false) {
                unset($enabledCurrencies[$key]);
            }
            $total->setSubtotal($subtotal)
                ->setValid(true);
        }

        foreach ($enabledCurrencies as $currency) {
            $total = new CartTotal($cart, $currency);
            $cart->addTotal($total);
            $subtotal = $this->lineItemNotPricedSubtotalProvider
                ->getSubtotalByCurrency($cart, $total->getCurrency());

            $total->setSubtotal($subtotal)
                ->setValid(true);
            $this->getEntityManager()->persist($total);
        }

        if ($doFlush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Cart $cart
     * @param string $currency
     * @param bool $doFlush
     *
     * @return CartTotal
     */
    public function getCartTotalForCurrency(
        Cart $cart,
        string $currency,
        bool $doFlush = false
    ): CartTotal {
        return $this->getTotalsForCurrencies($cart, [$currency], false, $doFlush)[$currency];
    }

    /**
     * @param Cart $cart
     * @param array $currencies
     * @param bool $recalculate
     * @param bool $doFlush
     *
     * @return array
     */
    private function getTotalsForCurrencies(
        Cart $cart,
        array $currencies,
        bool $recalculate,
        bool $doFlush
    ): array {
        $cartTotals = [];
        $currencies = array_flip($currencies);
        foreach ($cart->getTotals() as $eachCartTotal) {
            $eachCurrency = $eachCartTotal->getCurrency();
            if (!isset($currencies[$eachCurrency])) {
                continue;
            }

            if ($recalculate || !$eachCartTotal->isValid()) {
                $subtotal = $this->lineItemNotPricedSubtotalProvider
                    ->getSubtotalByCurrency($cart, $eachCurrency);

                $eachCartTotal
                    ->setSubtotal($subtotal)
                    ->setValid(true);
            }

            $cartTotals[$eachCurrency] = $eachCartTotal;
            unset($currencies[$eachCurrency]);
        }

        $entityManager = $this->getEntityManager();
        $isCartManaged = $entityManager->contains($cart);

        foreach ($currencies as $eachCurrency => $i) {
            $subtotal = $this->lineItemNotPricedSubtotalProvider->getSubtotalByCurrency($cart, $eachCurrency);

            $cartTotals[$eachCurrency] = (new CartTotal($cart, $eachCurrency))
                ->setSubtotal($subtotal)
                ->setValid(true);

            $cart->addTotal($cartTotals[$eachCurrency]);

            // It is possible that cart which came to this method is not managed dy doctrine, we should not
            // persist corresponding total in this case.
            if ($isCartManaged) {
                $entityManager->persist($cartTotals[$eachCurrency]);
            }
        }

        if ($doFlush && $isCartManaged) {
            $entityManager->flush();
        }

        return $cartTotals;
    }

    /**
     * @return ObjectRepository
     */
    protected function getTotalRepository(): ObjectRepository
    {
        return $this->doctrineHelper->getEntityRepositoryForClass(CartTotal::class);
    }

    /**
     * @return EntityManager|null
     */
    protected function getEntityManager(): ?EntityManager
    {
        return $this->doctrineHelper->getEntityManagerForClass(CartTotal::class);
    }
}
