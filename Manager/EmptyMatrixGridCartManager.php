<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\Proxy;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\DependencyInjection\Configuration;
use Oro\Bundle\ProductBundle\Entity\Product;
use Xngage\Bundle\CartBundle\CartLineItem\Factory\LineItemFactoryInterface;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Entity\Repository\CartRepository;

/**
 * Adds empty line item (with empty matrix) of configurable product to the cart
 */
class EmptyMatrixGridCartManager implements EmptyMatrixGridInterface
{
    protected DoctrineHelper $doctrineHelper;
    protected LineItemFactoryInterface $lineItemFactory;
    protected ConfigManager $configManager;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        LineItemFactoryInterface $lineItemFactory,
        ConfigManager $configManager
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->lineItemFactory = $lineItemFactory;
        $this->configManager = $configManager;
    }

    /**
     * {@inheritdoc}
     */
    public function addEmptyMatrix(MatrixCollectionEntityInterface $entity, Product $product)
    {
        /** @var Cart $entity */
        if ($this->hasProductVariants($entity, $product)) {
            return;
        }

        if ($this->hasConfigurableProduct($entity, $product)) {
            return;
        }

        $this->addConfigurableProduct($entity, $product);
    }

    /**
     * {@inheritdoc}
     */
    public function hasEmptyMatrix(MatrixCollectionEntityInterface $entity): bool
    {
        $lineItemsCollection = $entity->getLineItems();
        $result = false;
        if ($this->isTooManyUninitializedProducts($lineItemsCollection)) {
            /** @var Cart $entity */
            /** @var CartRepository $repository */
            $repository = $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class);
            $result = $repository->hasEmptyConfigurableLineItems($entity);
        } else {
            foreach ($lineItemsCollection as $lineItem) {
                if ($lineItem->getProduct()->isConfigurable()) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function isAddingEmptyMatrixAllowed(array $lineItems): bool
    {
        return $this->lineItemQuantitiesAreEmpty($lineItems) && $this->isEmptyMatrixAllowedInConfig();
    }

    /**
     * @param Cart $cart
     * @param Product $product
     *
     * @return bool
     */
    protected function hasProductVariants(Cart $cart, Product $product): bool
    {
        return $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class)->findOneBy([
            'cart' => $cart,
            'unit' => $product->getPrimaryUnitPrecision()->getUnit(),
            'parentProduct' => $product,
        ]) !== null;
    }

    /**
     * @param Cart $cart
     * @param Product $product
     *
     * @return bool
     */
    protected function hasConfigurableProduct(Cart $cart, Product $product): bool
    {
        return $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class)->findOneBy([
            'cart' => $cart,
            'unit' => $product->getPrimaryUnitPrecision()->getUnit(),
            'product' => $product,
        ]) !== null;
    }

    /**
     * @param Cart $cart
     * @param Product $product
     */
    protected function addConfigurableProduct(Cart $cart, Product $product)
    {
        $entityManager = $this->doctrineHelper->getEntityManagerForClass(CartLineItem::class);

        $entityManager->persist($this->lineItemFactory->create($cart, $product));
        $entityManager->flush();
    }

    /**
     * @param Collection $lineItemsCollection
     * @return bool
     */
    protected function isTooManyUninitializedProducts(Collection $lineItemsCollection): bool
    {
        $result = false;
        if ($lineItemsCollection->isEmpty()) {
            return $result;
        }

        if ($lineItemsCollection instanceof AbstractLazyCollection && !$lineItemsCollection->isInitialized()) {
            $result = true;
        } else {
            $notInitializedCount = 0;
            $lineItemsCollection->first();
            do {
                $product = $lineItemsCollection->current()->getProduct();
                if ($product instanceof Proxy && !$product->__isInitialized()) {
                    $notInitializedCount ++;
                    if ($notInitializedCount > 2) {
                        $result = true;
                        break;
                    }
                }
            } while ($lineItemsCollection->next());
        }

        return $result;
    }

    /**
     * @param CartLineItem[] $lineItems
     *
     * @return bool
     */
    protected function lineItemQuantitiesAreEmpty(array $lineItems): bool
    {
        foreach ($lineItems as $item) {
            if ($item->getQuantity() > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function isEmptyMatrixAllowedInConfig(): bool
    {
        return $this->configManager->get(
            sprintf('%s.%s', Configuration::ROOT_NODE, Configuration::MATRIX_FORM_ALLOW_TO_ADD_EMPTY)
        );
    }
}
