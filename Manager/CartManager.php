<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\EntityBundle\Handler\EntityDeleteHandlerRegistry;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Provider\ProductVariantAvailabilityProvider;
use Oro\Bundle\ProductBundle\Rounding\QuantityRoundingService;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Entity\Repository\CartLineItemRepository;

class CartManager
{
    protected DoctrineHelper $doctrineHelper;
    protected ConfigManager $configManager;
    protected EntityDeleteHandlerRegistry $deleteHandlerRegistry;
    protected ProductVariantAvailabilityProvider $productVariantProvider;
    protected QuantityRoundingService $rounding;
    protected CartTotalManager $totalManager;
    protected CurrentCartManager $currentCartManager;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        ConfigManager $configManager,
        EntityDeleteHandlerRegistry $deleteHandlerRegistry,
        ProductVariantAvailabilityProvider $productVariantProvider,
        QuantityRoundingService $rounding,
        CartTotalManager $totalManager,
        CurrentCartManager $currentCartManager
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->configManager = $configManager;
        $this->deleteHandlerRegistry = $deleteHandlerRegistry;
        $this->productVariantProvider = $productVariantProvider;
        $this->rounding = $rounding;
        $this->totalManager = $totalManager;
        $this->currentCartManager = $currentCartManager;
    }

    /**
     * Removes cart line items containing products with unavailable inventory statuses.
     * Recalculates subtotals if line items were removed.
     *
     * @param Cart $cart
     */
    public function actualizeLineItems(Cart $cart)
    {
        /** @var CartLineItemRepository $repository */
        $repository = $this->doctrineHelper
            ->getEntityRepositoryForClass(CartLineItem::class);

        $allowedStatuses = $this->configManager->get('oro_product.general_frontend_product_visibility');
        if ($repository->deleteNotAllowedLineItemsFromCart($cart, $allowedStatuses)) {
            $this->totalManager->recalculateTotals($cart, true);
        }
    }

    /**
     * @param CartLineItem $lineItem
     * @param Cart $cart
     * @param bool $flush
     * @param bool $concatNotes
     */
    public function addLineItem(CartLineItem $lineItem, Cart $cart, $flush = true, $concatNotes = false)
    {
        $func = function (CartLineItem $duplicate) use ($lineItem, $concatNotes) {
            $this->mergeLineItems($lineItem, $duplicate, $concatNotes);
        };

        $this
            ->prepareLineItem($lineItem, $cart)
            ->handleLineItem($lineItem, $cart, $func);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param CartLineItem|MatrixCollectionLineItemInterface $lineItem
     * @param Cart $cart
     */
    public function updateLineItem(CartLineItem $lineItem, Cart $cart)
    {
        $func = function (CartLineItem $duplicate) use ($lineItem) {
            if ($lineItem->getQuantity() > 0) {
                $this->updateLineItemQuantity($lineItem, $duplicate);
            } else {
                $this->removeLineItem($duplicate, true);
            }
        };

        $this
            ->prepareLineItem($lineItem, $cart)
            ->handleLineItem($lineItem, $cart, $func);

        $this->getEntityManager()->flush();
    }

    /**
     * Removes the given line item. In case if line item is the part of matrix representation - removes all
     * line items of the product from the given line item.
     *
     * @param CartLineItem $lineItem
     * @param bool $removeOnlyCurrentItem
     * @param bool $flush
     *
     * @return int Number of deleted line items
     */
    public function removeLineItem(CartLineItem $lineItem, bool $removeOnlyCurrentItem = false, bool $flush = true): int
    {
        $parentProduct = $lineItem->getParentProduct();
        if ($removeOnlyCurrentItem || !$parentProduct) {
            $this->deleteHandlerRegistry->getHandler(CartLineItem::class)->delete($lineItem, $flush);

            // return 1 because only the specified line item was deleted
            return 1;
        }

        return $this->removeProduct(
            $lineItem->getCart(),
            $parentProduct ?: $lineItem->getProduct(),
            $flush
        );
    }

    /**
     * @param array $lineItems
     * @param int $batchSize
     * @param Cart $cart
     *
     * @return int
     */
    public function bulkAddLineItems(array $lineItems, $batchSize, Cart $cart = null): int
    {
        if (!$cart) {
            $cart = $this->currentCartManager->getCurrent();
        }

        $lineItemsCount = count($lineItems);
        for ($iteration = 1; $iteration <= $lineItemsCount; $iteration++) {
            $flush = $iteration % $batchSize === 0 || $lineItemsCount === $iteration;
            $this->addLineItem($lineItems[$iteration - 1], $cart, $flush);
        }

        return $lineItemsCount;
    }

    /**
     * @param array $lineItems
     * @param int $batchSize
     *
     * @return int
     */
    public function bulkRemoveLineItems(array $lineItems, $batchSize): int
    {
        $lineItemsCount = count($lineItems);
        for ($iteration = 1; $iteration <= $lineItemsCount; $iteration++) {
            $flush = $iteration % $batchSize === 0 || $lineItemsCount === $iteration;
            $this->removeLineItem($lineItems[$iteration - 1], true, $flush);
        }

        return $lineItemsCount;
    }

    /**
     * @param Cart         $cart
     * @param Product      $product
     * @param bool         $flush
     *
     * @return int Number of removed line items
     */
    public function removeProduct(Cart $cart, Product $product, bool $flush = true): int
    {
        $products = [];
        if ($product->isConfigurable()) {
            $products = $this->productVariantProvider->getSimpleProductsByVariantFields($product);
        }
        $products[] = $product;

        $lineItems = $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class)
            ->getItemsByCartAndProducts($cart, $products);
        $this->deleteLineItems($lineItems, $flush);

        return count($lineItems);
    }

    /**
     * @param Cart $cart
     */
    public function clearLineItems($cart)
    {
        if (!$cart) {
            $cart = $this->currentCartManager->getCurrent();
        }

        $this->deleteLineItems($cart->getLineItems()->toArray());
    }

    /**
     * @param CartLineItem $lineItem
     * @param CartLineItem $duplicate
     * @param bool     $concatNotes
     */
    protected function mergeLineItems(CartLineItem $lineItem, CartLineItem $duplicate, $concatNotes)
    {
        $quantity = $this->rounding->roundQuantity(
            $duplicate->getQuantity() + $lineItem->getQuantity(),
            $duplicate->getUnit(),
            $duplicate->getProduct()
        );
        $duplicate->setQuantity($quantity);

        if ($concatNotes) {
            $notes = trim(implode(' ', [$duplicate->getNotes(), $lineItem->getNotes()]));
            $duplicate->setNotes($notes);
        }
    }

    /**
     * Set new quantity for $duplicate line item based on quantity value from $lineItem
     *
     * @param CartLineItem $lineItem
     * @param CartLineItem $duplicate
     */
    protected function updateLineItemQuantity(CartLineItem $lineItem, CartLineItem $duplicate)
    {
        $quantity = $this->rounding->roundQuantity(
            $lineItem->getQuantity(),
            $duplicate->getUnit(),
            $duplicate->getProduct()
        );
        $duplicate->setQuantity($quantity);
    }

    /**
     * @param CartLineItem $lineItem
     * @param Cart $cart
     *
     * @return $this
     */
    protected function prepareLineItem(CartLineItem $lineItem, Cart $cart): self
    {
        if (null === $lineItem->getCustomerUser() && $cart->getCustomerUser()) {
            $lineItem->setCustomerUser($cart->getCustomerUser());
        }
        if (null === $lineItem->getOrganization() && $cart->getOrganization()) {
            $lineItem->setOrganization($cart->getOrganization());
        }

        return $this;
    }

    /**
     * @param CartLineItem $lineItem
     * @param Cart $cart
     * @param \Closure $func
     *
     * @return $this
     */
    protected function handleLineItem(CartLineItem $lineItem, Cart $cart, \Closure $func): self
    {
        $em = $this->getEntityManager();
        $duplicate = $this->getLineItemRepository()->findDuplicateInCart($lineItem, $cart);
        if ($duplicate) {
            $func($duplicate);
            $em->remove($lineItem);
        } elseif ($lineItem->getQuantity() > 0 || !$lineItem->getProduct()->isSimple()) {
            $cart->addLineItem($lineItem);
            $em->persist($lineItem);
        }

        $this->totalManager->recalculateTotals($cart, false);

        return $this;
    }

    /**
     * @param CartLineItem[] $lineItems
     * @param bool       $flush
     */
    protected function deleteLineItems(array $lineItems, bool $flush = true)
    {
        if (!$lineItems) {
            return;
        }

        $handler = $this->deleteHandlerRegistry->getHandler(CartLineItem::class);
        $flushAllOptions = [];
        foreach ($lineItems as $lineItem) {
            $flushAllOptions[] = $handler->delete($lineItem, false);
        }
        if ($flush) {
            $handler->flushAll($flushAllOptions);
        }
    }

    /**
     * @return CustomerUser|null
     */
    protected function getCustomerUser(): ?CustomerUser
    {
        return $this->currentCartManager->getCustomerUser();
    }

    /**
     * @return EntityManager|null
     */
    protected function getEntityManager(): ?EntityManager
    {
        return $this->doctrineHelper->getEntityManagerForClass(Cart::class);
    }

    /**
     * @return EntityRepository|CartLineItemRepository
     */
    protected function getLineItemRepository()
    {
        return $this->doctrineHelper->getEntityRepositoryForClass(CartLineItem::class);
    }
}
