<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Doctrine\ORM\EntityManager;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface;
use Oro\Bundle\WebsiteBundle\Manager\WebsiteManager;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CurrentCartManager implements MatrixCollectionCurrentEntityManagerInterface
{
    protected DoctrineHelper $doctrineHelper;
    protected GuestCartManager $guestCartManager;
    protected AuthorizationCheckerInterface $authorizationChecker;
    protected WebsiteManager $websiteManager;
    protected TranslatorInterface $translator;
    protected TokenAccessorInterface $tokenAccessor;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        GuestCartManager $guestCartManager,
        AuthorizationCheckerInterface $authorizationChecker,
        WebsiteManager $websiteManager,
        TranslatorInterface $translator,
        TokenAccessorInterface $tokenAccessor
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->guestCartManager = $guestCartManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->websiteManager = $websiteManager;
        $this->translator = $translator;
        $this->tokenAccessor = $tokenAccessor;
    }

    /**
     * @return Cart|null
     */
    public function getCurrent(): ?Cart
    {
        if ($this->guestCartManager->isGuestToken()) {
            if ($this->guestCartManager->isGuestCartAvailable()) {
                return $this->guestCartManager->getOrCreate();
            }

            return null;
        }

        return $this->getOrCreate();
    }

    /**
     * @return Cart|null
     */
    public function getOrCreate(): ?Cart
    {
        $cart = $this->findExisting();

        if (!$cart && $this->authorizationChecker->isGranted('xngage_cart_frontend_create')) {
            return $this->create();
        }

        return $cart;
    }

    /**
     * @return Cart|null
     */
    public function findExisting(): ?Cart
    {
        return $this->doctrineHelper->getEntityRepositoryForClass(Cart::class)
            ->findOneByCustomerUser($this->getCustomerUser());
    }

    /**
     * @return Cart
     */
    public function create(): Cart
    {
        $customerUser = $this->getCustomerUser();

        $cart = new Cart();
        $cart
            ->setOrganization($customerUser->getOrganization())
            ->setCustomer($customerUser->getCustomer())
            ->setCustomerUser($customerUser)
            ->setCustomerVisitor(null)
            ->setWebsite($this->websiteManager->getCurrentWebsite());

        $cart->setLabel($this->translator->trans('xngage.cart.default.label'));

        $em = $this->getEntityManager();
        $em->persist($cart);
        $em->flush();

        return $cart;
    }

    /**
     * @return EntityManager|null
     */
    protected function getEntityManager(): ?EntityManager
    {
        return $this->doctrineHelper->getEntityManagerForClass(Cart::class);
    }

    /**
     * @return CustomerUser|null
     */
    public function getCustomerUser(): ?CustomerUser
    {
        $user = $this->tokenAccessor->getUser();

        if (!($user instanceof CustomerUser)) {
            throw new \LogicException('The customer user does not exist in the security context.');
        }

        return $user;
    }
}
