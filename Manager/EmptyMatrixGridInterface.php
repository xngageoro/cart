<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\ProductBundle\Entity\Product;

interface EmptyMatrixGridInterface
{
    /**
     * @param MatrixCollectionEntityInterface $entity
     * @param Product $product
     */
    public function addEmptyMatrix(MatrixCollectionEntityInterface $entity, Product $product);

    /**
     * @param MatrixCollectionLineItemInterface[] $lineItems
     * @return bool
     */
    public function isAddingEmptyMatrixAllowed(array $lineItems): bool;

    /**
     * @param MatrixCollectionEntityInterface $entity
     * @return bool
     */
    public function hasEmptyMatrix(MatrixCollectionEntityInterface $entity): bool;
}
