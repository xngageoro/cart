<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollectionColumn;

interface MatrixGridOrderManagerInterface
{
    /**
     * @param Product $product
     * @param MatrixCollectionEntityInterface|null $entity
     *
     * @return mixed
     */
    public function getMatrixCollection(Product $product, MatrixCollectionEntityInterface $entity = null);

    /**
     * @param Product $product
     * @param ProductUnit $unit
     * @param MatrixCollectionEntityInterface $entity
     *
     * @return mixed
     */
    public function getMatrixCollectionForUnit(
        Product $product,
        ProductUnit $unit,
        MatrixCollectionEntityInterface $entity
    );

    /**
     * @param MatrixCollection $collection
     * @param Product $product
     * @param $requiredCollection
     *
     * @return mixed
     */
    public function convertMatrixIntoLineItems(MatrixCollection $collection, Product $product, $requiredCollection);

    /**
     * @param MatrixCollectionEntityInterface $entity
     * @param Product $product
     * @param array $lineItems
     *
     * @return mixed
     */
    public function addEmptyMatrixIfAllowed(
        MatrixCollectionEntityInterface $entity,
        Product $product,
        array $lineItems
    );

    /**
     * @param MatrixCollectionColumn $column
     * @param MatrixCollection $collection
     * @param Product $product
     *
     * @return mixed
     */
    public function createLineItemFromMatrixColumn(
        MatrixCollectionColumn $column,
        MatrixCollection $collection,
        Product $product
    );
}
