<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\CustomerBundle\Entity\CustomerVisitor;
use Oro\Bundle\CustomerBundle\Security\Token\AnonymousCustomerUserToken;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\WebsiteBundle\Entity\Website;
use Oro\Bundle\WebsiteBundle\Manager\WebsiteManager;
use Xngage\Bundle\CartBundle\Authorization\CartAuthorizationChecker;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GuestCartManager
{
    protected DoctrineHelper $doctrineHelper;
    protected TokenStorageInterface $tokenStorage;
    protected WebsiteManager $websiteManager;
    protected TranslatorInterface $translator;
    protected CartAuthorizationChecker $cartAuthorizationChecker;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        TokenStorageInterface $tokenStorage,
        WebsiteManager $websiteManager,
        TranslatorInterface $translator,
        CartAuthorizationChecker $cartAuthorizationChecker
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->tokenStorage = $tokenStorage;
        $this->websiteManager = $websiteManager;
        $this->translator = $translator;
        $this->cartAuthorizationChecker = $cartAuthorizationChecker;
    }

    /**
     * @return bool
     */
    public function isGuestCartAvailable(): bool
    {
        return $this->cartAuthorizationChecker->isGuestTokenAndGuestFeatureEnabled();
    }

    /**
     * @return Cart
     */
    public function getOrCreate(): Cart
    {
        $cart = $this->findExisting();

        return $cart ?: $this->create();
    }

    /**
     * @return Cart|null
     */
    public function findExisting(): ?Cart
    {
        $token = $this->getToken();
        $customerVisitor = $this->getCustomerVisitor($token);
        $currentWebsite = $this->getCurrentWebsite();

        /** @var Cart $cart */
        $cart = $this->doctrineHelper->getEntityRepositoryForClass(Cart::class)->findOneBy(
            [
                'customerVisitor' => $customerVisitor,
                'website' => $currentWebsite
            ]
        );

        return $cart;
    }

    /**
     * @return Cart
     */
    public function create(): Cart
    {
        $token = $this->getToken();
        $customerVisitor = $this->getCustomerVisitor($token);
        $currentWebsite = $this->getCurrentWebsite();

        $cart = new Cart();
        $cart
            ->setOrganization($currentWebsite->getOrganization())
            ->setCustomer(null)
            ->setCustomerUser(null)
            ->setCustomerVisitor($customerVisitor)
            ->setWebsite($currentWebsite);

        $cart->setLabel($this->translator->trans('xngage.cart.default.label'));

        $em = $this->doctrineHelper->getEntityManagerForClass(Cart::class);
        $em->persist($cart);
        $em->flush();

        return $cart;
    }

    /**
     * @return bool
     */
    public function isGuestToken(): bool
    {
        return $this->tokenStorage->getToken() instanceof AnonymousCustomerUserToken;
    }

    /**
     * @return AnonymousCustomerUserToken
     */
    protected function getToken(): AnonymousCustomerUserToken
    {
        $token = $this->tokenStorage->getToken();
        if (!$token instanceof AnonymousCustomerUserToken) {
            throw new \LogicException(sprintf('Token should be instance of %s.', AnonymousCustomerUserToken::class));
        }

        return $token;
    }

    /**
     * @param AnonymousCustomerUserToken $token
     *
     * @return CustomerVisitor
     */
    protected function getCustomerVisitor(AnonymousCustomerUserToken $token): CustomerVisitor
    {
        $customerVisitor = $token->getVisitor();
        if ($customerVisitor === null) {
            throw new \LogicException('Customer visitor is empty.');
        }

        return $customerVisitor;
    }

    /**
     * @return Website
     */
    protected function getCurrentWebsite(): Website
    {
        $currentWebsite = $this->websiteManager->getCurrentWebsite();
        if ($currentWebsite === null) {
            throw new \LogicException('Current website is empty.');
        }

        return $currentWebsite;
    }
}
