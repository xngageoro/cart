<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\CustomerBundle\Entity\CustomerVisitor;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\Repository\CartRepository;

class GuestCartMigrationManager
{
    public const FLUSH_BATCH_SIZE = 100;

    protected CartManager $cartManager;
    protected DoctrineHelper $doctrineHelper;

    public function __construct(
        CartManager $cartManager,
        DoctrineHelper $doctrineHelper
    ) {
        $this->cartManager = $cartManager;
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * Migrate guest-created cart to customer user's
     *
     * @param CustomerVisitor $customerVisitor
     */
    public function migrateGuestCart(CustomerVisitor $customerVisitor)
    {
        /** @var CartRepository $cartRepository */
        $cartRepository = $this->doctrineHelper->getEntityRepositoryForClass(Cart::class);
        $guestCart = $cartRepository->findOneByVisitor($customerVisitor);

        if ($guestCart) {
            $cartLineItems = clone $guestCart->getLineItems();

            $em = $this->doctrineHelper->getEntityManagerForClass(Cart::class);
            $em->remove($guestCart);
            if (count($cartLineItems) === 0) {
                $em->flush();

                return;
            }

            $this->cartManager->bulkAddLineItems(
                $cartLineItems->toArray(),
                self::FLUSH_BATCH_SIZE
            );
        }
    }
}
