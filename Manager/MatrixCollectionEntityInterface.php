<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Doctrine\Common\Collections\Collection;

/** Provides entity parameters needed for MatrixCollection */
interface MatrixCollectionEntityInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return MatrixCollectionLineItemInterface[]|Collection
     */
    public function getLineItems();
}
