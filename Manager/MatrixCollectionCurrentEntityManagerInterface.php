<?php

namespace Xngage\Bundle\CartBundle\Manager;

interface MatrixCollectionCurrentEntityManagerInterface
{
    public function getCurrent(): ?MatrixCollectionEntityInterface;
}
