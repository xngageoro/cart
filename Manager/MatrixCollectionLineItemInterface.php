<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\ProductBundle\Entity\Product;

/** Provides line item parameters needed for MatrixCollection */
interface MatrixCollectionLineItemInterface
{
    /**
     * @return Product
     */
    public function getProduct();

    /**
     * @return Product|null
     */
    public function getParentProduct();

    /**
     * Get productUnitCode
     *
     * @return string
     */
    public function getProductUnitCode();
}
