<?php

namespace Xngage\Bundle\CartBundle\Manager;

use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\ProductBundle\Entity\ProductUnitPrecision;
use Oro\Bundle\ProductBundle\Provider\ProductVariantAvailabilityProvider;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollection;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollectionColumn;
use Oro\Bundle\ShoppingListBundle\Model\MatrixCollectionRow;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Provides matrix product collection which will used to update configurable products.
 */
abstract class MatrixGridOrderManager implements MatrixGridOrderManagerInterface
{
    protected PropertyAccessor $propertyAccessor;
    protected ProductVariantAvailabilityProvider $variantAvailability;
    protected EmptyMatrixGridInterface $emptyMatrixGridManager;

    /** @var array|MatrixCollection[] */
    protected array $collectionCache = [];

    public function __construct(
        PropertyAccessor $propertyAccessor,
        ProductVariantAvailabilityProvider $variantAvailability,
        EmptyMatrixGridInterface $emptyMatrixGridManager
    ) {
        $this->propertyAccessor = $propertyAccessor;
        $this->variantAvailability = $variantAvailability;
        $this->emptyMatrixGridManager = $emptyMatrixGridManager;
    }

    /**
     * @param Product $product
     * @param MatrixCollectionEntityInterface|null $entity
     *
     * @return MatrixCollection
     */
    public function getMatrixCollection(
        Product $product,
        MatrixCollectionEntityInterface $entity = null
    ): MatrixCollection {
        $entityId = $entity ? $entity->getId() : null;
        if (isset($this->collectionCache[$product->getId()][$entityId])) {
            return $this->collectionCache[$product->getId()][$entityId];
        }

        $variantFields = $this->getVariantFields($product);
        $availableVariants = $this->getAvailableVariants($product, $variantFields);

        $collection = new MatrixCollection();
        $collection->unit = $product->getPrimaryUnitPrecision()->getUnit();

        if (!isset($variantFields[0])) {
            return $collection;
        }

        foreach ($variantFields[0]['values'] as $firstValue) {
            $row = new MatrixCollectionRow();
            $row->label = $firstValue['label'];

            if (count($variantFields) === 1) {
                $column = new MatrixCollectionColumn();
                if (isset($availableVariants[$firstValue['value']]['_product'])) {
                    $column->product = $availableVariants[$firstValue['value']]['_product'];
                    $column->quantity = $this->getQuantity(
                        $product->getPrimaryUnitPrecision()->getUnit(),
                        $column->product,
                        $entity
                    );
                }

                $row->columns = [$column];
            } else {
                foreach ($variantFields[1]['values'] as $secondValue) {
                    $column = new MatrixCollectionColumn();
                    $column->label = $secondValue['label'];

                    if (isset($availableVariants[$firstValue['value']][$secondValue['value']]['_product'])) {
                        $column->product = $availableVariants[$firstValue['value']][$secondValue['value']]['_product'];
                        $column->quantity = $this->getQuantity(
                            $product->getPrimaryUnitPrecision()->getUnit(),
                            $column->product,
                            $entity
                        );
                    }

                    $row->columns[] = $column;
                }
            }

            $collection->rows[] = $row;
        }

        return $this->collectionCache[$product->getId()][$entityId] = $collection;
    }

    /**
     * @param Product $product
     * @param ProductUnit $unit
     * @param MatrixCollectionEntityInterface $entity
     *
     * @return MatrixCollection
     */
    public function getMatrixCollectionForUnit(
        Product $product,
        ProductUnit $unit,
        MatrixCollectionEntityInterface $entity
    ) {
        $entityId = $entity->getId();
        $unitCode = $unit->getCode();
        if (isset($this->collectionCache[$product->getId()][$unitCode][$entityId])) {
            return $this->collectionCache[$product->getId()][$unitCode][$entityId];
        }

        $variantFields = $this->getVariantFields($product);
        $availableVariants = $this->getAvailableVariants($product, $variantFields, $unit);

        $collection = new MatrixCollection();
        $collection->unit = $unit;

        if (!isset($variantFields[0])) {
            return $collection;
        }

        foreach ($variantFields[0]['values'] as $firstValue) {
            $row = new MatrixCollectionRow();
            $row->label = $firstValue['label'];

            if (count($variantFields) == 1) {
                $column = new MatrixCollectionColumn();
                if (isset($availableVariants[$firstValue['value']]['_product'])) {
                    $column->product = $availableVariants[$firstValue['value']]['_product'];
                    $column->quantity = $this->getQuantity($unit, $column->product, $entity);
                }

                $row->columns = [$column];
            } else {
                foreach ($variantFields[1]['values'] as $secondValue) {
                    $column = new MatrixCollectionColumn();
                    $column->label = $secondValue['label'];

                    if (isset($availableVariants[$firstValue['value']][$secondValue['value']]['_product'])) {
                        $column->product = $availableVariants[$firstValue['value']][$secondValue['value']]['_product'];
                        $column->quantity = $this->getQuantity($unit, $column->product, $entity);
                    }
                    $row->columns[] = $column;
                }
            }
            $collection->rows[] = $row;
        }
        return $this->collectionCache[$product->getId()][$unitCode][$entityId] = $collection;
    }

    /**
     * @param MatrixCollection $collection
     * @param Product $product
     * @param array $requiredCollection Matrix collection from a request
     *
     * @return array|MatrixCollectionLineItemInterface[]
     */
    public function convertMatrixIntoLineItems(
        MatrixCollection $collection,
        Product $product,
        $requiredCollection
    ): array {
        $lineItems = [];
        $rowIds    = [];

        // For partial operations, we must use required rows only
        if (isset($requiredCollection['rows'])) {
            $rowIds = array_keys($requiredCollection['rows']);
        }

        /** @var MatrixCollectionRow $row */
        foreach ($collection->rows as $rowIndex => $row) {
            if (in_array($rowIndex, $rowIds, true)) {
                /** @var MatrixCollectionColumn $column */
                foreach ($row->columns as $column) {
                    if ($column->product) {
                        $lineItem = $this->createLineItemFromMatrixColumn($column, $collection, $product);
                        $lineItems[] = $lineItem;
                    }
                }
            }
        }

        return $lineItems;
    }

    /**
     * @param MatrixCollectionEntityInterface $entity
     * @param Product $product
     * @param MatrixCollectionLineItemInterface[] $lineItems
     */
    public function addEmptyMatrixIfAllowed(MatrixCollectionEntityInterface $entity, Product $product, array $lineItems)
    {
        if ($this->emptyMatrixGridManager->isAddingEmptyMatrixAllowed($lineItems)) {
            $this->emptyMatrixGridManager->addEmptyMatrix($entity, $product);
        }
    }

    /**
     * @param MatrixCollectionColumn $column
     * @param MatrixCollection $collection
     * @param Product $product
     */
    abstract public function createLineItemFromMatrixColumn(
        MatrixCollectionColumn $column,
        MatrixCollection $collection,
        Product $product
    );

    /**
     * Get variant fields with values for product
     *
     * @param Product $product
     *
     * @return array ex.: [['name' => 'color', 'values' => [['value' => 'red', 'label' => 'Red'], ...]], ...]
     */
    protected function getVariantFields(Product $product): array
    {
        $variantFields = [];

        $fieldNames = array_keys($this->variantAvailability->getVariantFieldsAvailability($product));
        foreach ($fieldNames as $fieldName) {
            $values = $this->variantAvailability->getVariantFieldValues($fieldName);

            $formattedValues = [];
            foreach ($values as $label => $value) {
                $formattedValues[] = [
                    'value' => $value,
                    'label' => $label,
                ];
            }

            $variantFields[] = [
                'name' => $fieldName,
                'values' => $formattedValues
            ];
        }

        return $variantFields;
    }

    /**
     * Get all available variants for product grouped by variant field[s] value[s]
     *
     * @param Product $product
     * @param array $variantFields
     * @param ProductUnit|null $unit
     * @return array ex.: ['red' => ['xxl' => ['product' => object(Product)#1], ...], ...]
     * @throws \InvalidArgumentException
     */
    protected function getAvailableVariants(Product $product, array $variantFields, ProductUnit $unit = null): array
    {
        if (!$unit) {
            $unit = $product->getPrimaryUnitPrecision()->getUnit();
        }

        $availableVariants = [];

        $variants = $this->variantAvailability->getSimpleProductsByVariantFields($product);
        foreach ($variants as $variant) {
            if (!$this->doSimpleProductSupportsUnitPrecision($variant, $unit)) {
                continue;
            }

            $values = [];
            foreach ($variantFields as $field) {
                $value = $this->variantAvailability->getVariantFieldScalarValue($variant, $field['name']);
                if (is_bool($value)) {
                    $value = ($value) ? '1' : '0';
                }
                $values[] = "[$value]";
            }
            $values[] = '[_product]';

            $this->propertyAccessor->setValue($availableVariants, implode('', $values), $variant);
        }

        return $availableVariants;
    }

    /**
     * @param Product $product
     * @param ProductUnit $unit
     *
     * @return bool
     */
    protected function doSimpleProductSupportsUnitPrecision(Product $product, ProductUnit $unit): bool
    {
        $productUnits = $product->getUnitPrecisions()->map(
            function (ProductUnitPrecision $unitPrecision) {
                return $unitPrecision->getUnit();
            }
        );

        return $productUnits->contains($unit);
    }

    /**
     * Get MatrixCollectionColumn's quantity by entity line items
     *
     * @param ProductUnit $productUnit
     * @param Product $cellProduct
     * @param MatrixCollectionEntityInterface|null  $entity
     *
     * @return float|null
     */
    protected function getQuantity(
        ProductUnit $productUnit,
        Product $cellProduct,
        MatrixCollectionEntityInterface $entity = null
    ): ?float {
        if (!$entity) {
            return null;
        }
        $lineItems = $entity->getLineItems();
        if ($lineItems->isEmpty()) {
            return null;
        }

        /** @var MatrixCollectionLineItemInterface $lineItem */
        foreach ($lineItems->getIterator() as $lineItem) {
            if ($cellProduct->getId() === $lineItem->getProduct()->getId()
                && $lineItem->getProductUnitCode() === $productUnit->getCode()
            ) {
                return $lineItem->getQuantity();
            }
        }

        return null;
    }
}
