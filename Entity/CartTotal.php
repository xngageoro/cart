<?php

namespace Xngage\Bundle\CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\PricingBundle\SubtotalProcessor\Model\Subtotal;
use Oro\Bundle\PricingBundle\SubtotalProcessor\Provider\LineItemNotPricedSubtotalProvider;
use Xngage\Bundle\CartBundle\Model\ExtendCartTotal;

/**
 * Entity for caching cart subtotals data by currency
 * If isValid=false values should be recalculated
 *
 * @ORM\Table(
 *     name="xngage_cart_total",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="unique_cart_currency", columns={
 *              "cart_id",
 *              "currency"
 *          })
 *      }
 * )
 * @ORM\Entity(repositoryClass="Xngage\Bundle\CartBundle\Entity\Repository\CartTotalRepository")
 * @Config()
 **/
class CartTotal extends ExtendCartTotal
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Cart
     *
     * @ORM\ManyToOne(targetEntity="Xngage\Bundle\CartBundle\Entity\Cart", inversedBy="totals")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $cart;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255, nullable=false)
     */
    protected $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="subtotal_value", type="money", nullable=true)
     */
    protected $subtotalValue;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean")
     */
    protected $valid = false;

    /**
     * @param Cart $cart
     * @param string $currency
     */
    public function __construct(Cart $cart, $currency)
    {
        $this->cart = $cart;
        $this->currency = $currency;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @param boolean $valid
     * @return $this
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @param Subtotal $subtotal
     * @return $this
     */
    public function setSubtotal(Subtotal $subtotal)
    {
        if ($subtotal->getCurrency() !== $this->currency) {
            throw new \InvalidArgumentException();
        }

        $this->subtotalValue = $subtotal->getAmount();

        return $this;
    }

    /**
     * @return Subtotal
     */
    public function getSubtotal()
    {
        $subtotal = new Subtotal();
        $subtotal->setAmount($this->subtotalValue)
            ->setCurrency($this->currency)
            ->setType(LineItemNotPricedSubtotalProvider::TYPE)
            ->setLabel(LineItemNotPricedSubtotalProvider::LABEL);

        return $subtotal;
    }
}
