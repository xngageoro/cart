<?php

namespace Xngage\Bundle\CartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\CustomerBundle\Entity\CustomerOwnerAwareInterface;
use Oro\Bundle\CustomerBundle\Entity\CustomerVisitor;
use Oro\Bundle\CustomerBundle\Entity\CustomerVisitorOwnerAwareInterface;
use Oro\Bundle\CustomerBundle\Entity\Ownership\AuditableFrontendCustomerUserAwareTrait;
use Oro\Bundle\EntityBundle\EntityProperty\DatesAwareTrait;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\OrganizationBundle\Entity\OrganizationAwareInterface;
use Oro\Bundle\PricingBundle\SubtotalProcessor\Model\LineItemsNotPricedAwareInterface;
use Oro\Bundle\PricingBundle\SubtotalProcessor\Model\Subtotal;
use Oro\Bundle\ProductBundle\Model\ProductLineItemsHolderInterface;
use Oro\Bundle\UserBundle\Entity\Ownership\UserAwareTrait;
use Oro\Bundle\WebsiteBundle\Entity\Website;
use Oro\Bundle\WebsiteBundle\Entity\WebsiteBasedCurrencyAwareInterface;
use Oro\Component\Checkout\Entity\CheckoutSourceEntityInterface;
use Xngage\Bundle\CartBundle\Manager\MatrixCollectionEntityInterface;
use Xngage\Bundle\CartBundle\Model\ExtendCart;

/**
 * Cart entity
 *
 * @ORM\Table(
 *      name="xngage_cart",
 *      indexes={
 *          @ORM\Index(name="orlab_cart_created_at_idx", columns={"created_at"})
 *      },
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="xngage_cart_uidx",
 *              columns={"customer_user_id", "customer_visitor_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="Xngage\Bundle\CartBundle\Entity\Repository\CartRepository")
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(name="customerUser",
 *          joinColumns=@ORM\JoinColumn(name="customer_user_id", referencedColumnName="id", onDelete="CASCADE")
 *      )
 * })
 * @Config(
 *      routeName="xngage_cart_index",
 *      routeView="xngage_cart_view",
 *      defaultValues={
 *          "entity"={
 *              "icon"="fa-shopping-cart"
 *          },
 *          "ownership"={
 *              "owner_type"="USER",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="user_owner_id",
 *              "frontend_owner_type"="FRONTEND_USER",
 *              "frontend_owner_field_name"="customerUser",
 *              "frontend_owner_column_name"="customer_user_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "dataaudit"={
 *              "auditable"=true
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"="commerce",
 *              "category"="shopping"
 *          }
 *      }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class Cart extends ExtendCart implements
    OrganizationAwareInterface,
    LineItemsNotPricedAwareInterface,
    CustomerOwnerAwareInterface,
    CustomerVisitorOwnerAwareInterface,
    WebsiteBasedCurrencyAwareInterface,
    CheckoutSourceEntityInterface,
    \JsonSerializable,
    ProductLineItemsHolderInterface,
    MatrixCollectionEntityInterface
{
    use DatesAwareTrait;
    use AuditableFrontendCustomerUserAwareTrait;
    use UserAwareTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $label;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $notes;

    /**
     * @var Website
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\WebsiteBundle\Entity\Website")
     * @ORM\JoinColumn(name="website_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $website;

    /**
     * @var Collection|CartLineItem[]
     *
     * @ORM\OneToMany(
     *      targetEntity="CartLineItem",
     *      mappedBy="cart",
     *      cascade={"ALL"},
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"id" = "ASC"})
     **/
    protected $lineItems;

    /**
     * @var Collection|CartTotal[]
     *
     * @ORM\OneToMany(
     *      targetEntity="Xngage\Bundle\CartBundle\Entity\CartTotal",
     *      mappedBy="cart",
     *      cascade={"ALL"},
     *      orphanRemoval=true,
     *      indexBy="currency"
     * )
     **/
    protected $totals;

    /**
     * @var CustomerVisitor
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\CustomerBundle\Entity\CustomerVisitor", cascade={"persist"})
     * @ORM\JoinColumn(name="customer_visitor_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $customerVisitor;

    /**
     * @var Subtotal
     */
    protected $subtotal;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct();

        $this->lineItems = new ArrayCollection();
        $this->totals = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->label;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     *
     * @return \Xngage\Bundle\CartBundle\Entity\Cart
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @param CartLineItem $item
     *
     * @return $this
     */
    public function addLineItem(CartLineItem $item)
    {
        if (!$this->lineItems->contains($item)) {
            $item->setCart($this);
            $this->lineItems->add($item);
        }

        return $this;
    }

    /**
     * @param CartLineItem $item
     *
     * @return $this
     */
    public function removeLineItem(CartLineItem $item)
    {
        if ($item->getId() === null) {
            if ($this->lineItems->contains($item)) {
                $this->lineItems->removeElement($item);
            }

            return $this;
        }

        foreach ($this->lineItems as $lineItem) {
            if ($item->getId() === $lineItem->getId()) {
                $this->lineItems->removeElement($lineItem);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartLineItem[]
     */
    public function getLineItems()
    {
        return $this->lineItems;
    }

    /**
     * @param CartTotal $item
     *
     * @return $this
     */
    public function addTotal(CartTotal $item)
    {
        $this->totals->set($item->getCurrency(), $item);

        return $this;
    }

    /**
     * @param CartTotal $item
     *
     * @return $this
     */
    public function removeTotal(CartTotal $item)
    {
        if ($this->totals->contains($item)) {
            $this->totals->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|CartTotal[]
     */
    public function getTotals()
    {
        return $this->totals;
    }

    /**
     * Pre persist event handler
     *
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
    }

    /**
     * Pre update event handler
     *
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
    }

    /**
     * @param Website $website
     *
     * @return $this
     */
    public function setWebsite(Website $website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getId();
    }

    /**
     * @return $this
     */
    public function getSourceDocument()
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSourceDocumentIdentifier()
    {
        return $this->label;
    }

    /**
     * @return Subtotal
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param Subtotal $subtotal
     */
    public function setSubtotal(Subtotal $subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * @return CustomerVisitor|null
     */
    public function getCustomerVisitor()
    {
        return $this->customerVisitor;
    }

    /**
     * @param CustomerVisitor|null $customerVisitor
     * @return $this
     */
    public function setCustomerVisitor(CustomerVisitor $customerVisitor = null)
    {
        $this->customerVisitor = $customerVisitor;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getVisitor()
    {
        return $this->customerVisitor;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
        ];
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
            $this->lineItems = clone $this->lineItems;
            $this->totals = clone $this->totals;
        }
    }
}
