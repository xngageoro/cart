<?php

namespace Xngage\Bundle\CartBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Oro\Bundle\BatchBundle\ORM\Query\ResultIterator\IdentifierHydrator;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SecurityBundle\Acl\BasicPermission;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class CartLineItemRepository extends EntityRepository
{
    /**
     * @param Cart $cart
     * @param array $allowedInventoryStatuses
     * @return int Number of deleted line items
     */
    public function deleteNotAllowedLineItemsFromCart(
        Cart $cart,
        array $allowedInventoryStatuses
    ): int {
        $lineItemsQb = $this->createQueryBuilder('line_item');
        $lineItemsQuery = $lineItemsQb
            ->select('line_item.id')
            ->innerJoin('line_item.product', 'product')
            ->where(
                $lineItemsQb->expr()->orX(
                    $lineItemsQb->expr()->notIn('IDENTITY(product.inventory_status)', ':allowedInventoryStatuses'),
                    $lineItemsQb->expr()->eq('product.status', ':status')
                ),
                $lineItemsQb->expr()->eq('line_item.cart', ':cart')
            )
            ->setParameter('allowedInventoryStatuses', $allowedInventoryStatuses)
            ->setParameter('status', Product::STATUS_DISABLED)
            ->setParameter('cart', $cart)
            ->getQuery();

        $identifierHydrationMode = 'IdentifierHydrator';

        $lineItemsQuery->getEntityManager()
            ->getConfiguration()
            ->addCustomHydrationMode($identifierHydrationMode, IdentifierHydrator::class);

        $ids = $lineItemsQuery->getResult($identifierHydrationMode);
        $deletedCount = 0;
        if ($ids) {
            $deleteQb = $this->getEntityManager()->createQueryBuilder();
            $deletedCount = $deleteQb->delete()
                ->from($this->getEntityName(), 'line_item')
                ->where($deleteQb->expr()->in('line_item.id', ':ids'))
                ->getQuery()
                ->execute(['ids' => $ids]);
        }

        return $deletedCount;
    }

    /**
     * @param Cart $cart
     * @param Product[] $products
     *
     * @return array|CartLineItem[]
     */
    public function getItemsByCartAndProducts(Cart $cart, array $products): array
    {
        $qb = $this->createQueryBuilder('li');
        $qb->select('li')
            ->where('li.cart = :cart', $qb->expr()->in('li.product', ':product'))
            ->setParameter('cart', $cart)
            ->setParameter('product', $products)
            ->addOrderBy($qb->expr()->asc('li.id'));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Cart $cart
     *
     * @return array|CartLineItem[]
     */
    public function getItemsWithProductByCart(Cart $cart): array
    {
        $qb = $this->createQueryBuilder('li');
        $qb->select('li, product, names')
            ->join('li.product', 'product')
            ->join('product.names', 'names')
            ->where('li.cart = :cart')
            ->setParameter('cart', $cart)
            ->addOrderBy($qb->expr()->asc('li.id'));

        return $qb->getQuery()->getResult();
    }

    /**
     * Find line item with the same product and unit
     *
     * @param CartLineItem $lineItem
     * @param Cart|null $cart
     *
     * @return CartLineItem|null
     */
    public function findDuplicateInCart(CartLineItem $lineItem, ?Cart $cart): ?CartLineItem
    {
        $qb = $this->createQueryBuilder('li');
        $qb->where('li.product = :product')
            ->andWhere('li.unit = :unit')
            ->andWhere('li.cart = :cart')
            ->setParameter('product', $lineItem->getProduct())
            ->setParameter('unit', $lineItem->getUnit())
            ->setParameter('cart', $cart)
            ->addOrderBy($qb->expr()->asc('li.id'))
            ->setMaxResults(1);

        if ($lineItem->getId()) {
            $qb
                ->andWhere('li.id != :currentId')
                ->setParameter('currentId', $lineItem->getId());
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param AclHelper $aclHelper
     * @param Cart $cart
     * @param array|Product[] $products
     *
     * @return CartLineItem[]
     */
    public function getProductItemsByCart(AclHelper $aclHelper, Cart $cart, array $products): array
    {
        $qb = $this->createQueryBuilder('li');
        $qb->select('li')
            ->join('li.product', 'product')
            ->leftJoin('product.parentVariantLinks', 'parentVariantLinks')
            ->andWhere('li.cart = :cart')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->in('product', ':products'),
                    $qb->expr()->in('li.parentProduct', ':products'),
                    $qb->expr()->in('parentVariantLinks.parentProduct', ':products')
                )
            )
            ->setParameter('cart', $cart)
            ->setParameter('products', $products)
            ->addOrderBy($qb->expr()->asc('li.id'));

        return $aclHelper->apply($qb, BasicPermission::EDIT)->getResult();
    }

    /**
     * @param int $cartId
     * @return bool
     */
    public function hasEmptyMatrix(int $cartId): bool
    {
        $qb = $this->createQueryBuilder('li');
        $qb->select('li.quantity, p.type, p.id, IDENTITY(li.parentProduct) as parent')
            ->join('li.product', 'p')
            ->where($qb->expr()->eq('li.cart', ':cartId'))
            ->setParameter('cartId', $cartId);

        $configurable = [];
        $simple = [];

        foreach ($qb->getQuery()->getArrayResult() as $row) {
            if ($row['type'] === Product::TYPE_CONFIGURABLE) {
                $configurable[] = $row['id'];
                continue;
            }

            if (!$row['parent'] || $row['quantity'] <= 0) {
                continue;
            }

            $simple[$row['parent']][] = $row['id'];
        }

        foreach ($configurable as $id) {
            if (!isset($simple[$id]) || !count($simple[$id])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $cartId
     * @return bool
     */
    public function canBeGrouped(int $cartId): bool
    {
        $qb = $this->createQueryBuilder('li');
        $qb->resetDQLPart('select')
            ->select($qb->expr()->count('li.id'))
            ->where(
                $qb->expr()->in('li.cart', ':cartId'),
                $qb->expr()->isNotNull('li.parentProduct')
            )
            ->setParameter('cartId', $cartId)
            ->groupBy('li.parentProduct')
            ->having($qb->expr()->gt($qb->expr()->count('li.id'), 1))
            ->setMaxResults(1);

        return (bool) $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int $cartId
     * @param int $productId
     * @param string $unitCode
     * @return array
     */
    public function findLineItemsByParentProductAndUnit(int $cartId, int $productId, string $unitCode): array
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();

        return $this
            ->createQueryBuilder('line_item')
            ->where($expr->eq('IDENTITY(line_item.cart)', ':cart_id'))
            ->andWhere(
                $expr->orX(
                    $expr->eq('line_item.parentProduct', ':product_id'),
                    $expr->eq('line_item.product', ':product_id')
                )
            )
            ->andWhere($expr->eq('line_item.unit', ':unit_code'))
            ->getQuery()
            ->execute(['cart_id' => $cartId, 'product_id' => $productId, 'unit_code' => $unitCode]);
    }
}
