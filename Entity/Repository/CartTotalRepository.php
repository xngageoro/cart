<?php

namespace Xngage\Bundle\CartBundle\Entity\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Oro\Bundle\PricingBundle\Entity\CombinedProductPrice;
use Oro\Bundle\PricingBundle\Entity\ProductPrice;
use Oro\Bundle\WebsiteBundle\Entity\Website;
use Oro\Component\DoctrineUtils\ORM\QueryBuilderUtil;
use Oro\Component\DoctrineUtils\ORM\ResultSetMappingUtil;
use Oro\Component\DoctrineUtils\ORM\SqlQueryBuilder;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;

class CartTotalRepository extends EntityRepository
{
    /**
     * Invalidate Cart subtotals by given Combined Price List ids
     *
     * @param array $combinedPriceListIds
     */
    public function invalidateByCombinedPriceList(array $combinedPriceListIds): void
    {
        $this->invalidateByBasePriceList($combinedPriceListIds, CombinedProductPrice::class);
    }

    /**
     * Invalidate Cart subtotals by given Price List ids
     *
     * @param array $priceListIds
     */
    public function invalidateByPriceList(array $priceListIds): void
    {
        $this->invalidateByBasePriceList($priceListIds, ProductPrice::class);
    }

    /**
     * Invalidate Cart subtotals by given Base Price List ids
     *
     * @param array $priceListIds
     * @param string $priceClass
     */
    protected function invalidateByBasePriceList(array $priceListIds, string $priceClass): void
    {
        if (!$priceListIds) {
            return;
        }

        $subQuery = $this->getEntityManager()->createQueryBuilder();
        $subQuery->select('1')
            ->from(CartLineItem::class, 'lineItem')
            ->join(
                $priceClass,
                'productPrice',
                Join::WITH,
                $subQuery->expr()->eq('lineItem.product', 'productPrice.product')
            )
            ->where(
                $subQuery->expr()->eq('total.cart', 'lineItem.cart'),
                $subQuery->expr()->in('productPrice.priceList', ':priceLists')
            );

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update($this->getEntityName(), 'total')
            ->set('total.valid', ':newIsValid')
            ->where(
                $qb->expr()->eq('total.valid', ':isValid'),
                $qb->expr()->exists($subQuery)
            )
            ->setParameter('newIsValid', false, Types::BOOLEAN)
            ->setParameter('priceLists', $priceListIds, Connection::PARAM_INT_ARRAY)
            ->setParameter('isValid', true, Types::BOOLEAN);

        $qb->getQuery()->execute();
    }

    /**
     * @param array $customerIds
     * @param int $websiteId
     */
    public function invalidateByCustomers(array $customerIds, int $websiteId)
    {
        if (empty($customerIds)) {
            return;
        }

        $expr = $this->getEntityManager()->getExpressionBuilder();
        $updateQB = $this->getBaseInvalidateQb($websiteId);
        $updateQB->andWhere($expr->in('c.customer_id', ':customerIds'));
        $updateQB->setParameter('customerIds', $customerIds, Connection::PARAM_INT_ARRAY);

        $updateQB->execute();
    }

    /**
     * @param array $customerGroupIds
     * @param int $websiteId
     */
    public function invalidateByCustomerGroupsForFlatPricing(array $customerGroupIds, int $websiteId)
    {
        if (empty($customerGroupIds)) {
            return;
        }

        $expr = $this->getEntityManager()->getExpressionBuilder();
        $updateQB = $this->getBaseInvalidateQb($websiteId);
        $updateQB
            ->innerJoin('c', 'oro_customer', 'cus', $expr->eq('c.customer_id', 'cus.id'))
            ->innerJoin(
                'cus',
                'oro_price_list_to_cus_group',
                'pl2cg',
                $expr->andX(
                    $expr->eq('pl2cg.customer_group_id', 'cus.group_id'),
                    $expr->eq('pl2cg.website_id', 'c.website_id')
                )
            )
            ->leftJoin(
                'cus',
                'oro_price_list_to_customer',
                'pl2cus',
                $expr->andX(
                    $expr->eq('pl2cus.customer_id', 'cus.id'),
                    $expr->eq('pl2cus.website_id', 'c.website_id')
                )
            )
            ->andWhere(
                $expr->andX(
                    $expr->isNull('pl2cus.id'),
                    $expr->eq('c.website_id', ':websiteId'),
                    $expr->in('cus.group_id', ':customerGroupIds')
                )
            )
            ->setParameter('websiteId', $websiteId, Types::INTEGER)
            ->setParameter('customerGroupIds', $customerGroupIds, Connection::PARAM_INT_ARRAY);

        $updateQB->execute();
    }

    /**
     * @param array $websiteIds
     */
    public function invalidateByWebsitesForFlatPricing(array $websiteIds)
    {
        if (empty($websiteIds)) {
            return;
        }

        $updateQB = $this->getInvalidateQb();
        $expr = $this->getEntityManager()->getExpressionBuilder();

        $updateQB
            ->innerJoin('c', 'oro_customer', 'cus', $expr->eq('c.customer_id', 'cus.id'))
            ->leftJoin(
                'cus',
                'oro_price_list_to_cus_group',
                'pl2cg',
                $expr->andX(
                    $expr->eq('pl2cg.customer_group_id', 'cus.group_id'),
                    $expr->eq('pl2cg.website_id', 'c.website_id')
                )
            )
            ->leftJoin(
                'cus',
                'oro_price_list_to_customer',
                'pl2cus',
                $expr->andX(
                    $expr->eq('pl2cus.customer_id', 'cus.id'),
                    $expr->eq('pl2cus.website_id', 'c.website_id')
                )
            )
            ->andWhere(
                $expr->andX(
                    $expr->isNull('pl2cus.id'),
                    $expr->isNull('pl2cg.id'),
                    $expr->in('c.website_id', ':websiteIds'),
                )
            )
            ->setParameter('websiteIds', $websiteIds, Connection::PARAM_INT_ARRAY);

        $updateQB->getQuery()->execute();
    }

    public function invalidateGuestCarts(int $websiteId): void
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        $updateQB = $this->getBaseInvalidateQb($websiteId);
        $updateQB->andWhere($expr->isNotNull('c.customer_visitor_id'));

        $updateQB->execute();
    }

    /**
     * Invalidate Cart subtotals by given Product ids for website
     *
     * @param Website $website
     * @param array $productIds
     */
    public function invalidateByProducts(Website $website, array $productIds): void
    {
        if (!$productIds) {
            return;
        }

        $lineItemMetadata = $this->getEntityManager()->getClassMetadata(CartLineItem::class);

        $expr = $this->getEntityManager()->getExpressionBuilder();
        $subQuery = $this->getEntityManager()->getConnection()->createQueryBuilder();
        $subQuery->select($subQuery->expr()->literal(1, Types::INTEGER))
            ->from($lineItemMetadata->getTableName(), 'li')
            ->where(
                $subQuery->expr()->eq('li.cart_id', 'c.id'),
                $subQuery->expr()->in('li.product_id', ':products')
            );

        $updateQB = $this->getBaseInvalidateQb($website->getId());
        $updateQB->andWhere($expr->exists($subQuery->getSQL()));
        $updateQB->setParameter('products', $productIds, Connection::PARAM_INT_ARRAY);

        $updateQB->execute();
    }

    /**
     * Invalidate Cart subtotals by given website
     *
     * @param Website $website
     */
    public function invalidateByWebsite(Website $website): void
    {
        $this->getBaseInvalidateQb($website->getId())->execute();
    }

    /**
     * @param int $websiteId
     *
     * @return SqlQueryBuilder
     */
    protected function getBaseInvalidateQb(int $websiteId): SqlQueryBuilder
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        $updateQB = $this->getInvalidateQb();
        $updateQB->andWhere($expr->eq('c.website_id', ':websiteId'))
            ->setParameter('websiteId', $websiteId, Types::INTEGER);

        return $updateQB;
    }

    protected function getInvalidateQb(): SqlQueryBuilder
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        $rsm = ResultSetMappingUtil::createResultSetMapping(
            $this->getEntityManager()->getConnection()->getDatabasePlatform()
        );
        $updateQB = new SqlQueryBuilder($this->getEntityManager(), $rsm);
        $updateQB->update('xngage_cart_total', 'ct')
            ->innerJoin('ct', 'xngage_cart', 'c', $expr->eq('ct.cart_id', 'c.id'))
            ->set('is_valid', ':newIsValid')
            ->where($expr->eq('ct.is_valid', ':isValid'))
            ->setParameter('newIsValid', false, Types::BOOLEAN)
            ->setParameter('isValid', true, Types::BOOLEAN);

        return $updateQB;
    }
}
