<?php

namespace Xngage\Bundle\CartBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\CustomerBundle\Entity\CustomerVisitor;
use Oro\Bundle\ProductBundle\Entity\Product;
use Xngage\Bundle\CartBundle\Entity\Cart;

class CartRepository extends EntityRepository
{
    /**
     * @param CustomerUser|null $customerUser
     *
     * @return Cart|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCustomerUser(CustomerUser $customerUser): ?Cart
    {
        /** @var Cart $cart */
        $qb = $this->getCartQueryBuilder();

        $qb
            ->andWhere($qb->expr()->eq('cart.customerUser', ':customerUser'))
            ->setParameter('customerUser', $customerUser);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param CustomerVisitor $customerVisitor
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByVisitor(CustomerVisitor $customerVisitor)
    {
        /** @var Cart $cart */
        $qb = $this->getCartQueryBuilder();

        $qb
            ->andWhere($qb->expr()->eq('cart.customerVisitor', ':customerVisitor'))
            ->setParameter('customerVisitor', $customerVisitor);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Cart $cart
     * @return bool
     */
    public function hasEmptyConfigurableLineItems(Cart $cart): bool
    {
        $qb = $this->createQueryBuilder('cart');
        return (bool) $qb
            ->select('1')
            ->innerJoin('cart.lineItems', 'cart_line_item')
            ->innerJoin('cart_line_item.product', 'line_item_product')
            ->where($qb->expr()->eq('cart', ':cart'))
            ->setParameter('cart', $cart->getId())
            ->andWhere($qb->expr()->eq('line_item_product.type', $qb->expr()->literal(Product::TYPE_CONFIGURABLE)))
            ->setMaxResults(1)
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * @return QueryBuilder
     */
    protected function getCartQueryBuilder(): QueryBuilder
    {
        return $this
            ->createQueryBuilder('cart')
            ->select('cart');
    }
}
