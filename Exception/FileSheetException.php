<?php

namespace Xngage\Bundle\CartBundle\Exception;

use Exception;

class FileSheetException extends Exception
{
    /**
     * @param Exception $e The original Box/Spout exception
     */
    public function __construct(Exception $e)
    {
        parent::__construct($e->getMessage(), $e->getCode(), $e);
    }
}
