<?php

namespace Xngage\Bundle\CartBundle\Migrations\Schema;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityBundle\EntityConfig\DatagridScope;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\MigrationBundle\Migration\Installation;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class XngageCartBundleInstaller implements Installation, ExtendExtensionAwareInterface
{
    /** @var ExtendExtension */
    private $extendExtension;

    /**
     * {@inheritdoc}
     */
    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * {@inheritdoc}
     */
    public function getMigrationVersion()
    {
        return 'v1_0';
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createXngageCartTable($schema);
        $this->createXngageCartLineItemTable($schema);
        $this->createXngageCartTotalTable($schema);

        /** Foreign keys generation **/
        $this->addXngageCartForeignKeys($schema);
        $this->addXngageCartLineItemForeignKeys($schema);
        $this->addXngageCartTotalForeignKeys($schema);

        $this->addCartCheckoutSource($schema);
    }

    /**
     * Create xngage_cart table
     *
     * @param Schema $schema
     */
    protected function createXngageCartTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_cart');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('user_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('customer_id', 'integer', ['notnull' => false]);
        $table->addColumn('customer_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('customer_visitor_id', 'integer', ['notnull' => false]);
        $table->addColumn('website_id', 'integer', ['notnull' => false]);
        $table->addColumn('label', 'string', ['length' => 255]);
        $table->addColumn('notes', 'text', ['notnull' => false]);
        $table->addColumn('created_at', 'datetime');
        $table->addColumn('updated_at', 'datetime');
        $table->setPrimaryKey(['id']);
        $table->addIndex(['created_at'], 'xngage_cart_created_at_idx', []);
        $table->addUniqueIndex(
            ['customer_user_id', 'customer_visitor_id'],
            'xngage_cart_uidx'
        );
    }

    /**
     * Create xngage_cart_line_item table
     *
     * @param Schema $schema
     */
    protected function createXngageCartLineItemTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_cart_line_item');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('user_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('customer_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('cart_id', 'integer');
        $table->addColumn('product_id', 'integer');
        $table->addColumn('parent_product_id', 'integer', ['notnull' => false]);
        $table->addColumn('unit_code', 'string', ['length' => 255]);
        $table->addColumn('quantity', 'float');
        $table->addColumn('notes', 'text', ['notnull' => false]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(
            ['product_id', 'cart_id', 'unit_code'],
            'xngage_cart_line_item_uidx'
        );
    }

    /**
     * Create xngage_cart_total table
     *
     * @param Schema $schema
     */
    protected function createXngageCartTotalTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_cart_total');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('cart_id', 'integer');
        $table->addColumn('currency', 'string', ['length' => 255]);
        $table->addColumn(
            'subtotal_value',
            'money',
            ['notnull' => false, 'precision' => 19, 'scale' => 4, 'comment' => '(DC2Type:money)']
        );
        $table->addColumn('is_valid', 'boolean');
        $table->addUniqueIndex(['cart_id', 'currency'], 'unique_cart_currency');
        $table->setPrimaryKey(['id']);
    }

    /**
     * Add xngage_cart foreign keys.
     *
     * @param Schema $schema
     */
    protected function addXngageCartForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_cart');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['user_owner_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_customer_user'),
            ['customer_user_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_customer_visitor'),
            ['customer_visitor_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_customer'),
            ['customer_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_website'),
            ['website_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
    }

    /**
     * Add xngage_cart_line_item foreign keys.
     *
     * @param Schema $schema
     */
    protected function addXngageCartLineItemForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_cart_line_item');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['user_owner_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'SET NULL']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_customer_user'),
            ['customer_user_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('xngage_cart'),
            ['cart_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_product'),
            ['product_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_product'),
            ['parent_product_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_product_unit'),
            ['unit_code'],
            ['code'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add xngage_cart_total foreign keys.
     *
     * @param Schema $schema
     */
    protected function addXngageCartTotalForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_cart_total');
        $table->addForeignKeyConstraint(
            $schema->getTable('xngage_cart'),
            ['cart_id'],
            ['id'],
            ['onDelete' => 'CASCADE']
        );
    }

    /**
     * @param Schema $schema
     */
    protected function addCartCheckoutSource(Schema $schema)
    {
        if (class_exists('Oro\Bundle\CheckoutBundle\Entity\CheckoutSource')) {
            $this->extendExtension->addManyToOneRelation(
                $schema,
                'oro_checkout_source',
                'cart',
                'xngage_cart',
                'id',
                [
                    'entity' => ['label' => 'Xngage.cart.entity_label'],
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM
                    ],
                    'datagrid' => [
                        'is_visible' => DatagridScope::IS_VISIBLE_FALSE,
                    ],
                    'form' => [
                        'is_enabled' => false
                    ],
                    'view' => ['is_displayable' => false],
                    'merge' => ['display' => false],
                    'dataaudit' => ['auditable' => false]
                ]
            );
        }
    }
}
