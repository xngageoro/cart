<?php

namespace Xngage\Bundle\CartBundle\Controller\Frontend;

use Oro\Bundle\ProductBundle\Entity\Product;
use Xngage\Bundle\CartBundle\DataProvider\ProductCartDataProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Generator\MessageGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Base methods for cart line item controllers
 */
abstract class AbstractLineItemController extends AbstractController
{
    /**
     * @param Cart $cart
     * @param Product $product
     * @param $message
     *
     * @return array
     */
    protected function getSuccessResponse(Cart $cart, Product $product, $message): array
    {
        $productCartUnits = $this->get(ProductCartDataProvider::class)->getProductUnitsQuantity($product);

        return [
            'successful' => true,
            'message' => $this->get(MessageGenerator::class)->getCartSuccessMessage($message),
            'product' => [
                'id' => $product->getId(),
                'product_cart_units' => $productCartUnits
            ],
            'cart' => [
                'id' => $cart->getId(),
                'label' => $cart->getLabel()
            ]
        ];
    }

    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            ProductCartDataProvider::class,
            MessageGenerator::class
        ]);
    }
}
