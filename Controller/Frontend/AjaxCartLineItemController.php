<?php

namespace Xngage\Bundle\CartBundle\Controller\Frontend;

use Oro\Bundle\DataGridBundle\Controller\GridController;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Form\Type\FrontendLineItemType;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\SecurityBundle\Annotation\CsrfProtection;
use Oro\Bundle\ShoppingListBundle\Model\LineItemModel;
use Xngage\Bundle\CartBundle\CartLineItem\LineItemMapper;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Form\Handler\CartLineItemHandler;
use Xngage\Bundle\CartBundle\Generator\MessageGenerator;
use Xngage\Bundle\CartBundle\Handler\CartLineItemBatchUpdateHandler;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller that manages products and line items for a cart via AJAX requests.
 * @CsrfProtection()
 */
class AjaxCartLineItemController extends AbstractLineItemController
{
    /**
     * Add Product to Cart (product view form)
     *
     * @Route(
     *      "/add-product-from-view/{productId}",
     *      name="xngage_cart_frontend_add_product",
     *      requirements={"productId"="\d+"},
     *      methods={"POST"}
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     * @ParamConverter("product", class="OroProductBundle:Product", options={"id" = "productId"})
     *
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function addProductFromViewAction(Request $request, Product $product): JsonResponse
    {
        $cart = $this->get(CurrentCartManager::class)->getCurrent();

        if (!$this->isGranted('EDIT', $cart)) {
            throw $this->createAccessDeniedException();
        }

        $parentProduct = $this->getParentProduct($request);

        $lineItem = (new CartLineItem())
            ->setProduct($product)
            ->setCart($cart)
            ->setCustomerUser($cart->getCustomerUser())
            ->setOrganization($cart->getOrganization());

        if ($parentProduct && $parentProduct->isConfigurable()) {
            $lineItem->setParentProduct($parentProduct);
        }

        $form = $this->createForm(FrontendLineItemType::class, $lineItem);

        $handler = new CartLineItemHandler(
            $form,
            $request,
            $this->getDoctrine(),
            $this->get(CartManager::class),
            $this->get(ValidatorInterface::class)
        );
        $isFormHandled = $handler->process($lineItem);

        if (!$isFormHandled) {
            return new JsonResponse(['successful' => false, 'message' => (string)$form->getErrors(true, false)]);
        }

        return new JsonResponse(
            $this->getSuccessResponse($cart, $product, 'xngage.cart.product.added.label')
        );
    }

    /**
     * Remove Line item from Cart
     *
     * @Route(
     *      "/remove-cart-line-item/{lineItemId}",
     *      name="xngage_cart_frontend_remove_line_item",
     *      requirements={"lineItemId"="\d+"},
     *      methods={"DELETE"}
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     * @ParamConverter("lineItem", class="XngageCartBundle:CartLineItem", options={"id" = "lineItemId"})
     *
     * @param CartLineItem $lineItem
     *
     * @return JsonResponse
     */
    public function removeLineItemAction(CartLineItem $lineItem): JsonResponse
    {
        if (!$this->isGranted('DELETE', $lineItem)) {
            throw $this->createAccessDeniedException();
        }

        $isRemoved = $this->get(CartManager::class)->removeLineItem($lineItem);
        if ($isRemoved > 0) {
            $result = $this->getSuccessResponse(
                $lineItem->getCart(),
                $lineItem->getProduct(),
                'xngage.frontend.cart.lineitem.product.removed.label'
            );
        } else {
            $result = [
                'successful' => false,
                'message' => $this->get(TranslatorInterface::class)
                    ->trans('oro.frontend.shoppinglist.lineitem.product.cant_remove.label')
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Remove Product from Shopping List (product view form)
     *
     * @Route(
     *      "/remove-product-from-view/{productId}",
     *      name="xngage_cart_frontend_remove_product",
     *      requirements={"productId"="\d+"},
     *      methods={"DELETE"}
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     * @ParamConverter("product", class="OroProductBundle:Product", options={"id" = "productId"})
     *
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function removeProductFromViewAction(Request $request, Product $product): JsonResponse
    {
        $cart = $this->get(CurrentCartManager::class)->getCurrent();

        if (!$this->isGranted('EDIT', $cart)) {
            throw $this->createAccessDeniedException();
        }

        $result = [
            'successful' => false,
            'message' => $this->get(TranslatorInterface::class)
                ->trans('xngage.frontend.cart.line_item.product.cannot_remove.label')
        ];

        if ($cart) {
            $count = $this->get(CartManager::class)->removeProduct($cart, $product);
            if ($count) {
                $result = $this->getSuccessResponse(
                    $cart,
                    $product,
                    'xngage.frontend.cart.line_item.product.removed.label'
                );
            }
        }

        return new JsonResponse($result);
    }

    /**
     * Update cart line items in batch
     *
     * @Route(
     *      "/batch-update/{id}",
     *      name="xngage_cart_frontend_line_item_batch_update",
     *      requirements={"id"="\d+"},
     *      methods={"PUT"}
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param Request $request
     * @param Cart $cart
     *
     * @return JsonResponse
     */
    public function batchUpdateAction(Request $request, Cart $cart): Response
    {
        $data = \json_decode($request->getContent(), true);
        if (!isset($data['data']) || !is_array($data['data'])) {
            return $this->json(['message' => 'The request data should not be empty'], Response::HTTP_BAD_REQUEST);
        }

        $handler = $this->get(CartLineItemBatchUpdateHandler::class);

        $errors = $handler->process($this->getLineItemModels($data['data']), $cart);
        if ($errors) {
            return $this->json(['message' => implode(', ', $errors)], Response::HTTP_BAD_REQUEST);
        }

        foreach ($data['fetchData'] ?? [] as $key => $value) {
            $request->query->set($key, $value);
        }

        return $this->forward(
            GridController::class . '::getAction',
            ['gridName' => 'frontend-customer-user-cart-edit-grid'],
            $request->query->all()
        );
    }

    /**
     * @param array $rawLineItems
     *
     * @return array
     */
    private function getLineItemModels(array $rawLineItems): array
    {
        return array_filter(
            array_map(
                static function (array $item) {
                    $quantity = (float)$item['quantity'];

                    return $quantity > 0 ?
                        new LineItemModel((int)$item['id'], $quantity, (string)$item['unitCode'])
                        : null;
                },
                $rawLineItems
            )
        );
    }

    /**
     * @param Request $request
     * @return null|Product
     */
    protected function getParentProduct(Request $request): ?Product
    {
        $parentProductId = $request->get('parentProductId');

        return $parentProductId
            ? $this->getDoctrine()->getRepository(Product::class)->find($parentProductId)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                CartManager::class,
                CurrentCartManager::class,
                ValidatorInterface::class,
                LineItemMapper::class,
                MessageGenerator::class,
                TranslatorInterface::class,
                CartLineItemBatchUpdateHandler::class
            ]
        );
    }
}
