<?php

namespace Xngage\Bundle\CartBundle\Controller\Frontend;

use Box\Spout\Common\Type;
use Oro\Bundle\DataGridBundle\Controller\GridController;
use Oro\Bundle\EntityBundle\Exception\EntityNotFoundException;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\PricingBundle\SubtotalProcessor\TotalProcessorProvider;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Xngage\Bundle\CartBundle\DataProvider\CartInstantExportDataProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Exception\FileSheetException;
use Xngage\Bundle\CartBundle\Handler\FileSheetHandler;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/{id}", name="xngage_cart_frontend_view", defaults={"id" = null}, requirements={"id"="\d+"})
     * @Layout()
     * @AclAncestor("xngage_cart_frontend_view")
     *
     * @param Cart $cart
     *
     * @return array
     */
    public function viewAction(Cart $cart = null): array
    {
        if (!$cart) {
            $cart = $this->get(CurrentCartManager::class)->getCurrent();
        }

        if ($cart) {
            $this->get(CartManager::class)->actualizeLineItems($cart);
        }

        return [
            'data' => [
                'entity' => $cart
            ],
        ];
    }

    /**
     * @Route("/update/{id}", name="xngage_cart_frontend_update", defaults={"id" = null}, requirements={"id"="\d+"})
     * @Layout()
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param null|Cart $cart
     *
     * @return array
     */
    public function updateAction(Cart $cart = null): array
    {
        if (!$cart) {
            $cart = $this->get(CurrentCartManager::class)->getCurrent();
        }

        if ($cart) {
            $this->get(CartManager::class)->actualizeLineItems($cart);
        }

        return [
            'data' => [
                'entity' => $cart
            ],
        ];
    }

    /**
     * @Route(
     *      "/{id}/massAction/{gridName}/{actionName}",
     *      name="xngage_cart_frontend_move_mass_action",
     *      requirements={"id"="\d+", "gridName"="[\w\:\-]+", "actionName"="[\w\-]+"}
     * )
     * @Layout
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param Cart $cart
     * @param Request $request
     * @param string $gridName
     * @param string $actionName
     *
     * @return array|Response
     */
    public function moveMassActionAction(
        Cart $cart,
        Request $request,
        string $gridName,
        string $actionName
    ) {
        if ($request->getMethod() === Request::METHOD_GET) {
            return [
                'data' => [
                    'entity' => $cart,
                ],
            ];
        }

        return $this->forward(
            GridController::class . '::massActionAction',
            ['gridName' => $gridName, 'actionName' => $actionName],
            $request->query->all()
        );
    }

    /**
     * @Route(
     *     "/csv/{id}",
     *     name="xngage_cart_frontend_export_to_csv",
     *     defaults={"id" = null},
     *     requirements={"id"="\d+"}
     * )
     * @AclAncestor("xngage_cart_frontend_view")
     *
     * @param Cart|null $cart
     *
     * @return Response
     *
     * @throws EntityNotFoundException
     */
    public function exportToCsvAction(Cart $cart = null): Response
    {
        return $this->getFileSheet(Type::CSV, $cart);
    }

    /**
     * @Route(
     *     "/xlsx/{id}",
     *     name="xngage_cart_frontend_export_to_xlsx",
     *     defaults={"id" = null},
     *     requirements={"id"="\d+"}
     * )
     * @AclAncestor("xngage_cart_frontend_view")
     *
     * @param Cart|null $cart
     *
     * @return Response
     *
     * @throws EntityNotFoundException
     */
    public function exportToXlsxAction(Cart $cart = null): Response
    {
        return $this->getFileSheet(Type::XLSX, $cart);
    }

    /**
     * @param string $type
     * @param Cart $cart
     *
     * @return Response
     *
     * @throws \RuntimeException
     * @throws EntityNotFoundException
     */
    protected function getFileSheet($type, $cart): Response
    {
        if (!$cart) {
            $cart = $this->get(CurrentCartManager::class)->getCurrent();
        }

        if (!$cart instanceof Cart) {
            throw new EntityNotFoundException('Could not find the cart');
        }

        $fileContents = $this->getFileContents($cart, $type);

        $response = new Response($fileContents);
        $response->headers->add($this->generateHeaders($type));

        return $response;
    }

    /**
     * @param Cart $cart
     * @param string $type
     *
     * @return false|string
     */
    protected function getFileContents(Cart $cart, string $type)
    {
        $cartExportDataProvider = $this->get(CartInstantExportDataProvider::class);
        $headerRow = $cartExportDataProvider->generateHeaderRow();
        $dataRows = $cartExportDataProvider->generateRowsData($cart->getLineItems());

        try {
            $fileContents = $this->get(FileSheetHandler::class)->writeFile($type, $dataRows, $headerRow);
        } catch (FileSheetException $e) {
            $error = sprintf('Error creating %s (%s)', $type, $e->getMessage());
            $this->get('logger')->error($error, [ 'exception' => $e ]);

            throw new \RuntimeException($error, $e->getCode(), $e);
        }

        return $fileContents;
    }

    /**
     * @param string $type
     *
     * @return array
     */
    protected function generateHeaders(string $type): array
    {
        $fileName = $this->get(CartInstantExportDataProvider::class)->getFilename($type);

        $headers['Content-Disposition'] = 'attachment; filename="'.$fileName.'"';
        switch ($type) {
            case Type::CSV:
                $headers['Content-Type'] = 'text/csv';
                break;
            case Type::XLSX:
                $headers['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                break;
            default:
                throw new \LogicException("Unknown type $type");
        }

        return $headers;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            CartManager::class,
            CurrentCartManager::class,
            TotalProcessorProvider::class,
            CartInstantExportDataProvider::class,
            FileSheetHandler::class,
        ]);
    }
}
