<?php

namespace Xngage\Bundle\CartBundle\Controller\Frontend\Api\Rest;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Oro\Bundle\EntityBundle\Handler\EntityDeleteHandlerRegistry;
use Oro\Bundle\ProductBundle\Form\Type\FrontendLineItemType;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\SoapBundle\Controller\Api\Rest\RestController;
use Xngage\Bundle\CartBundle\Entity\CartLineItem;
use Xngage\Bundle\CartBundle\Form\Handler\CartLineItemHandler;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for cart line item REST API requests.
 */
class CartLineItemController extends RestController
{
    /**
     * @ApiDoc(
     *      description="Delete Cart Line Item",
     *      resource=true
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param int $id
     * @param int $onlyCurrent
     *
     * @return Response
     */
    public function deleteAction(int $id, int $onlyCurrent = 0): Response
    {
        $success = false;
        /** @var CartLineItem $lineItem */
        $lineItem = $this->getManager()->find($id);

        $view = $this->view(null, Response::HTTP_NO_CONTENT);

        if ($lineItem) {
            if ($this->isGranted('DELETE', $lineItem) && $this->isGranted('EDIT', $lineItem->getCart())) {
                $this->get(CartManager::class)->removeLineItem(
                    $lineItem,
                    (bool) $onlyCurrent
                );
                $success = true;
            } else {
                $view = $this->view(null, Response::HTTP_FORBIDDEN);
            }
        } else {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->buildResponse($view, self::ACTION_DELETE, ['id' => $id, 'success' => $success]);
    }

    /**
     * @ApiDoc(
     *      description="Delete Line Item",
     *      resource=true
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param int $cartId
     * @param int $productId
     * @param string $unitCode
     *
     * @return Response
     */
    public function deleteConfigurableAction(int $cartId, int $productId, string $unitCode): Response
    {
        $success = false;

        /** @var CartLineItem[] $lineItems */
        $lineItems = $this->getDoctrine()
            ->getRepository(CartLineItem::class)
            ->findLineItemsByParentProductAndUnit($cartId, $productId, $unitCode);

        $view = $this->view(null, Response::HTTP_NO_CONTENT);

        $allowed = false;
        $ids = [];

        if ($lineItems) {
            foreach ($lineItems as $lineItem) {
                if (!$this->isGranted('DELETE', $lineItem) || !$this->isGranted('EDIT', $lineItem->getCart())) {
                    break;
                }

                $allowed = true;
            }

            if ($allowed) {
                $options = [];
                $handler = $this->get(EntityDeleteHandlerRegistry::class)
                    ->getHandler(CartLineItem::class);

                foreach ($lineItems as $lineItem) {
                    $handler->delete($lineItem, false);

                    $options[]['entity'] = $lineItem;
                    $ids[] = $lineItem->getId();
                }

                $handler->flushAll($options);

                $success = true;
            } else {
                $view = $this->view(null, Response::HTTP_FORBIDDEN);
            }
        } else {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->buildResponse($view, self::ACTION_DELETE, ['ids' => $ids, 'success' => $success]);
    }

    /**
     * @ApiDoc(
     *      description="Update Cart Line Item",
     *      resource=true
     * )
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     */
    public function putAction(int $id, Request $request): Response
    {
        /** @var CartLineItem $lineItem */
        $lineItem = $this->getManager()->find($id);

        if ($lineItem) {
            if ($this->isGranted('EDIT', $lineItem) && $this->isGranted('EDIT', $lineItem->getCart())) {
                $form = $this->createForm(FrontendLineItemType::class, $lineItem, ['csrf_protection' => false]);

                $handler = new CartLineItemHandler(
                    $form,
                    $request,
                    $this->getDoctrine(),
                    $this->get(CartManager::class),
                    $this->get('validator')
                );

                $isFormHandled = $handler->process($lineItem);
                if ($isFormHandled) {
                    $view = $this->view(
                        ['unit' => $lineItem->getUnit()->getCode(), 'quantity' => $lineItem->getQuantity()],
                        Response::HTTP_OK
                    );
                } else {
                    $view = $this->view($form, Response::HTTP_BAD_REQUEST);
                }
            } else {
                $view = $this->view(null, Response::HTTP_FORBIDDEN);
            }
        } else {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->buildResponse($view, self::ACTION_UPDATE, ['id' => $id, 'entity' => $lineItem]);
    }

    /**
     * {@inheritdoc}
     */
    public function getManager()
    {
        return $this->get('xngage_cart.line_item.manager.api');
    }

    /**
     * {@inheritdoc}
     */
    public function getForm()
    {
        throw new \LogicException('This method should not be called');
    }

    /**
     * {@inheritdoc}
     */
    public function getFormHandler()
    {
        throw new \LogicException('This method should not be called');
    }
}
