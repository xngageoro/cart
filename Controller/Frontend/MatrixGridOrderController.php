<?php

namespace Xngage\Bundle\CartBundle\Controller\Frontend;

use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Xngage\Bundle\CartBundle\Form\Handler\CartMatrixGridOrderFormHandler;
use Xngage\Bundle\CartBundle\Layout\DataProvider\MatrixGridOrderFormProvider;
use Xngage\Bundle\CartBundle\Manager\CartManager;
use Xngage\Bundle\CartBundle\Manager\CartMatrixGridOrderManager;
use Xngage\Bundle\CartBundle\Manager\CurrentCartManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Submits matrix form for configurable products; also displays popup matrix form
 */
class MatrixGridOrderController extends AbstractLineItemController
{
    /**
     * @Route("/{productId}", name="xngage_cart_frontend_matrix_grid_order")
     * @ParamConverter("product", options={"id" = "productId"})
     * @Layout()
     * @AclAncestor("xngage_cart_frontend_view")
     *
     * @param Request $request
     * @param Product $product
     *
     * @return array | JsonResponse
     */
    public function orderAction(Request $request, Product $product)
    {
        $cart = $this->get(CurrentCartManager::class)->getCurrent();

        $form = $this->get(MatrixGridOrderFormProvider::class)->getMatrixOrderForm($product, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $matrixGridOrderManager = $this->get(CartMatrixGridOrderManager::class);
            $lineItems = $matrixGridOrderManager->convertMatrixIntoLineItems(
                $form->getData(),
                $product,
                $request->request->get('matrix_collection', [])
            );

            foreach ($lineItems as $lineItem) {
                $this->get(CartManager::class)->updateLineItem($lineItem, $cart);
            }

            $matrixGridOrderManager->addEmptyMatrixIfAllowed($cart, $product, $lineItems);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(
                    $this->getSuccessResponse(
                        $cart,
                        $product,
                        'xngage.cart.flash.update_success'
                    )
                );
            }
        }

        return ['data' => [
            'product' => $product,
            'cart' => $cart,
            'hasLineItems' => $form->getData()->hasLineItems(),
        ]];
    }

    /**
     * @Route("/{cartId}/{productId}/{unitCode}", name="xngage_cart_frontend_matrix_grid_update")
     * @ParamConverter("cart", options={"id" = "cartId"})
     * @ParamConverter("product", options={"id" = "productId"})
     * @ParamConverter("unit", options={"id" = "unitCode"})
     * @Layout()
     * @AclAncestor("xngage_cart_frontend_update")
     *
     * @param Cart $cart
     * @param Product $product
     * @param ProductUnit $unit
     * @param Request $request
     * @return array|JsonResponse
     */
    public function updateAction(Cart $cart, Product $product, ProductUnit $unit, Request $request)
    {
        $form = $this->get(MatrixGridOrderFormProvider::class)
            ->getMatrixOrderByUnitForm($product, $unit, $cart);

        $result = $this->get(CartMatrixGridOrderFormHandler::class)
            ->process($form->getData(), $form, $request);

        if ($result) {
            return new JsonResponse(
                $this->getSuccessResponse($cart, $product, 'xngage.cart.flash.update_success')
            );
        }

        return [
            'data' => [
                'product' => $product,
                'productUnit' => $unit,
                'cart' => $cart,
                'hasLineItems' => $form->getData()->hasLineItems(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            CartManager::class,
            CurrentCartManager::class,
            CartMatrixGridOrderManager::class,
            MatrixGridOrderFormProvider::class,
            CartMatrixGridOrderFormHandler::class,
        ]);
    }
}
