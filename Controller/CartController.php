<?php

namespace Xngage\Bundle\CartBundle\Controller;

use Oro\Bundle\PricingBundle\SubtotalProcessor\TotalProcessorProvider;
use Xngage\Bundle\CartBundle\Entity\Cart;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/view/{id}", name="xngage_cart_view", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("xngage_cart_view")
     *
     * @param Cart $cart
     *
     * @return array
     */
    public function viewAction(Cart $cart): array
    {
        return [
            'entity' => $cart,
            'totals' => $this->get(TotalProcessorProvider::class)->getTotalWithSubtotalsAsArray($cart)
        ];
    }

    /**
     * @Route("/info/{id}", name="xngage_cart_info", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("xngage_cart_view")
     *
     * @param Cart $cart
     *
     * @return array
     */
    public function infoAction(Cart $cart): array
    {
        return [
            'cart' => $cart
        ];
    }

    /**
     * @Route("/", name="xngage_cart_index")
     * @Template
     * @AclAncestor("xngage_cart_view")
     *
     * @return array
     */
    public function indexAction(): array
    {
        return [
            'entity_class' => Cart::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            TotalProcessorProvider::class,
        ]);
    }
}
