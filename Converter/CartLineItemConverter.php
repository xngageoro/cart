<?php

namespace Xngage\Bundle\CartBundle\Converter;

use Oro\Bundle\CheckoutBundle\Converter\ShoppingListLineItemConverter;
use Xngage\Bundle\CartBundle\Entity\Cart;

/**
 * Converts Cart line items to CheckoutLineItems.
 */
class CartLineItemConverter extends ShoppingListLineItemConverter
{
    /**
     * {@inheritDoc}
     */
    public function isSourceSupported($source): bool
    {
        return $source instanceof Cart;
    }
}
