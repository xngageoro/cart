<?php

namespace Xngage\Bundle\CartBundle\Provider;

use Xngage\Bundle\CartBundle\Entity\Cart;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Provides url for the shopping list entity depends on user access permissions.
 */
class CartUrlProvider
{
    private AuthorizationCheckerInterface $authorizationChecker;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param Cart|null $cart
     * @return string
     */
    public function getFrontendUrl(?Cart $cart = null): string
    {
        if (!$cart) {
            if ($this->authorizationChecker->isGranted('xngage_cart_frontend_update')) {
                return $this->urlGenerator->generate('xngage_cart_frontend_update');
            }

            return $this->urlGenerator->generate('xngage_cart_frontend_view');
        }

        if ($this->authorizationChecker->isGranted('xngage_cart_frontend_update', $cart)) {
            return $this->urlGenerator->generate('xngage_cart_frontend_update', ['id' => $cart->getId()]);
        }

        return $this->urlGenerator->generate('xngage_cart_frontend_view', ['id' => $cart->getId()]);
    }
}
