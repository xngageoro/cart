<?php

namespace Xngage\Bundle\CartBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class CartLineItem extends Constraint
{
    public string $message = 'xngage.cart.lineitem.already_exists';

    /**
     * {@inheritDoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return 'xngage_cart_line_item_validator';
    }
}
