<?php

namespace Xngage\Bundle\CartBundle\Validator\Constraints;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Xngage\Bundle\CartBundle\Entity\CartLineItem as CartLineItemEntity;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks if line item's product and unit are unique in cart.
 */
class CartLineItemValidator extends ConstraintValidator
{
    private ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param CartLineItemEntity $value
     * @param Constraint|LineItem $constraint
     *
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        $lineItemRepository = $this->registry->getRepository(CartLineItemEntity::class);
        $cart = $value->getCart();

        if ($cart && $lineItemRepository->findDuplicateInCart($value, $cart)) {
            $this->context->addViolation($constraint->message);
        }
    }
}
